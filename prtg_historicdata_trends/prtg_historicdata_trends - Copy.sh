#!/bin/bash

RED='\033[1;31m'
NC='\033[0m' # No Color

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Handle script swiches
verbose=false

while getopts ":hv" option; do
  case $option in
    h) echo -e "Usage: '$0 [ -h ] [ -v ]c<CSV filename>'\n"; exit ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

script_opts=()
$verbose && script_opts+=( -v )

$verbose && echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

if [[ $# -eq 0 ]] ; then
  echo -e "\ninsufficient arguments: <CSV filename>"
  echo -e "Usage: '$0 <CSV filename>'\n"
  exit 1
fi

FILE=$1
$verbose && echo "CSV File name is: $FILE"

line_count=0
processed=0
failed_counter=0
succeed_counter=0

# Serach for tabs in input file
awk '{exit !/\t/}' $FILE

if [[ $? = 0 ]]
then
  $verbose && echo -e "Found tab delimited file \n"
  IFS=$'\t'
else
  $verbose && echo -e "Found comma delimited file \n"
  IFS=","
fi

found_Packet_Loss=false
min_Packet_Loss=999999
max_Packet_Loss=0

Packet_Loss_Array=()

handle_Loss() {
    if [[ "$Packet_Loss_RAW" -ne "0" ]] && ! $found_Packet_Loss
    then
      first_Loss_Date_Time=$Date_Time
      found_Packet_Loss=true
    fi

    if [[ $found_Packet_Loss ]]
    then
      (( min_Packet_Loss > Packet_Loss_RAW )) && (( Packet_Loss_RAW > 0 )) && min_Packet_Loss=$Packet_Loss_RAW
      (( max_Packet_Loss < Packet_Loss_RAW )) && max_Packet_Loss=$Packet_Loss_RAW
    fi

    if [[ "$Packet_Loss_RAW" -eq "0" ]] && $found_Packet_Loss
    then
      found_Packet_Loss=false
      Packet_Loss_Array+=("$first_Loss_Date_Time,$Date_Time,$min_Packet_Loss,$max_Packet_Loss")

      min_Packet_Loss=999999
      max_Packet_Loss=0
    fi
}

# Iterate the loop to read and print each array element
print_array() {
  Array=("$@")

  echo "Start Time,End Time,Min,Max"
  for value in "${Array[@]}"
  do
      echo $value
  done
}

summary() {
    echo -e "\n####################################\n"
    echo -e "Summary:"
    echo -e "Total CSV lines: $line_count"
    echo -e "Processed lines: $processed"
    echo -e "Skipped lines:   " $(( $line_count - $processed ))
}

# Search for Retry_percentage_RAW anomaly
while read Date_Time Date_Time_RAW	Ping_Time_Ping Time_RAW	Minimum	Minimum_RAW	Maximum	Maximum_RAW	Packet_Loss	Packet_Loss_RAW	Coverage	Coverage_RAW
do
  ((line_count++))

  # Check Date_Time validaty
  if [[ $(date -d $Date_Time 2> /dev/null) ]]
  then
    handle_Loss
    ((processed++))
  else
    $verbose && echo "Skipped."
  fi
done <<< $(cat "$FILE" | tr -d '\"' | sed 's/.0000//g')

IFS=
echo -e "\nPacket Loss Anomaly"
print_array ${Packet_Loss_Array[@]}

$verbose && summary

$verbose && echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

exit 0

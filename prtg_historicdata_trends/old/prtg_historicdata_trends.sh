#!/bin/bash

RED='\033[1;31m'
NC='\033[0m' # No Color

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Handle script swiches
verbose=false

while getopts ":hv" option; do
  case $option in
    h) echo -e "Usage: '$0 [ -h ] [ -v ]c<CSV filename>'\n"; exit ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

script_opts=()
$verbose && script_opts+=( -v )

$verbose && echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

if [[ $# -eq 0 ]] ; then
  echo -e "\ninsufficient arguments: <CSV filename>"
  echo -e "Usage: '$0 <CSV filename>'\n"
  exit 1
fi

FILE=$1
$verbose && echo "CSV File name is: $FILE"

line_count=0
processed=0
failed_counter=0
succeed_counter=0

# Serach for tabs in input file
awk '{exit !/\t/}' $FILE

if [[ $? = 0 ]]
then
  $verbose && echo -e "Found tab delimited file \n"
  IFS=$'\t'
else
  $verbose && echo -e "Found comma delimited file \n"
  IFS=","
fi

found_Loss_percentage=false
found_Retry_percentage=false

min_Retry_percentage=999999
max_Retry_percentage=0

min_Loss_percentage=999999
max_Loss_percentage=0

Retry_percentage_Array=()
Loss_percentage_Array=()

handle_Retry() {
    if [[ $Retry_percentage_RAW -ne 0 ]] && ! $found_Retry_percentage
    then
      first_Retry_Date_Time=$Date_Time
      found_Retry_percentage=true
    fi

    if [[ $found_Retry_percentage ]]
    then
      (( min_Retry_percentage > $Retry_percentage_RAW )) && (( Retry_percentage_RAW > 0 )) && min_Retry_percentage=$Retry_percentage_RAW
      (( max_Retry_percentage < $Retry_percentage_RAW )) && max_Retry_percentage=$Retry_percentage_RAW
    fi

    if [[ $Retry_percentage_RAW -eq 0 ]] && $found_Retry_percentage
    then
      found_Retry_percentage=false
      #echo -e "$first_Date_Time,$Date_Time,$min_Retry_percentage,$max_Retry_percentage"
      Retry_percentage_Array+=("$first_Retry_Date_Time,$Date_Time,$min_Retry_percentage,$max_Retry_percentage")

      min_Retry_percentage=999999
      max_Retry_percentage=0
    fi
}

handle_Loss() {
    if [[ $Loss_percentage_RAW -ne 0 ]] && ! $found_Loss_percentage
    then
      first_Loss_Date_Time=$Date_Time
      found_Loss_percentage=true
    fi

    if [[ $found_Loss_percentage ]]
    then
      (( min_Loss_percentage > $Loss_percentage_RAW )) && (( Loss_percentage_RAW > 0 )) && min_Loss_percentage=$Loss_percentage_RAW
      (( max_Loss_percentage < $Loss_percentage_RAW )) && max_Loss_percentage=$Loss_percentage_RAW
    fi

    if [[ $Loss_percentage_RAW -eq 0 ]] && $found_Loss_percentage
    then
      found_Loss_percentage=false
      #echo -e "$first_Date_Time,$Date_Time,$min_Loss_percentage,$max_Loss_percentage"
      Loss_percentage_Array+=("$first_Loss_Date_Time,$Date_Time,$min_Loss_percentage,$max_Loss_percentage")

      min_Loss_percentage=999999
      max_Loss_percentage=0
    fi
}

# Iterate the loop to read and print each array element
print_array() {
  Array=("$@")

  echo "Start Time,End Time,Min,Max"
  for value in "${Array[@]}"
  do
      echo $value
  done
}

summary() {
    echo -e "\n####################################\n"
    echo -e "Summary:"
    echo -e "Total CSV lines: $line_count"
    echo -e "Processed lines: $processed"
    echo -e "Skipped lines:   " $(( $line_count - $processed ))
}

# Search for Retry_percentage_RAW anomaly
while read Date_Time Date_Time_RAW Loss_percentage Loss_percentage_RAW Noise_level Noise_level_RAW Associated_station_count Associated_station_count_RAW Channel_utilization Channel_utilization_RAW Retry_percentage Retry_percentage_RAW Throughput Throughput_RAW Coverage Coverage_RAW
do
  ((line_count++))

  # Check Date_Time validaty
  if [[ $(date -d $Date_Time 2> /dev/null) ]]
  then
    handle_Retry
    handle_Loss
    ((processed++))
  else
    $verbose && echo "Skipped."
  fi
done < "$FILE"

IFS=
echo "Retry Anomaly"
print_array ${Retry_percentage_Array[@]}

echo -e "\nLoss Anomaly"
print_array ${Loss_percentage_Array[@]}

$verbose && summary

$verbose && echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

exit 0

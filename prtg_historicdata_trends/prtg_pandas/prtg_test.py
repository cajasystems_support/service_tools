#!/usr/bin/python

import pandas as pd # type: ignore
import numpy as np # type: ignore
import os
import time

start = time.time()

# Read filename from environmrnt parameter
#from dotenv import load_dotenv
#load_dotenv()
#inFile = os.getenv('FILENAME')

inFile = os.sys.argv[1]

#'newfile.csv'
df = pd.read_csv(inFile, usecols =["Date Time","Packet Loss(RAW)"])
df = df.reset_index()

found_Packet_Loss = False
Packet_Loss_Array = np.empty((0, 4), int)

for index, row in df.iterrows():
    PL = row['Packet Loss(RAW)']

    if PL != 0 and not found_Packet_Loss:
        first_Loss_Date_Time = row['Date Time']
        first_Loss_Index = index
        found_Packet_Loss = True

    if PL == 0 and found_Packet_Loss:
        found_Packet_Loss = False
        min_Packet_Loss = min(df[first_Loss_Index:index]['Packet Loss(RAW)'])
        max_Packet_Loss = max(df[first_Loss_Index:index]['Packet Loss(RAW)'])

        newrow = [first_Loss_Date_Time, row['Date Time'], min_Packet_Loss,max_Packet_Loss]
        Packet_Loss_Array = np.vstack([Packet_Loss_Array, newrow])

print(Packet_Loss_Array)

end = time.time()

#Subtract Start Time from The End Time
total_time = end - start
print("\n"+ str(total_time))
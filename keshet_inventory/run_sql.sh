run_sql() {
  SITE=$1
  sql_query=$2
  PARAM1=$3
  PARAM2=$4

  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
  user=$(echo $SCRIPT_DIR | awk -F/ '{print $3'})

  # path /home/ubuntu is being used and not $HOME so every running user will be able to access this file
  # also the full path is being used because the run_sql function can be called through ssh or directly
  db_yml="/home/$user/.config/database_$SITE.yml"
  eval $(parse_yaml $db_yml)

  export PGPASSWORD=$pgsql_pass

  dbCmd="psql -h $pgsql_host -d $pgsql_name -U $pgsql_user"

  case $PARAM1 in
    CSV)  Database=$($dbCmd -A -F , -c "${sql_query}")     ;;
    CCSV) Database=$($dbCmd -A -F , -t -c "${sql_query}")  ;;
    FILE) Database=$($dbCmd -A -F , -t -c -f $PARAM2)      ;;
       *) Database=$($dbCmd -F , -c "${sql_query}")        ;;
  esac

  [ -n "$Database" ] && echo "$Database"
}

#!/bin/bash

keshet_web_upd_db() {
  CSV=$1

  sql_query="
    select barcode,bintype,frame_code,locked_for_recall,isavailable,accessible,consolidation_rank
    from bins b
    where consolidation_rank < 25
      and locked_for_recall is false
      and bintype in ('COLD_BIG_287','COLD_SMALL_208','FOOD_SMALL_208','FOOD_BIG_287','NONFOOD_SMALL_208','NONFOOD_BIG_287')
    order by consolidation_rank"

  echo "$(run_sql $SITE "$sql_query" $CSV | sed 's|[{}]||g' )"
}

usage() {
  echo "Usage: $0 <Site name> <Output filename> " >&2
  echo -e "  Site name   - Site name like Deckers_MW | Deckers_MV | Keshet_KH | Keshet_PT"
  echo -e "  Filename    - Output filename without extension"
  exit 1
}

### MAIN ###

# Handle script swiches
verbose=false

while getopts ":vh" option; do
  case $option in
    h) usage        ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

[ $# -ne 2 ] && usage

SITE=$1
FILE=$2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

$verbose && echo SITE $SITE
$verbose && echo FILE $FILE
$verbose && echo SCRIPT_DIR $SCRIPT_DIR

source $SCRIPT_DIR/parse_yaml.sh
source $SCRIPT_DIR/run_sql.sh
source $SCRIPT_DIR/upload_file_to_ftp.sh

outputFile="${FILE}_${SITE}"
config="/home/ubuntu/.config"
db_yml="$config/database_$SITE.yml"
global_yml="$config/global_$SITE.yml"

validate_yaml $db_yml
validate_yaml $global_yml

# In order that ssconvert will work install gnumeric package
# -- sudo apt-get update -y
# -- sudo apt-get install -y gnumeric
#./Connect.sh 18 keshet_web_upd CCSV | grep '^[0-9]' > 110822192238.csv && ln -sf 110822192238.csv 110822192238.ln && ssconvert 110822192238.ln 110822192238.xlsx && ftp -v -in -u ftp://keshet-teamim-ftp:bEa8j72vM@82.163.139.76/keshetTeamimInventoryReport/110822192238.xlsx ~/110822192238.xlsx
rm ${outputFile}.csv ${outputFile}.ln ${outputFile}.xlsx 2> /dev/null
keshet_web_upd_db CSV | grep -v "^," > ${outputFile}.csv && sed -i '$ d' ${outputFile}.csv && ln -sf ${outputFile}.csv ${outputFile}.ln && ssconvert ${outputFile}.ln ${outputFile}.xlsx

eval $(parse_yaml $global_yml)
upload_file_to_ftp ${outputFile}.xlsx $FTP_ORDER_RECOMMENDATION_FOLDER

upload_file_to_ftp() {
  FILE=$1
  FOLDER=$2

  $verbose && echo FTP_HOST $FTP_HOST
  $verbose && echo FTP_USER $FTP_USER
  $verbose && echo FTP_PASSWD $FTP_PASSWD
  $verbose && echo FOLDER $FOLDER
  $verbose && echo FILE $FILE

ftp -p -n $FTP_HOST << EOF
  quote USER $FTP_USER
  quote PASS $FTP_PASSWD
  binary
  cd $FOLDER
  put $FILE
  quit
EOF
}

#!/bin/bash

keshet_web_upd_db() {
  CSV=$1

  sql_query_1="
    select upcs , qty , unavailableqty,name
    from skus s
    where s.updated >= round(DATE_PART('epoch',now()- '$time_interval hour'::INTERVAL)*1000)
      and s.qty = 0  and unavailableqty = 0"

  sql_query_2="
    with STEP as (
      select distinct s.upcs, s.qty,
             to_char(to_timestamp(b.entry_date  /1000),'DD/MM/YYYY HH24:MI:SS') AS fixed_entry,
             s."name", s.updated
      from binsskus b
      right join skus s on b.skuid = s.id
      where b.entry_date >= round(DATE_PART('epoch',now()- '$time_interval hour'::INTERVAL)*1000))

      select distinct upcs as upcs, qty ,name, min(fixed_entry)
      from STEP
      group by qty, upcs, name, updated
      order by qty desc"

  echo "$(run_sql $SITE "$sql_query_1" $CSV | sed 's|[{}]||g' )"
  echo
  echo "$(run_sql $SITE "$sql_query_2" $CSV | sed 's|[{}]||g' )"
}

usage() {
  echo "Usage: $0 <Site name> <Time interval> <Output filename> " >&2
  echo -e "  Site name       - Site name like Deckers_MW | Deckers_MV | Keshet_KH | Keshet_PT"
  echo -e "  Time interval   - Time interval in Hours (HH)"
  echo -e "  Filename        - Output filename without extension"
  exit 1
}

### MAIN ###

# Handle script swiches
verbose=false

while getopts ":vh" option; do
  case $option in
    h) usage        ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

[ $# -ne 3 ] && usage

SITE=$1
time_interval=$2
FILE=$3

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

$verbose && echo SITE $SITE
$verbose && echo time_interval $time_interval
$verbose && echo FILE $FILE
$verbose && echo SCRIPT_DIR $SCRIPT_DIR

source $SCRIPT_DIR/parse_yaml.sh
source $SCRIPT_DIR/run_sql.sh
source $SCRIPT_DIR/upload_file_to_ftp.sh

outputFile="${FILE}"
config="/home/ubuntu/.config"
db_yml="$config/database_$SITE.yml"
global_yml="$config/global_$SITE.yml"

validate_yaml $db_yml
validate_yaml $global_yml

# In order that ssconvert will work install gnumeric package
# -- sudo apt-get update -y
# -- sudo apt-get install -y gnumeric
#./Connect.sh 18 keshet_web_upd CCSV | grep '^[0-9]' > 110822192238.csv && ln -sf 110822192238.csv 110822192238.ln && ssconvert 110822192238.ln 110822192238.xlsx && ftp -v -in -u ftp://keshet-teamim-ftp:bEa8j72vM@82.163.139.76/keshetTeamimInventoryReport/110822192238.xlsx ~/110822192238.xlsx
rm ${outputFile}.csv ${outputFile}.ln ${outputFile}.xlsx 2> /dev/null
keshet_web_upd_db CCSV | grep '^[0-9]' > ${outputFile}.csv && ln -sf ${outputFile}.csv ${outputFile}.ln && ssconvert ${outputFile}.ln ${outputFile}.xlsx

eval $(parse_yaml $global_yml)
upload_file_to_ftp ${outputFile}.xlsx $FTP_INVENTORY_REPORT_FOLDER

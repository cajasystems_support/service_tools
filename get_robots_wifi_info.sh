#!/bin/bash

echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

if [[ $# -eq 0 ]] ; then
  echo -e "\ninsufficient argument input filename with machine_ids"
  echo -e "Usage: '$0 <filename>' \n"
  exit 1
fi

FILE=$1
echo "Input File name is: $FILE"

line_count=0
processed=0
failed_counter=0
succeed_counter=0

dest_folder="/usr/local/backend/log"
pub_file="/home/ec2-user/.ssh/id_rsa_robots"
ssh_flags="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${pub_file}"

# Read input file line by line delimited by IFS=
while read machine_id
do
  ((line_count++))

  echo "line: " $line_count

  # Check timestamp validaty and that the machine_id is not empty
  if [[ -n "$machine_id" ]]
  then
    # parse robot name for script use, remove prefix i.e BB2 or OP1
    machine_id=${machine_id:3}
    # Make it lowercase
    machine_id=${machine_id,,}

    echo "Input line: $line_count, remove public file for: $machine_id"
    echo "ssh -n ${ssh_flags} ${machine_id} \"iwconfig wlp2s0\""

    ping -w 1 $machine_id
	ping_ret_val=$?

    if [[ $ping_ret_val -ne 0 ]]
    then
	    echo "Robot $machine_id is unreachable"
        ((failed_counter++))
	else
        ssh -n ${ssh_flags} ${machine_id} "iwconfig wlp2s0"
        ret_val=$?
        echo "Error code: " $ret_val

        if [[ $ret_val -ne 0 ]]
        then
          ((failed_counter++))
        else
          ((succeed_counter++))
        fi
    fi

       ((processed++))
  else
    echo "Skipped."
  fi
done < "$FILE"

echo -e "\nSummary:"
echo -e "Total Input lines: $line_count"
echo -e "Processed lines:   $processed"
echo -e "Succeed lines:     $succeed_counter"
echo -e "Failed lines:      $failed_counter"
echo -e "Skipped lines:    " $(( $line_count - $processed ))

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0
  
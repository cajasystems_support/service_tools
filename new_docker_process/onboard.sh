#!/bin/bash

usage() {
  echo -e "\nUsage: $0"
  echo -e "       $0 <run_mode> <robot_mode> <wh_name> <wifi_ssid> <wifi_pass> <chrony_ip>"
  echo -e "       $0 <run_mode> <robot_mode> <wh_name> <wifi_ssid> <wifi_pass> <chrony_ip> <model_type> <model_id> <robot_serial> <base_driver_model> <elevator_driver_model> <lidar_type> <camera_type> <model_height>"
  echo
  echo -e "  -h flag       - show this usage help"
  echo -e "  -v flag       - display verbose messages"
  echo -e "  -s flag       - skip parameters validations"
  echo -e "  -t flag       - test mode (create files locally)"
  echo
  echo -e "run_mode              - setup mode [ ${setup_modes// / | } ]"
  echo -e "                                    factory - get all etc_environment params"
  echo -e "                                    field   - get only WH, CHRONY and WIRELESS params"
  echo -e "robot_mode            - Robot mode [ ${robot_modes// / | } ]"
  echo -e "                                     factory | deboarded - will enable AP mode"
  echo -e "wh_name               - Site WH_NAME (only letters and underscores are allowed)"
  echo -e "wifi_ssid             - Wifi SSID (Caja-Robots)"
  echo -e "wifi_pass             - Wifi Password"
  echo -e "chrony_ip             - Chrony Server IP [x.x.x.x]"
  echo -e "model_type            - Model Type [ ${model_types// / | } ]"
  echo -e "model_id              - Model ID [ ${model_ids// / | } ]"
  echo -e "robot_serial          - Robot short serial number (only digits XXXX)"
  echo -e "base_driver_model     - Base driver model [ ${base_driver_models// / | } ]"
  echo -e "elevator_driver_model - Elevator driver model [ ${elevator_driver_models// / | } ]"
  echo -e "lidar_type            - Lidar type [ ${lidar_types// / | } ]"
  echo -e "camera_type           - Camera type [ ${camera_types// / | } ]"
  echo -e "model_height          - Model height [ ${model_heights// / | } ]"
  exit 1
}

function choose_from_menu() {
  local prompt="$1" outvar="$2"
  shift
  shift
  local options=("$@") cur=0 count=${#options[@]} index=0
  local esc=$(echo -en "\e") # cache ESC as test doesn't allow esc codes
  printf "$prompt\n"

  while true
  do
    # list all options (option list is zero-based)
    index=0 
    for o in "${options[@]}"
    do
      if [ "$index" == "$cur" ]
      then echo -e " >\e[7m$o\e[0m" # mark & highlight the current option
      else echo "  $o"
      fi
      (( index++ ))
    done

    read -s -n3 key # wait for user to key in arrows or ENTER

    if [[ $key == $esc"[A" ]]; then # up arrow
      (( cur-- )); (( cur < 0 )) && (( cur = 0 ))
    elif [[ $key == $esc"[B" ]]; then # down arrow
      (( cur++ )); (( cur >= count )) && (( cur = count - 1 ))
    elif [[ $key == "" ]]; then # nothing, i.e the read delimiter - ENTER
      break
    fi

    echo -en "\e[${count}A" # go up to the beginning to re-render
  done

  # export the selection to the requested output variable
  printf -v $outvar "${options[$cur]}"
}

serach_json_item(){
  item_to_search="$1"

  # Search for the item using jq
  ipAddresses="$(jq -r --arg item "$item_to_search" '.warehouses[] | select(.houseName == $item) | .ipAddresses' "$JSON_FILE")"
  whName="$(     jq -r --arg item "$item_to_search" '.warehouses[] | select(.houseName == $item) | .whName'      "$JSON_FILE")"
}

get_params_from_user() {
  echo "Configure robot parameters"

  options=($setup_modes)
  choose_from_menu "\nSelect setup mode:" run_mode "${options[@]}"

  options=($robot_modes)
  choose_from_menu "\nRobot mode:" robot_mode "${options[@]}"

  choose_from_menu "\nWarehouse name:" wh_full_name "${warehouses[@]}"
  serach_json_item "$wh_full_name"

  default_wh_name=$whName
  default_chrony_ip=$ipAddresses

  echo -en "\nEnvironment wh_name (${RED}$default_wh_name${NC}): "
  read -r wh_name
  wh_name=${wh_name:-$default_wh_name}

  echo -en "Wifi SSID (${RED}$default_wifi_ssid${NC}): "
  read -r  wifi_ssid
  wifi_ssid=${wifi_ssid:-"$default_wifi_ssid"}

  read -p "Wifi Password: " wifi_pass
  wifi_pass=${wifi_pass:-$default_wifi_pass}

  echo -en "Chrony Server IP (${RED}$default_chrony_ip${NC}): "
  read -r  chrony_ip
  chrony_ip=${chrony_ip:-"$default_chrony_ip"}

  [ "$run_mode" == "factory" ] && get_factory_params_from_user
}

get_factory_params_from_user() {
  options=($model_ids)
  choose_from_menu "\nModel ID:" model_id "${options[@]}"

  case $model_id in
    CT02) model_height=08 ; model_type=CART ;;
    CT03) model_height=20 ; model_type=CART ;;
    LT02) model_height=32 ; model_type=LIFT ;;
    LT03) model_height=53 ; model_type=LIFT ;;
  esac

  while true; do
    echo -en "\nSerial (${YLW}digits only XXXX${NC}): " 
    read -r robot_serial

    if [ -z ${robot_serial} ]; then
      echo "Serial is mandatory, please re-enter."
    elif [[ ! "$robot_serial" =~ ^[0-9]+$ ]]; then
      echo "Serial should contains digits only, please re-enter."
    else
      robot_serial="$(printf "%04d" ${robot_serial})"
      break
    fi
  done

  build_serial_params

  echo -en "Full serial (${RED}$full_serial${NC}): "
  read -r _full_serial

  full_serial=${_full_serial:-"$full_serial"}
  backend_identifier="${full_serial//-/_}"

  options=($base_driver_models)
  choose_from_menu "\nBase driver model:" base_driver_model "${options[@]}"

  options=($elevator_driver_models)
  [[ "$model_id" == "CT02" ]] && elevator_driver_model=stxi || choose_from_menu "Elevator driver model:" elevator_driver_model "${options[@]}"

  options=($lidar_types)
  choose_from_menu "\nLidar type:" lidar_type "${options[@]}"

  options=($camera_types)
  choose_from_menu "\nCamera type:" camera_type "${options[@]}"
  [[ "$camera_type" == "ueye" ]] && imu_type="/um7"
}

build_serial_params(){
  case $model_id in
    CT02) serial_filler=DUN ;;
    LT02) serial_filler=ALT ;;
    CT03|LT03) serial_filler=$model_height ;;
  esac

  full_serial="$model_id""-""$serial_filler""-""$robot_serial"
}

get_params_from_external() {
  run_mode=${1}
  [[ "$run_mode" == "field" && $# -eq 14 ]] && { echo -e "run_mode ${RED}field${NC} type requires only 6 input parameters. Aborting." ; exit 1 ; }

  robot_mode=${2}
  wh_name=${3}
  wifi_ssid=${4}
  wifi_pass=${5}
  chrony_ip=${6}

  if [ "$run_mode" == "factory" ]; then
    model_type=${7}
    model_id=${8}
    robot_serial=${9}
    base_driver_model=${10}
    elevator_driver_model=${11}
    lidar_type=${12}
    camera_type=${13}
    model_height=${14}

    [[ "$camera_type" == "ueye" ]] && imu_type="/um7"

    robot_serial="$(printf "%04d" ${robot_serial})"
    build_serial_params
    backend_identifier="${full_serial//-/_}"
  fi
}

validate_parameters() {
  general_validation() {
    local list="$1" item="$2" item_name="$3" special_message="$4"
    [ -z "$special_message" ] && message="can be [${list}]" || message="$special_message"
    [[ " ${list} " =~ " $item " ]] || { echo -e "Invalid $item_name ${RED}$item${NC}, ${message}." ; exit 1 ; }  
  }

  echo

  general_validation "${setup_modes}" $run_mode   run_mode
  general_validation "${robot_modes}" $robot_mode robot_mode
  [[ $wh_name =~ ^[A-Za-z_]+$ ]]                            || { echo -e "Invalid wh_name ${RED}$wh_name${NC}, only letters and underscore are allowed." ; exit 1 ; }
  [[ $chrony_ip =~ ^(([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.){3}([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))$ ]] || { echo -e "Invalid chrony_ip ${RED}$chrony_ip${NC}" ; exit 1 ; }

  if [ "$run_mode" == "factory" ]; then
    [[ "$robot_serial" =~ ^[0-9]+$ ]] || { echo -e "Invalid robot_serial ${RED}$robot_serial${NC}, only digits are allowed." ; exit 1 ; }
    general_validation "${model_types}"            $model_type            model_type
    general_validation "${model_ids}"              $model_id              model_id
    general_validation "${base_driver_models}"     $base_driver_model     base_driver_model
    general_validation "${elevator_driver_models}" $elevator_driver_model elevator_driver_model
    general_validation "${lidar_types}"            $lidar_type            lidar_type
    general_validation "${camera_types}"           $camera_type           camera_type
    general_validation "${model_heights}"          $model_height          model_height
  fi
}

read_params_from_json() {
  print_json_params() {
    # Combine and print the data from the arrays
    echo "Details from JSON file:"
    echo setup_modes=$setup_modes
    echo robot_modes=$robot_modes
    echo model_types=$model_types
    echo model_ids=$model_ids
    echo model_heights=$model_heights
    echo base_driver_models=$base_driver_models
    echo elevator_driver_models=$elevator_driver_models
    echo lidar_types=$lidar_types
    echo camera_types=$camera_types
    echo warehouses=$warehouses

    echo -e "\nDefault parameters:"
    echo default_wh_name=$default_wh_name
    echo default_wifi_ssid=$default_wifi_ssid
    echo default_wifi_pass=$default_wifi_pass
    echo default_chrony_ip=$default_chrony_ip
    echo ""
  }

  # Check if jq is installed
  if ! command -v jq &> /dev/null; then
    echo -e "${RED}jq${NC} is not installed. Please install it to run this script. Aborting."
    exit 1
  fi

  # JSON file
  JSON_FILE="onboard.json"

  # Check if the JSON file exists
  if [ ! -f "$JSON_FILE" ]; then
    echo -e "File ${RED}$JSON_FILE${NC} does not exist. Aborting."
    exit 1
  fi

  # Read and parse the JSON arrays using jq
  setup_modes=$(           jq -r '[.setup_modes[]] | join(" ")'               "$JSON_FILE")
  robot_modes=$(           jq -r '[.robot_modes[]] | join(" ")'               "$JSON_FILE")
  model_types=$(           jq -r '[.model_types[]] | join(" ")'               "$JSON_FILE")
  model_ids=$(             jq -r '[.model_ids[] | keys | .[]] | join(" ")'    "$JSON_FILE")
  model_heights=$(         jq -r '[.. | objects | .model_height? | select(.)] | unique | join(" ")' "$JSON_FILE")
  base_driver_models=$(    jq -r '[.base_driver_models[]] | join(" ")'        "$JSON_FILE")
  elevator_driver_models=$(jq -r '[.elevator_driver_models[]] | join(" ")'    "$JSON_FILE")
  lidar_types=$(           jq -r '[.lidar_types[]]  | join(" ")'              "$JSON_FILE")
  camera_types=$(          jq -r '[.camera_types[]] | join(" ")'              "$JSON_FILE")
  mapfile -t warehouses < <(jq -r '.warehouses[].houseName'    "$JSON_FILE")

  # Read default parameters
  default_wh_name=$(  jq -r '.defaultParams.default_wh_name'   "$JSON_FILE")
  default_wifi_ssid=$(jq -r '.defaultParams.default_wifi_ssid' "$JSON_FILE")
  default_wifi_pass=$(jq -r '.defaultParams.default_wifi_pass' "$JSON_FILE")
  default_chrony_ip=$(jq -r '.defaultParams.default_chrony_ip' "$JSON_FILE")

  $verbose && print_json_params
}

print_parameters_to_user() {
  echo -e "\nThe following parameters will be used for environment/chrony/connectivity files:"
  echo -e "Setup mode:            $run_mode"
  echo -e "Robot mode:            $robot_mode"
  echo -e "Warehouse name:        $wh_name"
  echo -e "Wifi SSID:             $wifi_ssid"
  echo -e "Wifi Password:         $wifi_pass"
  echo -e "Chrony Server IP:      $chrony_ip"

  if [ "$run_mode" == "factory" ]; then
    echo -e "Model Type:            $model_type"
    echo -e "Model ID:              $model_id"
    echo -e "Serial:                $robot_serial"
    echo -e "Full Serial:           $full_serial"
    echo -e "Backend Identifier:    $backend_identifier"
    echo -e "Base driver model:     $base_driver_model"
    echo -e "Elevator driver model: $elevator_driver_model"
    echo -e "Lidar type:            $lidar_type"
    echo -e "Camera type:           $camera_type"
    echo -e "IMU type:              $imu_type"
    echo -e "Model height:          $model_height"
  fi
}

### MAIN ###

# Handle script swiches
verbose=false
skipValidation=false
testMode=false

while getopts ":hvst" option; do
  case $option in
    h) read_params_from_json ; usage ;;
    v) verbose=true ;;
    s) skipValidation=true ;;
    t) testMode=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit 1 ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

RED='\033[1;31m'     # Red Color
YLW='\033[1;33m'     # Yellow Color
NC='\033[0m'         # No Color

read_params_from_json

case $# in
  0)  get_params_from_user ;;
  6)  get_params_from_external ${1} ${2} ${3} ${4} ${5} ${6} ;;
  14) get_params_from_external ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10} ${11} ${12} ${13} ${14} ;;
  *)  usage ;;
esac

print_parameters_to_user

echo -en "\nAre you sure you would like to continue? (Yy). "
read -p "[ ]"$'\b\b' -n 1 -r
echo

[[ $REPLY =~ ^[Yy]$ ]] || { echo -e "\nAborted." ; exit 1 ; }
[ $skipValidation = false ] && validate_parameters

ENV_DATA="""PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
ROS_LOG_DIR=/caja/roslogs/
MODEL_TYPE=${model_type}
MODEL_ID=${model_id}
BASE_DRIVER_MODEL=${base_driver_model}
ELEVATOR_DRIVER_MODEL=${elevator_driver_model}
LIDAR_TYPE=${lidar_type}
CAMERA_TYPE=${camera_type}
IMU_TYPE=${imu_type}
FULL_SERIAL=${full_serial}
MODEL_HEIGHT=${model_height}
SERIAL=${robot_serial}
BACKEND_IDENTIFIER=${backend_identifier}
WH_NAME=${wh_name}
CONFIG_BASE_DIR=/caja/caja_ws/caja_hardware_config
# "onboarded" | "deboarded" | "factory" - Decides WiFi AP Mode and other things.
SETUP_STATUS=${robot_mode}
"""

$verbose && echo -e "ENV_DATA:\n${ENV_DATA}"

if [ "$wifi_pass" == "REPLACE_ME" ]; then
  echo "Skipping wifi password replacement"
else
  $testMode && netplan_conf_wifi="00-installer-config-wifi.yaml" || netplan_conf_wifi="/etc/netplan/00-installer-config-wifi.yaml"
  
  echo "Replacing wifi password"

  # Clear all access-points
  yq eval 'del(.network.wifis.wlp2s0.access-points)' -i $netplan_conf_wifi

  # Add new access-points
  yq eval ".network.wifis.wlp2s0.access-points = {\"$wifi_ssid\": {\"password\": \"$wifi_pass\"}}" -i "$netplan_conf_wifi"

  # Add quotes (") only when string is not only digits
  [[ "$wifi_ssid" =~ ^[0-9]+$ ]] || yq eval '.network.wifis.wlp2s0.access-points |= with_entries(.key |= "\"" + . + "\"")' -i "$netplan_conf_wifi"
  [[ "$wifi_pass" =~ ^[0-9]+$ ]] || yq eval '.network.wifis.wlp2s0.access-points |= with_entries(.value.password |= "\"" + . + "\"")' -i "$netplan_conf_wifi"

  # Remove surrounding apostrophe ('), skip remarks
  sed -i "/^\s*#/!s/'//g" "$netplan_conf_wifi"
fi

$testMode && env_file="environment" || env_file="/etc/environment"
if [ "$run_mode" == "factory" ]; then
  echo "Replacing $env_file with contents: "
  echo "$ENV_DATA"
  echo "$ENV_DATA" > $env_file
else
  echo "Replacing WH_NAME and SETUP_STATUS"
  sed -i.bak "s/^WH_NAME=.*$/WH_NAME=${wh_name}/g" $env_file
  sed -i "s/^SETUP_STATUS=.*$/SETUP_STATUS=${robot_mode}/g" $env_file
fi

echo "Applying environment configuration."
source $env_file

$testMode && docker_env_file="docker.env" || docker_env_file="/caja/image/docker.env"
$testMode && image_env_file=".env" || image_env_file="/caja/image/.env"
echo "" > $docker_env_file
echo -e "HOSTNAME=$HOSTNAME" >> $docker_env_file
echo -e "WH_NAME=$WH_NAME" >> $docker_env_file
echo -e "LIDAR_TYPE=$LIDAR_TYPE" >> $docker_env_file
echo -e "CAMERA_TYPE=$CAMERA_TYPE" >> $docker_env_file
echo -e "IMU_TYPE=$IMU_TYPE" >> $docker_env_file
echo -e "MODEL_ID=$MODEL_ID" >> $docker_env_file
echo -e "MODEL_TYPE=$MODEL_TYPE" >> $docker_env_file
echo -e "BASE_DRIVER_MODEL=$BASE_DRIVER_MODEL" >> $docker_env_file
echo -e "ELEVATOR_DRIVER_MODEL=$ELEVATOR_DRIVER_MODEL" >> $docker_env_file
echo -e "FULL_SERIAL=$FULL_SERIAL" >> $docker_env_file
echo -e "MODEL_HEIGHT=$MODEL_HEIGHT" >> $docker_env_file
echo -e "SERIAL=$SERIAL" >> $docker_env_file
echo -e "BACKEND_IDENTIFIER=$BACKEND_IDENTIFIER" >> $docker_env_file
cat $image_env_file >> $docker_env_file

$testMode && chrony_file="chrony.conf" || chrony_file="/etc/chrony/chrony.conf"
echo "Setting Chrony server to: $chrony_ip"
sed -i.bak "s/^server\s\([0-9.]*\)\sprefer\siburst/server $chrony_ip prefer iburst/g" $chrony_file

$testMode && exit 0

echo "Applying network - Connection will be lost, wait for 3 minutes."
netplan apply

count=0
while true; do
  curl localhost:8181/health
  if [ $? -eq 0 ]; then
    break
  fi
  
  sleep 1
  count=$((count+1))
  
  if [ $count -gt 120 ]; then
    echo "Failed to connect to the robot"

    if [ "$run_mode" == "factory" ]; then
      sed -ie "s/^SETUP_STATUS.*$/SETUP_STATUS=factory/g" /etc/environment
    elif [ "$run_mode" == "field" ]; then
      sed -ie "s/^SETUP_STATUS.*$/SETUP_STATUS=deboarded/g" /etc/environment
    fi

    source /etc/environment
    sleep 3
    echo "Error - Failed to connect with provided network details. Reactivating Access Point mode."
    exit 1
  fi
done

if [ "$(hostname)" == "default" ]; then
  echo "Updating certificates for $MODEL_ID-$SERIAL"
  hostnamectl set-hostname "$MODEL_ID-$SERIAL"
  curl localhost:8181/thing_name/$MODEL_ID-$SERIAL
  sleep 3
  curl localhost:8181/update_certs
  sleep 1
  curl localhost:8181/reload_certs
fi

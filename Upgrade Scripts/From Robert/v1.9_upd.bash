#!/bin/bash

if [ $# == 0 ]; then
	LIDAR_TYPE="_yd"
elif [ $1 == "TG" ]; then
	LIDAR_TYPE="_tg"
else
	LIDAR_TYPE="_yd"
fi
ROBOTS_SW_VER="rc_v1.9.0"
LOG_FILE="/tmp/image_migration.log"

echo "" > $LOG_FILE
 
function send_to_log ()
{
	echo "$(date -u +"%T.%N") UTC: $1" | tee -a $LOG_FILE
}

send_to_log "Installing $ROBOTS_SW_VER. Log file in $LOG_FILE"

send_to_log "Closing running application screen"
screen -X -S $(screen -ls | grep Detached | awk '{print $1}') stuff "^C"
screen -ls | egrep "Detached"
while [ $? == 0 ]; do
	send_to_log "Waiting for running application screen to close"
	sleep 1
	screen -ls | egrep "Detached"
done
send_to_log "Application screen closed"


cd /caja/3rd_parties && git pull && ./install_infonode_3rd_parties.sh
if [ $? != 0 ]; then
	send_to_log "Failed to install third parties"
        exit -1
fi

cd /caja/3rd_parties && ./install-poco.sh
if [ $? != 0 ]; then
	send_to_log "Failed to install poco"
	exit -1
fi

send_to_log "Updating /etc/environment"

sudo grep "IMU_TYPE" /etc/environment
if [ $? == 0 ]; then
	send_to_log "IMU_TYPE already exists in /etc/environment"
else
	sudo bash -c "echo \"IMU_TYPE=/um7\" >> /etc/environment"
fi

sudo grep CAMERA_TYPE /etc/environment
if [ $? == 0 ]; then
	send_to_log "CAMERA_TYPE already exists in /etc/environment"
else
	sudo bash -c "echo "CAMERA_TYPE=ueye" >> /etc/environment"
fi

sudo grep LIDAR_TYPE /etc/environment
if [ $? == 0 ]; then
	send_to_log "LIDAR_TYPE already exists in /etc/environment"
	sudo grep $LIDAR_TYPE /etc/environment
        if [ $? == 0 ]; then
		send_to_log "LIDAR_TYPE already set to $LIDAR_TYPE in /etc/environment"
	else
		if [ $LIDAR_TYPE == "_yd" ]; then
			LIDAR_OLD_TYPE="_tg"
		else
			LIDAR_OLD_TYPE="_yd"
		fi
		sudo bash -c "cat /etc/environment | sed '{s/$LIDAR_OLD_TYPE/$LIDAR_TYPE/g}' > /etc/environment.tmp"
		sudo mv /etc/environment.tmp /etc/environment
		sudo chmod 644 /etc/environment 
	fi	
else
	sudo bash -c "echo "LIDAR_TYPE=$LIDAR_TYPE" >> /etc/environment"
fi

send_to_log "Updated /etc/environment"

send_to_log "Updating sudoers"

sudo grep "os_op.sh" /etc/sudoers.d/os_op
if [ $? == 0 ]; then 
	send_to_log "os_op.sh already exists in sudoers"
else
	sudo bash -c "echo \"caja    ALL=(ALL:ALL)   NOPASSWD:/caja/caja_ws/src/caja-ros-pkgs/robot_operation/scripts/os_op.sh\" >> /etc/sudoers.d/os_op"
fi
sudo grep "os_op.bash" /etc/sudoers.d/os_op
if [ $? == 0 ]; then
	send_to_log "os_op.bash already in sudoers"
else
	sudo bash -c "echo \"caja    ALL=(ALL:ALL)   NOPASSWD:/caja/caja_ws/src/caja-ros-pkgs/robot_operation/scripts/os_op.bash\" >> /etc/sudoers.d/os_op"
fi

sudo grep "/bin/bash" /etc/sudoers.d/os_op
if [ $? == 0 ]; then
	send_to_log "bash already in sudoers"
else
	sudo bash -c "echo \"caja    ALL=(ALL:ALL)   NOPASSWD:/bin/bash\" >> /etc/sudoers.d/os_op"
fi

send_to_log "Updated sudoers"

send_to_log "Updating chrony"

sudo bash -c "cat /etc/chrony/chrony.conf | grep -v \"pool 2.debian.pool.ntp.org offline iburst\" > /etc/chrony/chrony.conf.tmp" 
sudo mv /etc/chrony/chrony.conf.tmp /etc/chrony/chrony.conf

send_to_log "Updated chrony"

send_to_log "Installing kinetic map server"

sudo apt install ros-kinetic-map-server -y && sudo apt install ros-kinetic-tf-conversions -y

send_to_log "Installed kinetic map server"

send_to_log "Updating bashrc"
grep "build -j3" ~/.bashrc
if [ $? == 0 ]; then
	send_to_log "build alias in bashrc already have the -j3 flag"
else
	cat ~/.bashrc | sed '{s/catkin build/catkin build -j3/g}' > ~/.bashrc.tmp
	mv  ~/.bashrc.tmp  ~/.bashrc
fi
source ~/.bashrc

send_to_log "Updated bashrc"

send_to_log "Adjusting repositories"

rm -rf /caja/caja_ws/src/robot-hardware-tests
rm -rf /caja/caja_ws/src/caja_camera_calibration
git -C /caja/caja_ws/src/robot-automation fetch
git -C /caja/caja_ws/src/robot-automation checkout v1.9

send_to_log "Adjusted repositories"

send_to_log "Prepare version"

cd /caja/caja_ws/src/caja_hardware_config
git fetch
git checkout v1.9
cd /caja/caja_ws/src/caja-ros-pkgs
cd ../../
sudo chown -R $USER: ./*
cd /caja/caja_ws/src/caja-ros-pkgs
git fetch
git stash
git checkout $ROBOTS_SW_VER
catkin clean -y
sudo rm -rf natnet_ros
catkin build -j3 --cmake-args -DCMAKE_BUILD_TYPE=Release

send_to_log "Prepared version"

send_to_log "Updating rc.local"

sudo bash -c "cat /etc/rc.local | sed '{s/robot_operation\/scripts\/launch_ro/ro_manager\/scripts\/launch_rom\/launch_rom/g}' > /etc/rc.local.tmp"
sudo mv /etc/rc.local.tmp /etc/rc.local

chmod +x /caja/caja_ws/src/caja-ros-pkgs/ro_manager/scripts/launch_rom/launch_rom
sudo chmod 744 /etc/rc.local

send_to_log "Updated rc.local"

send_to_log "Preparing teleop"

cd ~ && git clone git@bitbucket.org:robotsoft/teleopweb.git
cd teleopweb && git checkout WebController
cd ~ && mkdir Teleop
cp -R ./teleopweb/* ./Teleop
sudo rm -rf /var/www/html/Teleop
sudo mv ~/Teleop /var/www/html/
cd /var/www/html/Teleop && ls Direct.html 

send_to_log "Prepared teleop"

send_to_log "Done !!! Feel free to reboot"

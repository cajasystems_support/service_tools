#!/bin/bash

ondemand_commands() {
  echo -e "List all wifi drivers"
  lshw -C network 2>&1 | grep wireless | grep driver

  echo -e "Ressting WiFi driver"
  sudo modprobe -r iwlwifi && sudo modprobe iwlwifi
}


echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

if [[ $# -eq 0 ]] ; then
  echo -e "\ninsufficient argument input filename with machine_ids"
  echo -e "Usage: '$0 <filename>' \n"
  exit 1
fi

FILE=$1
CSV_FLAG=$2

echo "Input File name is: $FILE"

line_count=0
processed=0
failed_counter=0
succeed_counter=0

dest_folder="/usr/local/backend/log"
pub_file="/home/ec2-user/.ssh/id_rsa_robots"
ssh_flags="-o ConnectTimeout=10 -o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"

if [[ "$CSV_FLAG" == "C" ]]
then
  echo "#,Macine ID,Server Time,Robot Time,iburst,chronyc src,Note"
fi

# Read input file line by line delimited by IFS=
while read machine_id
do
  ((line_count++))

  # Check timestamp validaty and that the machine_id is not empty
  if [[ -n "$machine_id" ]]
  then
    # parse robot name for script use, remove prefix i.e BB2 or OP1
    machine_id=${machine_id:3}
    # Make it lowercase
    machine_id=${machine_id,,}

    if [[ "$CSV_FLAG" == "C" ]]
    then
      echo -n "$line_count,$machine_id,"
    else
      echo -e "==============================="
      echo -e "Input line: $line_count | Robot: $machine_id"
      echo -e "==============================="
    fi

    ping -w 1 $machine_id > /dev/null 2>&1
    ping_ret_val=$?

    if [[ $ping_ret_val -ne 0 ]]
    then
        if [[ "$CSV_FLAG" == "C" ]]
        then
          echo ",,,,,,Robot $machine_id is unreachable"
        else
          echo "Robot $machine_id is unreachable"
        fi

        ((failed_counter++))
    else

        server_time=$(date +"%T.%N")

        if [[ "$CSV_FLAG" == "C" ]]
        then
          echo -n "$server_time,"
        else
          echo -e "\nServer Time: " $server_time
        fi

        ssh -t -n $ssh_flags $machine_id "$(typeset -f) ; ondemand_commands $CSV_FLAG"

        ret_val=$?

        if [[ $ret_val -ne 0 ]]
        then
          ((failed_counter++))

          if [[ "$CSV_FLAG" == "C" ]]
          then
            echo ",,,,,,Error code: " $ret_val
          else
            echo -e "\nError code: " $ret_val
          fi
        else
          ((succeed_counter++))
        fi
    fi

    ((processed++))
  else
    if [[ "$CSV_FLAG" == "C" ]]
    then
      echo -e "$line_count,Skipped"
    else
      echo -e "==============================="
      echo -e "Input line: $line_count "
      echo -e "==============================="
      echo -e "Skipped."
    fi
  fi
done < "$FILE"

echo -e "\nSummary:"
echo -e "Total Input lines: $line_count"
echo -e "Processed lines:   $processed"
echo -e "Succeed lines:     $succeed_counter"
echo -e "Failed lines:      $failed_counter"
echo -e "Skipped lines:    " $(( $line_count - $processed ))

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0
  

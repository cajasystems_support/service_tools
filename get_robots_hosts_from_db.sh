#!/bin/bash

# include parse_yaml function
source /home/ubuntu/auto_get_bags_and_logs/parse_yaml.sh

handle_hosts_file() {
  lineToDeleteFrom=$(grep -n -m 1 "^# Caja Robots" $ORG_HOSTS | sed 's/\([0-9]*\).*/\1/')

  if [[ $lineToDeleteFrom = "" ]]
  then
    echo -e "Didn't found string \"# Caja Robots\" in $ORG_HOSTS file, appending to the end of $ORG_HOSTS"
    cp $ORG_HOSTS $NEW_HOSTS
    echo -e "\n# Caja Robots" >> $NEW_HOSTS
  else
    echo -e "Found string \"# Caja Robots\" in $ORG_HOSTS file, refreshing robots list"
    head -n $lineToDeleteFrom $ORG_HOSTS > $NEW_HOSTS
  fi

  handle_hosts=false
}

close_hosts_file() {
    diff $NEW_HOSTS $ORG_HOSTS > /dev/null 2>&1
    res=$?

    if [[ $res -eq 0 ]]
    then
      echo -e "New hosts file is simmilar to $ORG_HOSTS file. No update required. Removing new file.\n"
      rm $NEW_HOSTS
    else
      echo -e "New hosts file is different from $ORG_HOSTS file. Updating $ORG_HOSTS with updated robots list\n"
      # Backip /etc/hosts file
      cp $ORG_HOSTS $OLD_HOSTS
      # Copy new hosts file to /etc/hosts
      cp $NEW_HOSTS $ORG_HOSTS
    fi
}

get_list_from_db() {
  db_yml=$1
  eval $(parse_yaml $db_yml)

  export PGPASSWORD=$pgsql_pass

  #sql_1="select machineid,ip,status,reported_status from machines order by 1"
  sql_query="
    select ip, machineid
    from   machines
    where status = 'READY'
    order by machineid"

  Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c "${sql_query}"`

  IFS=","

  if [[ -n "$Database"  ]]
  then
    $handle_hosts && handle_hosts_file

    while read machineid ip status reported_status
    do
      ((line_count++))

      # parse robot name for script use, remove prefix i.e BB2 or OP1
      machine_id=${machineid:3}
      # Make it lowercase
      machine_id=${machine_id,,}

      echo -e "$ip\t$machineid\t$machine_id" >> $NEW_HOSTS
    done <<< $Database

    echo -e "\nSummary:"
    echo -e "Total SQL lines: $line_count\n"
  else
    echo -e "Failed to connect to database"
    echo -e "pgsql_host = $pgsql_host"
    echo -e "pgsql_name = $pgsql_name"
    echo -e "pgsql_user = $pgsql_user"
    echo -e "pgsql_pass = $pgsql_pass"
  fi
}

ts=$(date +%s)

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
NEW_HOSTS=$SCRIPT_DIR/archive/hosts.new.$ts
OLD_HOSTS=$SCRIPT_DIR/archive/hosts.old.$ts
ORG_HOSTS=/etc/hosts

handle_hosts=true

for yml in $(ls /home/ubuntu/.config/database*yml)
do
  get_list_from_db $yml
done

close_hosts_file

echo -e "DONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n" | tee >(cat - >&2)

#!/bin/bash

ondemand_commands() {
  chrony_data() {
    robot_time=$(date +"%T.%N")
    chrony_conf=$(cat /etc/chrony/chrony.conf | grep "^[^#;]" | grep "iburst")
    chronyc_src=$(chronyc sources | tail -1)
  }

  calibration_data() {
    calibration_yaml=/caja/caja_ws/src/caja_hardware_config/config/caja_description/`hostname`.yaml
    floor_calibration="$(grep floor_cam_pose -A 8 $calibration_yaml | grep -v 'floor_cam_pose:' | tr -d '\n\t\r' | sed -e 's/^  *//g')"

    #R: p: 0.759 r: -0.005 y: 3.14 T: x: -0.396 y: -0.019 z: 0.117
    regex_serial="p: ([0-9]*.*) r: ([0-9]*.*) y: ([0-9]*.*).*T:.*x: ([0-9]*.*) y: ([0-9]*.*) z: ([0-9]*.*)"
    [[ "$floor_calibration" =~ $regex_serial ]] &&
      floor_T_x="${BASH_REMATCH[1]//[[:blank:]]/}" &&
      floor_T_y="${BASH_REMATCH[2]//[[:blank:]]/}" &&
      floor_T_z="${BASH_REMATCH[3]//[[:blank:]]/}" &&
      floor_R_p="${BASH_REMATCH[4]//[[:blank:]]/}" &&
      floor_R_r="${BASH_REMATCH[5]//[[:blank:]]/}" &&
      floor_R_y="${BASH_REMATCH[6]}"
  }

  CSV=$1

  chrony_data
  calibration_data

  if [[ "$CSV" == "CSV" ]]
  then
    echo -n "$robot_time,$chrony_conf,$chronyc_src,$floor_T_x,$floor_T_y,$floor_T_z,$floor_R_p,$floor_R_r,$floor_R_y"
    echo
  else
    echo "chrony_data: $robot_time, $chrony_conf, $chronyc_src"
    echo "calibration_data: $floor_T_x,$floor_T_y,$floor_T_z,$floor_R_p,$floor_R_r,$floor_R_y"
  fi
}

parse_machine_id() {
  # parse robot name for script use, remove prefix i.e BB2 or OP1
  [[ $machineid =~ ^BB2*|^OP1* ]]   && machine_id=${machineid:3}
  [[ $machineid =~ ^CT02*|^LT02* ]] && machine_id=${machineid:0:2}${machineid: -3}
  [[ $machineid =~ ^LT03* ]]        && machine_id=mg${hostname: -3} # Take last 3 chars and add mg at the begining
  # Make it lowercase
  machine_id=${machine_id,,}
}

usage() {
  echo -e "\ninsufficient argument input filename with machine_ids"
  echo -e "Usage: '$0 <Filename>' \n" >&2
  echo -e "  Filename | db  - Filename that contains robots list or db to read from DB"
  echo -e "  CSV            - CSV parameter for output as CSV format"
  exit 1
}

### MAIN ###

# Handle script swiches
verbose=false

while getopts ":vh" option; do
  case $option in
    h) usage        ;;
    v) verbose=true ;;
    ?) echo "error: option -$option is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

[ $# -lt 1 ] && echo -e "\ninsufficient argument" && usage

echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

FILE=$1
CSV_FLAG=$2

echo "Input File name is: $FILE"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

line_count=0
processed=0
failed_counter=0
succeed_counter=0

pub_file="$HOME/.ssh/id_rsa_robots"
ssh_flags="-o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"

if [[ "$CSV_FLAG" == "CSV" ]]
then
  echo "#,Macine ID,Server Time,Robot Time,iburst,chronyc src,floor_T_x,floor_T_y,floor_T_z,floor_R_p,floor_R_r,floor_R_y,Note"
fi

# Read input file line by line delimited by IFS=
while read machine_id
do
  ((line_count++))

  # Check timestamp validaty and that the machine_id is not empty
  if [[ -n "$machine_id" ]]
  then
    parse_machine_id

    if [[ "$CSV_FLAG" == "CSV" ]]
    then
      echo -n "$line_count,$machine_id,"
    else
      echo -e "==============================="
      echo -e "Input line: $line_count | Robot: $machine_id"
      echo -e "==============================="
    fi

    ping -w 1 $machine_id > /dev/null 2>&1
    ping_ret_val=$?

    if [[ $ping_ret_val -ne 0 ]]
    then
        if [[ "$CSV_FLAG" == "CSV" ]]
        then
          echo ",,,,,,Robot $machine_id is unreachable"
        else
          echo "Robot $machine_id is unreachable"
        fi

        ((failed_counter++))
    else
        server_time=$(date +"%T.%N")

        if [[ "$CSV_FLAG" == "CSV" ]]
        then
          echo -n "$server_time,"
        else
          echo -e "\nServer Time: " $server_time
        fi

        ssh -n $ssh_flags $machine_id "$(typeset -f) ; ondemand_commands $CSV_FLAG"

        ret_val=$?

        if [[ $ret_val -ne 0 ]]
        then
          ((failed_counter++))

          if [[ "$CSV_FLAG" == "CSV" ]]
          then
            echo ",,,,,,Error code: " $ret_val
          else
            echo -e "\nError code: " $ret_val
          fi
        else
          ((succeed_counter++))
        fi
    fi

    ((processed++))
  else
    if [[ "$CSV_FLAG" == "CSV" ]]
    then
      echo -e "$line_count,Skipped"
    else
      echo -e "==============================="
      echo -e "Input line: $line_count "
      echo -e "==============================="
      echo -e "Skipped."
    fi
  fi
done < "$FILE"

echo -e "\nSummary:"
echo -e "Total Input lines: $line_count"
echo -e "Processed lines:   $processed"
echo -e "Succeed lines:     $succeed_counter"
echo -e "Failed lines:      $failed_counter"
echo -e "Skipped lines:    " $(( $line_count - $processed ))

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0
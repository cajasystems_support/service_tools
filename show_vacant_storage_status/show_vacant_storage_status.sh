#!/bin/bash

parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

run_sql() {
  sql_query=$1
  PARAM1=$2
  PARAM2=$3

  # I am using /home/ubuntu and not $HOME so every running user will be able to access this file
  db_yml="/home/ubuntu/.config/database.yml"
  eval $(parse_yaml $db_yml)

  export PGPASSWORD=$pgsql_pass

  case $PARAM1 in
    CSV)  Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -c "${sql_query}"`     ;;
    CCSV) Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c "${sql_query}"`  ;;
    FILE) Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c -f $PARAM2`      ;;
       *) Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -F , -c "${sql_query}"`        ;;
  esac

  if [[ -n "$Database" ]]
  then
    echo "$Database"
  fi
}

vacant_storage_db() {
  CSV=$1

  sql_query="
    select frametype, count(*)
    from frames f
    where locationarea in (select id from zones z where type = 'STORAGE')
      and isvacant and isavailable and issuspended = false and accessible
    group by frametype"

  run_sql "$sql_query" $CSV | sed 's|frameType||g'
}

vacant_storage=$(vacant_storage_db CCSV)

if [[ $vacant_storage = "" ]]
then
    echo -e "Query returns no value\n"
    exit 1
fi

declare -A arr
i=0

while IFS="," read -r frameType amount
do
    arr[$i,0]=frameType
    arr[$i,1]=amount
    ((i++))
done <<< "$vacant_storage"

echo $vacant_storage

echo "Displaying the contents of array mapped from csv file:"
index=0
for record in "${arr_csv[@]}"
do
    echo "Record at index-${index} : $record"
	((index++))
done
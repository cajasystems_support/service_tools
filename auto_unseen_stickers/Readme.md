The perpose of this script is to get all reported unseen stickers by the running robots

NOTE: Currently the script will only works on Service servers

Copy auto_unseen_stickers folder to the home folder (~ or $HOME) of the service server using WinSCP

Grant running permission to the script file
$ chmod +x auto_unseen_stickers.sh

Create a new crontab job for ubuntu user
$ sudo crontab -u ubuntu -e

Add the following line
# Run auto_unseen_stickers script at 9:00, 12:00 and 15:00 on Sun to Fri
0 9,12,15 * * 0,1,2,3,4,5 /home/ubuntu/auto_unseen_stickers/auto_unseen_stickers.sh >> /home/ubuntu/auto_unseen_stickers/archive/auto_unseen_stickers_`date +\%s`.log

Save changes and exit cron, you should get the below message:
crontab: installing new crontab

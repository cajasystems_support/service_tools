#!/bin/bash

parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

run_sql() {
  sql_query=$1
  PARAM1=$2
  PARAM2=$3

  db_yml="/home/ubuntu/.config/database.yml"
  eval $(parse_yaml $db_yml)

  export PGPASSWORD=$pgsql_pass

  case $PARAM1 in
    CSV)  Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -c "${sql_query}"`     ;;
    CCSV) Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c "${sql_query}"`  ;;
    FILE) Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c -f $PARAM2`      ;;
       *) Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -F , -c "${sql_query}"`        ;;
  esac

  if [[ -n "$Database"  ]]
  then
    echo "$Database"
  fi
}

working_machines_db() {
  sql_query="
    select machineid,ip
    from   machines
    where reported_status != 'SLEEPING'
      and status != 'OFFLINE'
    order by machineid"

  run_sql "$sql_query" CCSV
}

stickers_by_alias_db() {
  stickersAliasList=$1

  sql_query="
    select alias,code
    from stickers
    where alias in ($stickersAliasList)"

  run_sql "$sql_query" CCSV
}

unseen_stickers_server() {
  unseen_stickers_robot() {
    allFlag=$1

    RED='\033[1;31m'     # Red Color
    NC='\033[0m'         # No Color

    hostname=$(hostname)

    if [[ $allFlag != "all" ]]
    then
      echo -e "\nUnseen stickers for: ${RED}$hostname${NC}\n"
    fi

    # Make it lowercase
    hostname=${hostname,,}

    log_name=/caja/logs/rmanager_$hostname.log

    if [[ $allFlag = "all" ]]
    then
      unseen=$(grep unseen $log_name | cut -d : -f 4 | grep -v "\[\]}" | sed 's/[][{}"]//g' 2>/dev/null)
      echo -e "$unseen "
    else
      echo -e "----------------"
      echo -e "Sticker\t| Count"
      echo -e "----------------"
      grep unseen $log_name | cut -d : -f 4 | grep -v "\[\]}" | sed 's/[][{}"]//g' | tr -s ',' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\t| %s\n",$2,$1)}'
      echo
    fi
  }

  unseen_handler() {
    machine_id=$1
    ip=$2
    allFlag=$3

    RED='\033[1;31m'     # Red Color
    NC='\033[0m'         # No Color

    pub_file="$HOME/.ssh/id_rsa_robots"
    ssh_flags="-o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"

    commands=" $(typeset -f) ; unseen_stickers_robot $allFlag"

    if [[ $allFlag = "all" ]]
    then
      ping -w 1 $ip > /dev/null 2>&1
    else
      ping -w 1 $machine_id > /dev/null 2>&1
    fi
    ping_ret_val=$?

    if [[ $ping_ret_val -ne 0 ]]
    then
      echo -ne "\tRobot ${RED}$machine_id${NC} is unreachable" 1>&2
    else
      if [[ $allFlag = "all" ]]
      then
        ssh -t -n $ssh_flags caja@$ip "$commands"
      else
        ssh -t -n $ssh_flags $machine_id "$commands"
      fi
    fi
  }

  machine_id=$1

  if [[ $machine_id = "all" ]]
  then
    # Add "| head -10" to shorten the robot list to 10 robots
    workingRobots=$(working_machines_db)

    if [[ $workingRobots = "" ]]
    then
      echo -e "No registered robots found at the moment\n"
    else
      echo -e "Registered robots\n"
      echo -e "--------------------------------"
      echo -e "Robot ID\t| IP"
      echo -e "--------------------------------"
      echo $workingRobots | tr -s ' ' '\n' | awk -F "," '{printf("%s\t| %s\n",$1,$2)}'

      echo -e "\nLooking for unseen stickers in the above robots list\n"
      # For each machine found in /etc/hosts call unseen_handler
      i=0
      while IFS=, read -r robot ip
      do
        ((i++))
        echo -ne "$robot"
        # parse robot name for script use, remove prefix i.e BB2 or OP1
        robot=${robot:3}
        # Make it lowercase
        robot=${robot,,}

        unseen+=$(unseen_handler $robot $ip $machine_id)
        echo
      done <<< "$workingRobots"

      if [[ $unseen = "" ]]
      then
        echo -e "No unseen stickers found at the moment\n"
      else
        SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
        ts=$(date +%s)

        stickersAliasList=$(echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\n",$2)}' | sort -n | awk '{printf("%s,",$1)}')
        # Remove last comma from string
        stickersAliasList=${stickersAliasList::-1}
        echo -e "\nStickers Alias List\n"
        echo -e "$stickersAliasList\n"

        echo -e "Unseen\n"
        echo -e $(echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n')

        echo -e "\nStickers Codes List\n"
        echo -e "------------------"
        echo -e "Alias\t| Code"
        echo -e "------------------"
        stickersCodesList=$(stickers_by_alias_db $stickersAliasList)
        echo -e "$stickersCodesList\n" | tr -s ',' ' ' | sort | awk '{printf("%s\t| %s\n",$1,$2)}'
        echo -e "$stickersCodesList" | tr -s ',' ' ' | sort > $SCRIPT_DIR/file2

        echo -e "\n----------------"
        echo -e "Sticker\t| Count"
        echo -e "----------------"
        echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\t| %s\n",$2,$1)}'
        echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s %s\n",$2,$1)}' > $SCRIPT_DIR/file1
        echo

        echo -e "\n--------------------------------"
        printf "%s\t| %-10s\t| %s\n" "Alias" "Code" "Count"
        #echo -e "Alias\t| Code\t| Count"
        echo -e "--------------------------------"
        join -j 1 <(sort $SCRIPT_DIR/file1) <(sort $SCRIPT_DIR/file2) | sort -nr -k2 -k1 | awk '{printf("%s\t| %-10s\t| %s\n",$1,$3,$2)}'

        echo -e "Alias,Code,Count" > $SCRIPT_DIR/archive/unseen_stickers_$ts.csv
        join -j 1 <(sort $SCRIPT_DIR/file1) <(sort $SCRIPT_DIR/file2) | sort -nr -k2 -k1 | awk '{printf("%s,%s,%s\n",$1,$3,$2)}' >> $SCRIPT_DIR/archive/unseen_stickers_$ts.csv
        echo

        rm $SCRIPT_DIR/file1 $SCRIPT_DIR/file2
      fi
    fi
  else
    unseen_handler $machine_id
  fi
}

# This command will be running on the server
unseen_stickers_server all

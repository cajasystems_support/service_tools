#!/bin/bash

swgr_check_be_version() {
  swgr_be_ver=$(curl -s -X POST --header "Content-Type: application/json" --header "Accept: application/json;charset=utf-8" -d "{ \"userName\": \"$UI_user\", \"userPassword\": \"$UI_password\" }" "http://$server_ip:8082/users/login" | grep "ERROR 404" > /dev/null && echo 5.10 || echo 5.7)

  case $swgr_be_ver in
    5.10) admin="admin/" ; $verbose && echo "swgr_be_ver: $swgr_be_ver+" ;;
    5.7)  admin=""       ; $verbose && echo "swgr_be_ver: $swgr_be_ver-";;
  esac
  $verbose && echo "admin: $admin"
}

swgr_get_token() {
  $verbose && echo "url: http://$server_ip:8082/${admin}users/login "
  access_token_id=$(curl -s -X POST --header "Content-Type: application/json" --header "Accept: application/json;charset=utf-8" -d "{ \"userName\": \"$UI_user\", \"userPassword\": \"$UI_password\" }" "http://$server_ip:8082/${admin}users/login" | awk -F\" '{ print $6 }')
  $verbose && echo "Authorization Token: $access_token_id"
}

swgr_bulkOrdersRelevancy_usage() {
  echo -e "Usage: $0 <Server name> <Server IP> <originalRelevancyTime (DD-MM-YYYY_HH:MM)> <targetRelevancyTime (DD-MM-YYYY_HH:MM)>" >&2
  exit 1
}

validate_server_ip() {
  ip=$1

  if ! [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
  then
    echo -e "The provided IP ${RED}$ip${NC} is not valid\n"
    exit 1
  fi

  swagger_status=$(nmap $server_ip -PN -p 8082 | egrep 'open|closed|filtered' | awk -F" " '{print $2}')
  $verbose && echo swagger_status=$swagger_status

  #ping -w 1 $ip > /dev/null 2>&1
  #ping_ret_val=$?

  if [[ $swagger_status != "open" ]]
  then
    echo -e "Swagger for IP address ${RED}$ip${NC} is unreachable status is $swagger_status, Aborting." 1>&2
    exit 1
  fi
}

validate_and_set_relevancy(){
  relevancy=$1

  #if [[ $relevancy =~ ^[0-9]{2}-[.]{3}-[0-9]{4}_[0-9]{2}:[0-9]{2}$ ]]
  if [[ $relevancy =~ ^[0-9]{1,2}-.{2,3}-[0-9]{4}_[0-9]{2}:[0-9]{2}$ ]]
  then
    echo -e "Date $relevancy is in a valid date format DD-MM-YYYY_HH:MM"
  else 
    echo -e "Date $relevancy is ${RED}NOT${NC} in a valid date format DD-MM-YYYY_HH:MM"
    swgr_bulkOrdersRelevancy_usage
  fi

  IFS=_ read -r relevancyDate relevancyTime <<< $relevancy
  IFS=- read -r DD MM YYYY <<< $relevancyDate

  $verbose && echo "date -d: $YYYY $DD $MM"

  #relevancyDate=$(tr '-' '/' <<<"$relevancyDate")
  # In case MM is digits only and not Jan-Dec
  if [[ $MM =~ [0-9]{2} ]]
  then
    date_format="$MM/$DD/$YYYY"
  else
    date_format="$MM $DD $YYYY"
  fi

  $verbose && echo date_format=$date_format

  date -d "$date_format" > /dev/null 2>&1
  is_valid=$?

  $verbose && echo is_valid=$is_valid

  # Validate relevancyDate
  if [ $is_valid -eq 0 ]
  then
    echo -e "Date $relevancyDate is a valid date"
  else
    echo -e "Date $relevancyDate is ${RED}NOT${NC} a valid date"
    swgr_bulkOrdersRelevancy_usage
  fi

  relevancyDate=$(date "+%d-%m-%Y" -d "$date_format")
  $verbose && echo relevancyDate=$relevancyDate

  # Validate relevancyTime
  if $(date "+%H:%M" -d "$relevancyTime" > /dev/null 2>&1)
  then
    echo -e "Time $relevancyTime is a valid time"
  else
    echo -e "Time $relevancyTime is ${RED}NOT${NC} a valid time"
    swgr_bulkOrdersRelevancy_usage
  fi

  $verbose && echo relevancyTime=$relevancyTime

  IFS=: read -r HH MIN <<< $relevancyTime

  IFS=- read -r DD MM YYYY <<< $relevancyDate
  relevancyEpoch=$(date "+%s" -d "$MM/$DD/$YYYY $relevancyTime")
  $verbose && echo relevancyEpoch=$relevancyEpoch
}

swgr_set_print_vars() {
  UI_user="ADMIN"
  UI_password="ADMIN"

  $verbose && [ ! -z "$server_ip"   ] && echo "server_ip:   $server_ip"
  $verbose && [ ! -z "$UI_user"     ] && echo "UI_user:     $UI_user"
  $verbose && [ ! -z "$UI_password" ] && echo "UI_password: $UI_password"
}

moveRelevancy() {
  echo -e "\n${BLINK}Moving relevancy time.${NC}\n"

  res=$(curl -s -X PUT --header "Content-Type: application/json" --header "Accept: application/json;charset=utf-8" --header "Authorization: Bearer $access_token_id" -d "{}" "http://$server_ip:8082/${admin}orders/relevancy?originalRelevancyTime=${oldRelevancyDate}%20${oldHH}%3A${oldMM}&targetRelevancyTime=${newRelevancyDate}%20${newHH}%3A${newMM}")

  $verbose && echo $res
  # parse error code from result
  error_code=$(echo -e $res | awk -F"," {'print $1'} | cut -f2 -d":")
  echo -e "Error code: ${RED}$error_code${NC}"

  [ $error_code = 200 ] && echo -e "Relevancy time changed ${RED}successfully${NC}.\n" || echo -e "Relevancy time changed ${RED}failed${NC}.\n"
}

doInteractiveRelevancy() {
  $verbose && echo -e "${RED}Interactive mode${NC}"
  $verbose && echo -e "Command: curl -X PUT --header \"Content-Type: application/json\" --header \"Accept: application/json;charset=utf-8\" --header \"Authorization: Bearer $access_token_id\" -d \"{}\" \"http://$server_ip:8082/${admin}orders/relevancy?originalRelevancyTime=${oldRelevancyDate}%20${oldHH}%3A${oldMM}&targetRelevancyTime=${newRelevancyDate}%20${newHH}%3A${newMM}\"\n"

  echo -e "\n${RED}Make sure the WH is in STOP or SHUTDOWN state ${NC}\n"
  echo -e "Are you sure you would like to Change relevancy time from $oldRelevancyDate $oldRelevancyTime to $newRelevancyDate $newRelevancyTime? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    moveRelevancy
  else
    echo -e "\nAborted."
  fi
}

doBatchRelevancy() {
  $verbose && echo -e "${RED}Batch mode${NC}"
  $verbose && echo -e "Command: curl -X PUT --header \"Content-Type: application/json\" --header \"Accept: application/json;charset=utf-8\" --header \"Authorization: Bearer $access_token_id\" -d \"{}\" \"http://$server_ip:8082/${admin}orders/relevancy?originalRelevancyTime=${oldRelevancyDate}%20${oldHH}%3A${oldMM}&targetRelevancyTime=${newRelevancyDate}%20${newHH}%3A${newMM}\"\n"
  moveRelevancy
}

swgr_bulkOrdersRelevancy() {
  oldRelevancy=$1
  newRelevancy=$2

  if [ $# -ne 2 ] 
  then 
    swgr_bulkOrdersRelevancy_usage
    exit 1
  fi

  echo -e "\nSwagger of ${RED}$server_name${NC} with IP ${RED}$server_ip${NC}"

  validate_and_set_relevancy $oldRelevancy
  oldRelevancyDate=$relevancyDate
  oldRelevancyTime=$relevancyTime
  oldHH=$HH
  oldMM=$MIN
  oldRelevancyEpoch=$relevancyEpoch

  validate_and_set_relevancy $newRelevancy
  newRelevancyDate=$relevancyDate
  newRelevancyTime=$relevancyTime
  newHH=$HH
  newMM=$MIN
  newRelevancyEpoch=$relevancyEpoch

  swgr_set_print_vars

  $verbose && echo "oldRelevancy:       $oldRelevancy"
  $verbose && echo "oldRelevancyDate:   $oldRelevancyDate"
  $verbose && echo "oldRelevancyTime:   $oldRelevancyTime"
  $verbose && echo "oldHH:              $oldHH"
  $verbose && echo "oldMM:              $oldMM"
  $verbose && echo "oldRelevancyEpoch:  $oldRelevancyEpoch"
  $verbose && echo "newRelevancy:       $newRelevancy"
  $verbose && echo "newRelevancyDate:   $newRelevancyDate"
  $verbose && echo "newRelevancyTime:   $newRelevancyTime"
  $verbose && echo "newHH:              $newHH"
  $verbose && echo "newMM:              $newMM"
  $verbose && echo "newRelevancyEpoch:  $newRelevancyEpoch"

  swgr_check_be_version
  swgr_get_token

  echo ""

  if [[ $batch == true ]]
  then
    doBatchRelevancy
  else
    doInteractiveRelevancy
  fi
}

# Main
RED='\033[1;31m'     # Red Color
NC='\033[0m'         # No Color

# Handle script swiches
verbose=false
batch=false

while getopts ":hvb" option; do
  case $option in
    h) swgr_bulkOrdersRelevancy_usage; exit ;;
    v) verbose=true ;;
    b) batch=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

connect_opts=()
$batch && connect_opts+=( -b )
$verbose && connect_opts+=( -v )
$verbose && echo ops = "${connect_opts[@]}" "$@"

$verbose && echo batch=$batch

if [ "$#" -ne 4 ]
then
  swgr_bulkOrdersRelevancy_usage
  exit 1
fi

server_name=$1
server_ip=$2
oldRelevancy=$3
newRelevancy=$4

$verbose && echo server_name $server_name
$verbose && echo server_ip $server_ip
$verbose && echo oldRelevancy $oldRelevancy
$verbose && echo newRelevancy $newRelevancy

validate_server_ip $server_ip
swgr_bulkOrdersRelevancy $oldRelevancy $newRelevancy

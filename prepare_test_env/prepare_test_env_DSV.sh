#!/bin/bash

swgr_common_variables() {
  SWGR_SUCCESS="200 202"
  write_out=";response_code:%{response_code},errormsg:%{errormsg},exitcode:%{exitcode}\n"
  curl_headers=(-H "Content-Type: application/json" -H "Accept: */*;charset=utf-8")
  $verbose && echo curl_headers="${curl_headers[@]}"
}

swgr_common_variables_dependencies() {
  curl_headers+=(-H "Authorization: Bearer ${access_token_id}")
  $verbose && echo curl_headers="${curl_headers[@]}"
}

swgr_set_print_vars() {
  UI_user="TEST"
  UI_password="TEST"

  $verbose && [ ! -z "$server_ip"   ] && echo "server_ip:   $server_ip"
  $verbose && [ ! -z "$UI_user"     ] && echo "UI_user:     $UI_user"
  $verbose && [ ! -z "$UI_password" ] && echo "UI_password: $UI_password"  
}

swgr_build_url() {
  swgr_url="http://$server_ip:8082/admin/"
  swgr_url_dsv="http://$server_ip:8085/dsvwms/"
  $verbose && echo swgr_url=$swgr_url
  $verbose && echo swgr_url_dsv=$swgr_url_dsv
}

swgr_check_be_version() {
  swgr_be_ver=$(curl -s -X POST "${curl_headers[@]}" -d "{ \"userName\": \"$UI_user\", \"userPassword\": \"$UI_password\" }" "http://$server_ip/users/login" | grep "404" > /dev/null && echo 5.10 || echo 5.7)
  $verbose && echo swgr_be_ver=$swgr_be_ver

  case $swgr_be_ver in
    5.10) admin="admin/" ; $verbose && echo "swgr_be_ver: $swgr_be_ver+" ;;
    5.7)  admin=""       ; $verbose && echo "swgr_be_ver: $swgr_be_ver-";;
  esac
  $verbose && echo "admin: $admin"
  swgr_build_url
}

swgr_get_token() {
  $verbose && echo "url: ${swgr_url}users/login "
  res=$(curl -s -X POST "${curl_headers[@]}" -d "{ \"userName\": \"$UI_user\", \"userPassword\": \"$UI_password\" }" "${swgr_url}users/login")

  swgr_parse_response true
  [ -z $access_token_id ] && echo "Failed to get token ID, aborting." && exit 1

  $verbose && echo "Authorization Token: $access_token_id"
}

swgr_parse_part_response() {
  response_part=$1
  extract_var=$2
  part_type=$3

  case $part_type in
    json) extract_val=$(echo $response_part | jq | tr -d '"{},' | grep $extract_var | awk -F":" {'print $2'}) ;;
    csv)  extract_val=$(echo $response_part | sed 's/\,/\n/g' | grep $extract_var | awk -F":" {'print $2'}) ;;
  esac

  $verbose && $print_response && echo -e "$extract_var: ${RED}$extract_val${NC}" > /dev/tty

  $verbose && echo "${extract_val}"
}

swgr_parse_response() {
  print_response=$1

  # Part1 example: {"httpCode":404,"errorCode":"MACHINE_NOT_FOUND","externalErrorMessage"}
  # Part2 example: response_code:404,errormsg:,exitcode:0
  response_part1=$(echo -e $res | awk -F";" {'print $1'})
  response_part2=$(echo -e $res | awk -F";" {'print $2'})

  $verbose && echo "response_part1: $response_part1"
  $verbose && echo "response_part2: $response_part2"

  declare -A arrayResVars
  arrayResVars[part1]="httpCode errorCode externalErrorMessage userId access_token_id code message"
  arrayResVars[part2]="response_code errormsg exitcode"

  # create variable for each item in response_part1 
  if [[ ! -z "$response_part1" ]]; then
    for var in ${arrayResVars[part1]}
    do
      #arrayResVars[part1,1]=$(printf -v "${var}" $(swgr_parse_part_response "$response_part1" $var json) args)
      #printf -v "${var}" "$(swgr_parse_part_response "$response_part1" $var json)" args
      swgr_parse_part_response "$response_part1" $var json
      $verbose && echo extract_val=${extract_val} > /dev/tty
      printf -v "${var}" "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" ${extract_val}
      $verbose && echo var=${var}
    done
  fi

  # create variable for each item in response_part2
  if [[ ! -z "$response_part2" ]]; then
    for var in ${arrayResVars[part2]}
    do
      #arrayResVars[part2,1]=$(printf -v "${var}" $(swgr_parse_part_response "$response_part2" $var csv) args)
      #printf -v "${var}" $(swgr_parse_part_response "$response_part2" $var csv) args
      swgr_parse_part_response "$response_part2" $var csv
      $verbose && echo extract_val=${extract_val} > /dev/tty
      printf -v "${var}" "%s" ${extract_val}
    done
  fi
}

swgr_dependencies() {
  swgr_common_variables
  swgr_set_print_vars
  #swgr_check_be_version
  swgr_build_url
  swgr_get_token
  swgr_common_variables_dependencies
}

create_accounts() {
  echo -e "\nSwagger create_accounts to ${RED}$server_ip${NC} server"
  swgr_dependencies

  read -p "New Account Name: " newAccountName

  [ -z $newAccountName ] && echo "New account name cannot be blank, aborting" && return 1

  echo -e "Are you sure you would like to create a new account $newAccountName? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n$Creating a new account ${RED}$newAccountName${NC}\n"
    swgr_create_accounts
  else
    echo -e "\nAborted."
  fi
}

swgr_create_accounts() {
  url="${swgr_url}accounts"
  $verbose && echo "url: $url"
  $verbose && echo "curl_headers: ${curl_headers[@]}"

  #curl -X 'POST' \
  #  'http://172.27.10.10:8082/admin/accounts' \
  #  -H 'accept: */*' \
  #  -H 'Authorization: Bearer 8e1bb83b-3072-4d9f-a809-a419042a3fcb' \
  #  -H 'Content-Type: application/json;charset=utf-8' \
  #  -d '{
  #  "externalId": "TEST2",
  #  "name": "TEST2",
  #  "cycleCountEnabled": true,
  #  "cycleCountPeriods": "YEARLY",
  #  "minCycleCountPercentage": 0,
  #  "splitToteHtmlTemplate": null,
  #  "orderCompleteTemplate": null,
  #  "amountForConsolidation": 0
  #}'

  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{
    \"externalId\": \"${newAccountName}\",
    \"name\": \"${newAccountName}\",
    \"cycleCountEnabled\": true,
    \"cycleCountPeriods\": \"YEARLY\",
    \"minCycleCountPercentage\": 0,
    \"splitToteHtmlTemplate\": null,
    \"orderCompleteTemplate\": null,
    \"amountForConsolidation\": 0
  }" "$url")

  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Account ${RED}$newAccountName${NC} was created ${RED}successfully${NC}." || echo -e "Account ${RED}$newAccountName${NC} creation ${RED}failed${NC}. Response: $externalErrorMessage"

}

validate_input() {
  type=$1
  num=$2

  case $type in
    int) [[ ! $num =~ $re ]] && echo "Invalid number, should be a positive number" >&2 && return 1 ;;
    str) [ -z $accountName ] && echo "Account name cannot be blank, aborting" >&2 && return 1
  esac

  return 0
}

swgr_get_accounts() {
  url="${swgr_url}accounts"
  $verbose && echo "url: $url"
  $verbose && echo "curl_headers: ${curl_headers[@]}"

  res=$(curl -s -w "$write_out" -X GET "${curl_headers[@]}" -d "{}" "$url")

  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] || (echo -e "Unable to GET accounts. Code: '$response_code' Response: $externalErrorMessage" ; exit 1)

  accounts=($(jq -r .modelList[].externalId <<< ${res} | tr '\n' ' '))

  $verbose && echo accounts=$accounts
}

create_tags() {
  echo -e "\nSwagger create_tags to ${RED}$server_ip${NC} server"
  swgr_dependencies

  swgr_get_accounts

  PS3="Please select one of the accounts: "
  select account in "${accounts[@]}"; do
    echo "You have chosen $account"
    break
  done

  read -p "Number of TAGs to create: " numTAGs
  validate_input int $numTAGs
  [ $? -ne 0 ] && return 1

  read -p "Start creating from number: " startNum
  validate_input int $startNum
  [ $? -ne 0 ] && return 1

  endNum=$(( startNum + numTAGs -1 ))

  read -p "Quantiy on each TAG: " tagQty
  validate_input int $tagQty
  [ $? -ne 0 ] && return 1

  echo -e "Are you sure you would like to create $numTAGs TAGs with quantity of $tagQty for account $account? (Yy)."
  echo -e "First TAG $startNum, last TAG $endNum"
  read -p "[ ]"$'\b\b' -n 1 -r

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Creating new TAGs.${NC}\n"
    swgr_create_tags
  else
    echo -e "\nAborted."
  fi
}

swgr_create_tags() {
  url="${swgr_url_dsv}tag"
  $verbose && echo "url: $url"
  $verbose && echo "curl_headers: ${curl_headers[@]}"

  for (( i=$startNum; i<=$endNum; i++ ))
  do
    swgr_create_single_tag $i
  done
}

swgr_create_single_tag() {
  count=$1
#curl -X 'POST' \
#  'https://dev.dsv.caja.cloud/dsvwms/tag' \
#  -H 'accept: */*' \
#  -H 'Content-Type: application/json' \
#  -d '{
#  "DSV_WCSMOVORD0310": {
#    "HEAD": {
#      "Message_Id": "string",
#      "Date_Time": "string",
#      "Message_Type": "WCSMOVORD",
#      "Message_Version": "string",
#      "Sender_Id": "string",
#      "Recipient_Id": "string",
#      "MOVE": {
#        "Client_Id": "string",
#        "From_Location_Id": "string",
#        "To_Location_Id": "string",
#        "Final_Location_Id": "string",
#        "MOVDET": {
#          "Sku_Id": "string",
#          "Tag_Id": "string",
#          "Qty": 0,
#          "Key": "string",
#          "First_Key": "string",
#          "SKUDET": {
#            "Description": "string",
#            "EAN": "string",
#            "UPC": "string",
#            "SKUUDF": {
#              "User_Def_Type_1": "string",
#              "User_Def_Type_2": "string",
#              "User_Def_Note_2": "string"
#            }
#          }
#        }
#      }
#    }
#  }
#}'
  zcount=$(printf "%04d" $count)
  echo -en "TAG tag${zcount} with SKU skuCode${zcount} => "

  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{
  \"DSV_WCSMOVORD0310\": {
    \"HEAD\": {
      \"Message_Id\": \"msg${zcount}\",
      \"Date_Time\": \"string\",
      \"Message_Type\": \"WCSMOVORD\",
      \"Message_Version\": \"string\",
      \"Sender_Id\": \"string\",
      \"Recipient_Id\": \"string\",
      \"MOVE\": {
        \"Client_Id\": \"${account}\",
        \"From_Location_Id\": \"from_loc_${zcount}\",
        \"To_Location_Id\": \"to_loc_${zcount}\",
        \"Final_Location_Id\": \"final_loc_${zcount}\",
        \"MOVDET\": {
          \"Sku_Id\": \"skuCode${zcount}\",
          \"Tag_Id\": \"tag${zcount}\",
          \"Qty\": $tagQty,
          \"Key\": \"key${zcount}\",
          \"First_Key\": \"first_key${zcount}\",
          \"SKUDET\": {
            \"Description\": \"descSku${zcount}\",
            \"EAN\": \"upcCode${zcount}\",
            \"UPC\": \"upcCode${zcount}\",
            \"SKUUDF\": {
              \"User_Def_Type_1\": \"string\",
              \"User_Def_Type_2\": \"string\",
              \"User_Def_Note_2\": \"string\"
            }
          }
        }
      }
    }
  }
  }" "$url")

  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "successfully created" || echo -e "failed"
}

swgr_get_sku_by_account() {
  #curl -X 'PUT' \
  #  'http://172.27.10.10:8082/admin/skus/search?subCode=TEST&pageNum=0&pageSize=25' \
  #  -H 'accept: */*' \
  #  -H 'Authorization: Bearer 6f1d2ef6-af31-4ed3-b5c5-09069782994d' \
  #  -H 'Content-Type: application/json' \
  #  -d '{
  #}'

  url="${swgr_url}skus/search?subCode=$account&pageNum=0&pageSize=100"
  $verbose && echo "url: $url"
  $verbose && echo "curl_headers: ${curl_headers[@]}"

  res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")

  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Account ${RED}$newAccountName${NC} was created ${RED}successfully${NC}." || echo -e "Account ${RED}$newAccountName${NC} creation ${RED}failed${NC}. Response: $externalErrorMessage"

  skus=($(jq -r .skus[].skuCode <<< ${res} | tr '\n' ' '))

  $verbose && echo skus=$skus
}

create_orders() {
  echo -e "\nSwagger create_orders to ${RED}$server_ip${NC} server"
  swgr_dependencies

  swgr_get_accounts

  PS3="Please select one of the accounts: "
  select account in "${accounts[@]}"; do
    echo "You have chosen $account"
    break
  done

  swgr_get_sku_by_account

  PS3="Please select one of the squs: "
  select skuCode in "${skus[@]}"; do
    echo "You have chosen ($REPLY) $skuCode"
    break
  done

  read -p "New Order Number: " newOrderNumber
  [ -z $newOrderNumber ] && echo "New Order Number cannot be blank, aborting" && return 1

  read -p "SKU quantity: " skuQty
  [ -z $skuQty ] && echo "Count of SKUs cannot be blank, aborting" && return 1

  echo -e "Are you sure you would like to create a new order exid_$newOrderNumber with $skuQty items of $skuCode SKU? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Creating a new order.${NC}\n"
    swgr_create_orders
  else
    echo -e "\nAborted."
  fi
}

swgr_create_orders() {
  url="${swgr_url_dsv}wave"
  $verbose && echo "url: $url"
  $verbose && echo "curl_headers: ${curl_headers[@]}"
  $verbose && echo "newOrderNumber: $newOrderNumber / account: $account / skuCode: $skuCode / skuQty: $skuQty"

  #curl -X 'POST' \
  #  'https://dev.dsv.caja.cloud/dsvwms/wave' \
  #  -H 'accept: */*' \
  #  -H 'Content-Type: application/json' \
  #  -d '{
  #  "DSV_WCSWAVE0420": {
  #    "HEAD": {
  #      "Message_Id": "string",
  #      "Date_Time": "string",
  #      "Message_Type": "WCSMOVORD",
  #      "Message_Version": "string",
  #      "Sender_Id": "string",
  #      "Recipient_Id": "string",
  #      "WAVE": {
  #        "Wave_Id": "string",
  #        "ODH": {
  #          "Order_Id": "string",
  #          "Ship_By_Date": "string",
  #          "ODL": [
  #            {
  #              "Line_Id": "string",
  #              "Sku_Id": "string",
  #              "Qty_Ordered": 0,
  #              "TASK": {
  #                "Qty_To_Move": 0,
  #                "Key": "string",
  #                "Priority": 0
  #              },
  #              "SPCINS": {
  #                "Code": "string",
  #                "Text": "string"
  #              }
  #            }
  #          ]
  #        }
  #      }
  #    }
  #  }
  #}'

  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{
  \"DSV_WCSWAVE0420\": {
    \"HEAD\": {
      \"Message_Id\": \"msg${newOrderNumber}\",
      \"Date_Time\": \"string\",
      \"Message_Type\": \"WCSMOVORD\",
      \"Message_Version\": \"string\",
      \"Sender_Id\": \"string\",
      \"Recipient_Id\": \"string\",
      \"WAVE\": {
        \"Wave_Id\": \"wave${newOrderNumber}\",
        \"ODH\": {
          \"Order_Id\": \"order${newOrderNumber}\",
          \"Ship_By_Date\": \"string\",
          \"ODL\": [
            {
              \"Line_Id\": \"line${newOrderNumber}\",
              \"Sku_Id\": \"$skuCode\",
              \"Qty_Ordered\": $skuQty,
              \"TASK\": {
                \"Qty_To_Move\": $skuQty,
                \"Key\": \"key${newOrderNumber}\",
                \"Priority\": 50
              },
              \"SPCINS\": {
                \"Code\": \"code${newOrderNumber}\",
                \"Text\": \"text${newOrderNumber}\"
              }
            }
          ]
        }
      }
    }
  }
  }" "$url")

  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Order ${RED}e_$newOrderNumber${NC} was created ${RED}successfully${NC}." || echo -e "Order ${RED}e_$newOrderNumber${NC} creation ${RED}failed${NC}. Response: $externalErrorMessage"
}

create_bulk_orders() {
  echo -e "\nSwagger create_bulk_orders to ${RED}$server_ip${NC} server"
  swgr_dependencies

  swgr_get_accounts

  PS3="Please select one of the accounts: "
  select account in "${accounts[@]}"; do
    echo "You have chosen $account"
    break
  done

  read -p "New Order Number: " newOrderNumber
  [ -z $newOrderNumber ] && echo "New Order Number cannot be blank, aborting" && return 1

  read -p "Number of Orders to create: " numOrders
  validate_input int $numOrders
  [ $? -ne 0 ] && return 1

  read -p "Start creating from number: " startNum
  validate_input int $startNum
  [ $? -ne 0 ] && return 1

  endNum=$(( startNum + numOrders -1 ))

  echo -e "Are you sure you would like to create $numOrders Orders for account $account? (Yy)."
  echo -e "First order $startNum, last order $endNum"
  read -p "[ ]"$'\b\b' -n 1 -r

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Creating new Orders.${NC}\n"
    swgr_create_bulk_orders
  else
    echo -e "\nAborted."
  fi
}

swgr_create_bulk_orders() {
  url="${swgr_url}orders"
  $verbose && echo "url: $url"
  $verbose && echo "curl_headers: ${curl_headers[@]}"
  $verbose && echo "newOrderNumber: $newOrderNumber / account: $account / skuCode: $skuCode / skuQty: $skuQty"

  skuQty=1

  for (( i=$startNum; i<=$endNum; i++ ))
  do
    swgr_create_single_order $i
  done
}

swgr_create_single_order() {
  count=$1

  zcount=$(printf "%04d" $count)
  echo -en "Order e_${zcount} / SKU skuCode${zcount} => "

  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{
    \"externalId\": \"e_${newOrderNumber}_${zcount}\",
    \"pickTicket\": \"p_${zcount}\",
    \"skus\": [
      {
        \"externalId\": \"s_${newOrderNumber}_${zcount}\",
        \"skuCode\": \"skuCode${zcount}\",
        \"qty\": $skuQty,
        \"batchId\": \"\",
        \"minimalExpire\": 0
      }
    ],
    \"duedate\": 0,
    \"isSplit\": true,
    \"color\": \"#000000\",
    \"type\": \"WHOLESALE\",
    \"relevancyTime\": 0,
    \"boosted\": 0,
    \"putWallSlotType\": \"REGULAR\",
    \"packInfo\": \"string\",
    \"shift\": 0
  }" "$url")

  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Order e_${newOrderNumber}_${zcount}${NC} was created ${RED}successfully${NC}." || echo -e "Order ${RED}e_${newOrderNumber}_${zcount}${NC} creation ${RED}failed${NC}. Response: $externalErrorMessage"
}

usage() {
  echo "Usage: $0 <Server IP> " >&2
  echo -e "  Server IP   - IP of BE Web server"
  exit 1
}

show_menu() {
  clear

  declare -a items=("Create Accounts" "Create TAGs" "Create Orders" "Create Bulk Orders" "Quit")
  PS3=$'\n ::  Select Swagger resource please : '

  while true; do
    select item in "${items[@]}"
    do
      case $item in
          "Create Accounts")     create_accounts ;;
          "Create TAGs")         create_tags ;;
          "Create Orders")       create_orders ;;
          "Create Bulk Orders")  create_bulk_orders ;;
          "Quit")      echo "Bye bye"
                       exit 0;;
          *)           echo "Invalid selection, please try again";;
      esac
    done
  done
}

### MAIN ###

# Colors for display - will not be reconized on servers
RED='\033[1;31m'     # Red Color
NC='\033[0m'         # No Color
re='^[0-9]+$'

# Handle script swiches
verbose=false

while getopts ":vh" option; do
  case $option in
    h) usage        ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

[ $# -ne 1 ] && usage

server_ip=$1

show_menu

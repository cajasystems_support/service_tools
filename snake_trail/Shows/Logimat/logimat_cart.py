#!/usr/bin/python3

from TestRunner import *
from Tasks import *
"""
# This is an example file of how to build a test that you can run and check later with simulated robot.
# Copy this file and create another test scenario.
  As this example.py, your new test file must be under /caja/caja_ws/src/caja-ros-pkgs/tests/<TEST_FILE>.py
# You can call your class however you want, but need to have exactly one class (that inherits from Mission Runner).
# Don't change __init_, the only functions you can
  (and should) edit are defineFlow, defineExpectedRobotSTMFlow and defineExpectedMissionSTMFlow.
# To test STMs, param "test_mode" should be true (config it in caja_config/config/rmanager/rmanager.yaml).
# Test results logs will be available under /caja/logs/tests/<test-name>-<robot_model>-<map>.log
  and /caja/logs/tests/<test-name>-<robot_model>-<map>_stm.log.
# Currently have been tested only in Binyamina environment.
"""


class ExampleRunner(MissionRunner):
    """A class that holds everything you need in order to run a test.
    Your specific test is defined by defineFlow function - modify it to build the test you want to run.
    defineExpectedRobotSTMFlow and defineExpectedMissionSTMFlow are here to define the expected State Machines flow
    during the mission, the tester uses it to validated this.
    # Please look at the notes at the beginning of this file.
    # Under each of the functions, there's an explanation to how to modify it.
    """

    def __init__(self, map, map_name="", model="", machineId="", test_name="", is_real_robot=False):
        super().__init__(map, map_name, model, machineId, test_name, is_real_robot)

    def defineFlow(self):
        self.is_real_robot = True

        self.addMission(routeCells=[
            NavigationHint(stickerCode=1, orientation=90),
            NavigationHint(stickerCode=2, orientation=90),
        ]
            , dest="ST0020101"
            , missionType="RETRIEVE_BIN"
            , isBinOnRobotDuringMission=False
            , mapId="BUMBELBY_NO_BIN_MAP_ID"
        )

        for _ in range(50):
            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0020101"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0020201"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0020201"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0020301"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0020301"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0020102"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0020102"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0020202"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0020202"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0020302"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0020302"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0010101"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0010101"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0010201"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0010201"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0010301"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0010301"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0010102"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0010102"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0010202"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0010202"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0010302"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
                NavigationHint(stickerCode=3, orientation=90),
            ]
                , dest=(15000, 8210, 0)
                , missionType="PRESENT_BIN_TO_PICKER"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=3, orientation=90, isReverse=True),
                NavigationHint(stickerCode=2, orientation=90, isReverse=True),
            ]
                , dest="ST0010302"
                , missionType="DELIVER_BIN"
                , isBinOnRobotDuringMission=True
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

            self.addMission(routeCells=[
                NavigationHint(stickerCode=2, orientation=90),
            ]
                , dest="ST0020101"
                , missionType="RETRIEVE_BIN"
                , isBinOnRobotDuringMission=False
                , mapId="BUMBELBY_NO_BIN_MAP_ID"
            )

def defineExpectedRobotSTMFlow(self):
    """
    define the expected flow of Robot State Machine
    each transition will be added in the following format:
        self.robot_stm_flow.addExpectedTransition(<fromState>, <toState>, <viaEdge>)
    # if you don't want to test Robot STM, uncomment the following line in code (just "return").
    # pay attention, there's a code segment that you shouldn't modify.
    """
    return  # uncomment if you don't want to test Robot STM.
    # vvv Don't modify vvv#
    robotSTM = RobotSTM()

    # states:
    PENDING_INSTRUCTIONS, SEARCHING_STICKER, READY, RECOVERING, SLEEPING, DEEP_SLEEPING, \
    WAKING_UP, QUIESCE, RECOVERED = robotSTM.getSTMStates()

    # edges:
    FIND_STICKER, WAKEUP_STICKER_FOUND, RECOVERY_NEEDED, SLEEP, RECOVERY_TO_READY, \
    RECOVERY_ENDED, TURN_OFF_ENGINES, WAKEUP, MISSION_STOPPED, QUIESCE_ENDED = robotSTM.getSTMEdges()
    # ^^^ Don't modify ^^^#

    # to see all possible transitions, watch Robot State Machine."
    self.robot_stm_flow.addExpectedTransition(PENDING_INSTRUCTIONS, SEARCHING_STICKER, FIND_STICKER)
    self.robot_stm_flow.addExpectedTransition(SEARCHING_STICKER, READY, WAKEUP_STICKER_FOUND)


def defineExpectedMissionSTMFlow(self):
    """
        define the expected flow of Mission State Machine
    each transition will be added in the following format:
        self.mission_stm_flow.addExpectedTransition(<fromState>, <toState>, <viaEdge>)
    # if you don't want to test Mission STM, uncomment the following line in code (just "return").
    # pay attention, there's a code segment that you shouldn't modify.
    """
    return  # uncomment if you don't want to test Mission STM.
    # vvv Don't modify vvv#
    missionSTM = MissionSTM()

    # states:
    NONE, ONGOING, COMPLETED, CANCELED, ONGOING_ON_DEST, CANCELING, FAILING, STOPPED_ON_DEST, \
    STOPPED, NEED_MAINTENANCE, RECOVERING, RECOVERED = missionSTM.getSTMStates()

    # edges:
    GOT_NEXT_RESPONSE, SUCCEEDED, ARRIVED_TO_DEST, STOP, COMMAND_FAILED, DROP, MISSION_FAILED_ON_DEST, MISSION_FAILED, \
    GOT_REFRESH_RESPONSE, ABORT_AND_MAINTENANCE, CANCEL_MISSION_IMMEDIATELY, CANCEL_MISSION_WHEN_ON_DEST, \
    RECOVERY_NEEDED, RECOVERY_ENDED, RECOVERY_TO_STOP, RECOVERY_REFRESH_RESPONSE = missionSTM.getSTMEdges()
    # ^^^ Don't modify ^^^#

    # to see all possible transitions, watch Mission State Machine."
    self.mission_stm_flow.addExpectedTransition(NONE, ONGOING, GOT_NEXT_RESPONSE)
    self.mission_stm_flow.addExpectedTransition(ONGOING, ONGOING_ON_DEST, ARRIVED_TO_DEST)
    self.mission_stm_flow.addExpectedTransition(ONGOING_ON_DEST, COMPLETED, SUCCEEDED)
    self.mission_stm_flow.addExpectedTransition(COMPLETED, ONGOING, GOT_NEXT_RESPONSE)

#!/bin/bash

parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

check_be_version() {
  be_ver=$(grep version /usr/local/backend/config/static.yml | cut -d" " -f2)
  be_ver_short=$(echo $be_ver | awk -F. '{print $1 "." $2}')
  $verbose && echo be_ver=$be_ver
  $verbose && echo be_ver_short=$be_ver_short

  case $be_ver_short in
    5.10 | 6.0) 
        be_ver_5_10=true ;;
    *)  be_ver_5_10=false
  esac

  $verbose && ($be_ver_5_10 && echo "be_ver: 5.10+" || echo "be_ver: 5.10-")
}

restart_server() {
  verbose=$1
  serverType=$2
  check_be_version

  echo -e "\nRestarting Services..."
  $be_ver_5_10 && /usr/local/backend/bin/restart || /usr/local/backend/restart.sh
  [ $? -eq 0 ] && echo "Restart services succeed" || echo "Restart services failed"

  sleep 2
  echo -e "\nServices Status:"
  services_status_srv $serverType
}

restart_services() {
  echo -e "\nCurrent Services Status:"
  services_status

  echo -e "\n${RED}Make sure the WH is in STOP or SHUTDOWN state ${NC}\n"
  echo -e "Are you sure you would like to restart the server services? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Restarting services on server.${NC}\n"
    exec_commands_over_ssh "restart_server $verbose $serverType"
  else
    echo -e "\nAborted."
  fi
}

exec_commands_over_ssh() {
  in_run_func=$1
  in_param1=$2
  in_param2=$3
  in_param3=$4
  in_param4=$5

  commands=" $(typeset -f $typeset_funcs $in_run_func) ; $in_run_func $opt \"$in_param1\" \"$in_param2\" \"$in_param3\" \"$in_param4\""

  # the ssh -n flag is used to not breaking a while loop that calls the ssh
  sshpass -f fifo ssh $loop -t $ssh_flags -i $connect_str "$commands"
}

run_sql() {
  opt=$1
  sql_query=$2
  PARAM1=$3
  PARAM2=$4
  SITE=$(echo $opt | sed 's/\_[^_]*$//')

  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
  user=$(echo $SCRIPT_DIR | awk -F/ '{print $3'})

  # path /home/ubuntu is being used and not $HOME so every running user will be able to access this file
  # also the full path is being used because the run_sql function can be called through ssh or directly
  db_yml="/home/$user/.config/database_$SITE.yml"
  eval $(parse_yaml $db_yml)

  export PGPASSWORD=$pgsql_pass

  dbCmd="psql -h $pgsql_host -d $pgsql_name -U $pgsql_user"

  case $PARAM1 in
    CSV)  Database=$($dbCmd -A -F , -c "${sql_query}")     ;;
    CCSV) Database=$($dbCmd -A -F , -t -c "${sql_query}")  ;;
    FILE) Database=$($dbCmd -A -F , -t -c -f $PARAM2)      ;;
       *) Database=$($dbCmd -F , -c "${sql_query}")        ;;
  esac

  [ -n "$Database" ] && echo "$Database"
}

htop() {
  htop_option=$1
  $verbose && echo htop_option=$htop_option

  case $htop_option in
    Java) exec_commands_over_ssh "/usr/bin/htop -p \$(pgrep -d',' java)" ;;
       *) exec_commands_over_ssh "/usr/bin/htop" ;;
  esac
}

machines_status() {
  sql_query="
    select machineid,ip,baterylevel,action,status,currentsticker
    from   machines
    where reported_status != 'SLEEPING'
      and status != 'OFFLINE'
    order by status,machineid"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

stickers_list() {
  Sticker=$1

  if [[ $Sticker == "" ]]
  then
    sql_query="
      select alias,code
      from   stickers
      order by 1"
  else
    if [[ $Sticker =~ ^[0-9]+$ ]]
    then
      sql_query="
        select alias,code
        from   stickers
        where  alias=$Sticker
        order by 1"
    else
      sql_query="
        select alias,code
        from   stickers
        where  code='$Sticker'
        order by 1"
    fi
  fi

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

off_machines_errs() {
  sql_query="
    select machineid, errors
    from machines
    where status='OFFLINE'
    order by machineid"

  echo -e "\nErrors of Offline machines\n"
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

off_machines_missions() {
  sql_query="
    select m2.machine_id, m2.status, m2.type, m2.reported_status
    from machines m
    join missions m2 on m.machineid = m2.machine_id
    where m.status='OFFLINE'
      and m2.status not in ('PROCESSED', 'CANCELED')
    order by machineid"

  echo -e "\nMissions of Offline machines\n"
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

noisy_robots() {
  exec_commands_over_ssh "noisy_robots_srv"
}

noisy_robots_srv() {
  RED='\033[1;31m'     # Red Color
  NC='\033[0m'         # No Color

  echo -e "Showing ${RED}KA ERROR${NC} from machinesServer.log as they appears\n"

  tail -F machinesServer.log | grep --line-buffered ERROR | awk -F'[ /]' '{print $2 "\t" $3 "\t" $4 "\t" $10 "\t[" $11 "\t" $22 "\t" $27}'
}

bad_locations() {
  CSV=$1

  sql_query="
    SELECT bins.barcode, frames.id,frames.frametype,frames.code,frames.isvacant,frames.affected_sticker
    FROM bins INNER JOIN frames on frames.x = bins.x and frames.y = bins.y and frames.z = bins.z
    WHERE bins.isdeleted = false
      AND frames.accessible = false
      AND frames.disable_reason = 'BAD_LOCATION_ERRORS'"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

Validate_time_interval() {
  HOUR_MIN=$1
  interval=$2

  # if interval is empty use default 1 hour
  if [[ -z $interval ]]
  then
    case $HOUR_MIN in
      HOURS)   echo -e "Input time interval is empty. Using ${RED}1${NC} hour for interval\n"     >&2 ; interval=1  ;;
      MINUTES) echo -e "Input time interval is empty. Using ${RED}60${NC} minutes for interval\n" >&2 ; interval=60 ;;
    esac
  else
    re='^[0-9]+$'

    if ! [[ $interval =~ $re ]]
    then
      case $HOUR_MIN in
        HOURS)   echo -e "\nInvalid parameter $interval. Expected interval in hours \n" >&2          ; exit 1 ;;
        MINUTES) echo -e "Invalid parameter $time_interval_min. Expected interval in minutes \n" >&2 ; exit 1 ;;
      esac
    fi
  fi

  echo $interval
}

errors_stopped_wh() {
  time_interval_min=$(Validate_time_interval MINUTES $1)
  CSV=$2

  now_timestamp_ms=$(date +%s%3N)
  timestamp_interval_ms=$((now_timestamp_ms - $time_interval_min * 60 * 1000))
  echo -e "Using timestamp ${RED}$timestamp_interval_ms${NC} as interval"

  sql_query="
    select machines.machineid,
           count(*) filter (where error_notifications.error_action = 'STOP_WAREHOUSE') as stop_warehouse,
           count(*) filter (where error_notifications.error_action = 'STOP_AND_RESUME_WAREHOUSE') as stop_and_resume_warehouse,
           count(*) filter (where error_notifications.error_action = 'GIVE_TIME_TO_RECOVER') as give_time_to_recover
    from machines
    left outer join error_notifications on error_notifications.machine_id = machines.machineid
    where error_notifications.created_on > $timestamp_interval_ms
      and error_notifications.status = 'EXISTS'
    group by machines.machineid
    order by stop_warehouse,stop_and_resume_warehouse"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

errors_by_network() {
  time_interval_min=$(Validate_time_interval MINUTES $1)
  CSV=$2

  now_timestamp_ms=$(date +%s%3N)
  timestamp_interval_ms=$((now_timestamp_ms - $time_interval_min * 60 * 1000))
  echo -e "Using timestamp ${RED}$timestamp_interval_ms${NC} as interval"

  sql_query="
    select machines.machineid as machine_id,
           count(*) filter (where error_notifications.name = 'KA response exceeded timeout') as exceeded_timeout,
           count(*) filter (where error_notifications.name = 'Connection was lost with machine') as connection_lost
    from machines
    left outer join error_notifications on machines.machineid = error_notifications.machine_id
      and error_notifications.created_on > $timestamp_interval_ms
      and error_notifications.status = 'EXISTS'
    group by machines.machineid
    order by connection_lost,exceeded_timeout"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

skus_zero_qty() {
  check_pkg_installed xsel
  # Copy from clipboard to Products.csv file
  # In some environments xsel is working only when setting DISPLAY environment parameter 
  export DISPLAY=:0
  xsel -ob > products.csv

  if [ -s products.csv ]
  then
    # The file is not-empty.

    # Parse procducts.csv: replace CRLF with comma and add quote before and after comma
    #products=$(tr -s '\r\n' "," < products.csv | sed -e 's/,$/\n/' | sed -e "s/,/\\\',\\\'/g")
    products=$(tr -s '\r\n' "," < products.csv | sed -e 's/,$/\n/' | sed -e "s/,/\\',\\'/g")
    # add missing quote before the first argument and after the last argument
    #products1="\'$products\'"
    products1="'$products'"
    echo -e products1=$products1

    sql_query="
      select *
      from skus
      where code in ($products1)
        and QTY = 0"

    echo
    exec_commands_over_ssh run_sql "$sql_query" $CSV
    echo
  else
    # The file is empty.
    echo "The clipboard is empty, please copy products list from excel column into clipboard"
  fi
}

check_order() {
  order_number=$1
  CSV=$2

  if [[ $order_number = "" ]]
  then
    echo -e "\nPlease run check_order with ${RED}order number${NC}\n"
    exit 1
  else
    check_order_db
  fi
}

check_order_db() {
  sql_query="
    select b2.barcode, b2.status, b2.isavailable, b2.isdeleted,
           b2.locked_for_recall, b2.accessible, b2.checked_for_consolidation,
           o.isdeleted, o.tote_ids, o.completed, o2.qty, o2.skuid, o2.sku_code,
           f.code, f.isavailable, f.isdeleted
    from orders o
      join orderskus o2 on o.id = o2.orderid
      join binsskus b on b.skuid = o2.skuid
      join bins b2 on b.binid = b2.id
      join frames f on b2.frame_code = f.code
    where o.externalid = '$order_number'
      and (f.isavailable = false or b2.locked_for_recall  = true)"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

all_machines() {
  CSV=$1

  sql_query="
    select machineid,ip,status,reported_status,currentmissionid,currentsticker
    from machines
    order by 4,1"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

services_status() {
  exec_commands_over_ssh "services_status_srv $serverType"
}

services_status_srv() {
  serverType=$1

  RED='\033[1;31m'     # Red Color
  NC='\033[0m'         # No Color

  echo -e "\nRunning ${RED}Caja${NC} Services\n"

  case $serverType in
    WEB | JOB | ROUTE)
      ps -u ec2-user -o pid,user,%mem,%cpu,command ax | head -1 | awk '{printf $1 "\t" $2 "\t\t" $3 "\t" $4 "\t" $NF "\n"}' && ps -u ec2-user -o pid,user,%mem,%cpu,command ax | grep .jar | grep -v grep | awk -v r=${RED} -v nc=${NC} '{printf $1 "\t" $2 "\t" $3 "\t" $4 "\t" r $NF nc "\n"}' | sort -b -g -k3 -r ;;
  esac

  echo -e "\nNumber of running services: ${RED}$(ps -ef | grep .jar | grep -v grep | wc -l)${NC}\n"
  echo
}

unseen_stickers() {
  machine_id=$1
  CSV=$2

  if [[ $machine_id = "" ]]
  then
    echo -e "\nPlease run unseen_stickers with ${RED}machine id${NC}\n"
    exit 1
  else
    # This command will be running on the server
    echo
    exec_commands_over_ssh "unseen_stickers_server $opt $machine_id $CSV"
    echo
  fi
}

working_machines_db() {
  # must get opt as parameter do not remove
  opt=$1

  sql_query="
    select machineid,ip
    from   machines
    where reported_status != 'SLEEPING'
      and status != 'OFFLINE'
    order by machineid"

  run_sql $opt "$sql_query" CCSV
}

stickers_by_alias_db() {
  opt=$1
  stickersAliasList=$2

  sql_query="
    select alias,code
    from stickers
    where alias in ($stickersAliasList)"

  run_sql $opt "$sql_query" CCSV
}

never_seen_stickers_db() {
  opt=$1
  stickersAliasList=$2

  sql_query="
    select alias,code
    from stickers
    where alias not in ($stickersAliasList)"

  run_sql $opt "$sql_query" CCSV
}

unseen_stickers_server() {
  # Function unseen_stickers_robot will run on the robot
  unseen_stickers_robot() {
    allFlag=$1

    RED='\033[1;31m'     # Red Color
    NC='\033[0m'         # No Color

    hostname=$(hostname)
 
    if [[ $allFlag != "all" ]]
    then
      echo -e "\nUnseen stickers for: ${RED}$hostname${NC}\n"
    fi

    # Make it lowercase
    hostname=${hostname,,}

    log_name=/caja/logs/rmanager_$hostname.log

    if [[ $allFlag = "all" ]]
    then
      #unseen=$(grep unseen $log_name | cut -d : -f 4 | grep -v "\[\]}" | sed 's/[][{}"]//g' 2>/dev/null)
      unseen=$(grep unseen $log_name | awk -F '"unseen"' '{print $2}' | grep -v "\[\]}" | sed 's/[][{}:"]//g' 2>/dev/null)
      seen=$(grep unseen $log_name |  awk -F '"seen"' '{print $2}' | awk -F '"unseen"' '{print $1}' | grep -v "\[\]}" | sed 's/[][{}:"]//g' 2>/dev/null)
      echo -e "$seen;$unseen "
    else
      echo -e "----------------"
      echo -e "Sticker\t| Count"
      echo -e "----------------"
      #grep unseen $log_name | cut -d : -f 4 | grep -v "\[\]}" | sed 's/[][{}"]//g' | tr -s ',' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\t| %s\n",$2,$1)}'
      grep unseen $log_name | awk -F '"unseen"' '{print $2}' | grep -v "\[\]}" | sed 's/[][{}:"]//g' | tr -s ',' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\t| %s\n",$2,$1)}'
      echo
    fi
  }

  # Function unseen_handler will run on the service server
  unseen_handler() {
    machine_id=$1
    ip=$2
    allFlag=$3

    RED='\033[1;31m'     # Red Color
    NC='\033[0m'         # No Color

    pub_file="$HOME/.ssh/id_rsa_robots"
    ssh_flags="-o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"

    commands=" $(typeset -f) ; unseen_stickers_robot $allFlag"

    if [[ $allFlag = "all" ]]
    then
      ping -w 1 $ip > /dev/null 2>&1
    else
      ping -w 1 $machine_id > /dev/null 2>&1
    fi
    ping_ret_val=$?

    if [[ $ping_ret_val -ne 0 ]]
    then
      echo -ne "\tRobot ${RED}$machine_id${NC} is unreachable" 1>&2
    else
      if [[ $allFlag = "all" ]]
      then
        ssh -t -n $ssh_flags caja@$ip "$commands"
      else
        echo -en "\n$machine_id => "
        [[ $machine_id =~ ^BB2*|^OP1* ]] && machine_id=${machineid:3}
        [[ $machine_id =~ ^CT*|^LT* ]]   && machine_id=${machineid:0:2}${machineid: -3}

        # Make it lowercase
        machine_id=${machine_id,,}
        echo "$machine_id"

        ssh -t -n $ssh_flags $machine_id "$commands"
      fi
    fi
  }

  ### MAIN OF unseen_stickers_server ###
  opt=$1
  machine_id=$2
  CSV=$3

  if [[ $machine_id = "all" ]]
  then
    # Add "| head -10" to shorten the robot list to 10 robots
    workingRobots=$(working_machines_db $opt)
    echo workingRobots=$workingRobots

    if [[ $workingRobots = "" ]]
    then
      echo -e "No registered robots found at the moment\n"
      exit 1
    else
      echo -e "Registered robots\n"
      echo -e "--------------------------------"
      echo -e "Robot ID\t| IP"
      echo -e "--------------------------------"
      echo $workingRobots | tr -s ' ' '\n' | awk -F "," '{printf("%s\t| %s\n",$1,$2)}'

      echo -e "\nLooking for unseen stickers reported in logs of the above robots list\n"
      # For each machine found in /etc/hosts call unseen_handler
      i=0
      while IFS=, read -r robot ip
      do
        ((i++))
        echo -ne "$robot"
        # parse robot name for script use, remove prefix i.e BB2 or OP1
        #robot=${robot:3}
        [[ $robot =~ ^BB2*|^OP1* ]] && robot=${robot:3}
        [[ $robot =~ ^CT*|^LT* ]]   && robot=${robot:0:2}${robot: -3}
        # Make it lowercase
        robot=${robot,,}

        seen_unseen=$(unseen_handler $robot $ip $machine_id)
        seen+=$(echo $seen_unseen | awk -F ';' '{print $1}')
        unseen+=$(echo $seen_unseen | awk -F ';' '{print $2}')
        echo
      done <<< "$workingRobots"

      if [[ $seen = "" ]]
      then
        echo -e "No seen stickers found at the moment\n"
      else
        seenAliasList=$(echo $seen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\n",$2)}' | sort -n | awk '{printf("%s,",$1)}')
        # Remove last comma from string
        seenAliasList=${seenAliasList::-1}
        echo -e "\nSeen Stickers Alias List\n"
        echo -e "$seenAliasList\n"

        echo -e "Seen\n"
        echo -e $(echo $seen | tr -s ',' '\n' | tr -s ' ' '\n')

        echo -e "\nNever seen Stickers Codes List\n"
        echo -e "------------------"
        echo -e "Alias\t| Code"
        echo -e "------------------"
        neverSeenCodesList=$(never_seen_stickers_db $opt $seenAliasList)
        echo -e "$neverSeenCodesList\n" | tr -s ',' ' ' | sort | awk '{printf("%s\t| %s\n",$1,$2)}'
      fi

      if [[ $unseen = "" ]]
      then
        echo -e "No unseen stickers found at the moment\n"
      else
        unseenAliasList=$(echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\n",$2)}' | sort -n | awk '{printf("%s,",$1)}')
        # Remove last comma from string
        unseenAliasList=${unseenAliasList::-1}
        echo -e "\nUnseen Stickers Alias List\n"
        echo -e "$unseenAliasList\n"

        echo -e "Unseen\n"
        echo -e $(echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n')

        echo -e "\nUnseen Stickers Codes List\n"
        echo -e "------------------"
        echo -e "Alias\t| Code"
        echo -e "------------------"
        unseenCodesList=$(stickers_by_alias_db $opt $unseenAliasList)
        echo -e "$unseenCodesList\n" | tr -s ',' ' ' | sort | awk '{printf("%s\t| %s\n",$1,$2)}'
        echo -e "$unseenCodesList" | tr -s ',' ' ' | sort > ~/file2

        echo -e "\n----------------"
        echo -e "Sticker\t| Count"
        echo -e "----------------"
        echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s\t| %s\n",$2,$1)}'
        echo $unseen | tr -s ',' '\n' | tr -s ' ' '\n' | sort | uniq -c | sort -k1nr | awk '{printf("%s %s\n",$2,$1)}' > ~/file1
        echo

        if [[ $CSV = "CSV" ]]
        then
          printf "%s,%s,%s\n" "Alias" "Code" "Count"
          join -j 1 <(sort file1) <(sort file2) | sort -nr -k2 -k1 | awk '{printf("%s,%s,%s\n",$1,$3,$2)}'
        else
          echo -e "\n--------------------------------"
          printf "%s\t| %-10s\t| %s\n" "Alias" "Code" "Count"
          echo -e "--------------------------------"

          join -j 1 <(sort file1) <(sort file2) | sort -nr -k2 -k1 | awk '{printf("%s\t| %-10s\t| %s\n",$1,$3,$2)}'
        fi
        echo

        rm file1 file2
      fi
    fi
  else
    unseen_handler $machine_id
  fi
}

server_stats() {
  exec_commands_over_ssh "server_stats_srv"
}

server_stats_srv() {
  # Define Colors
  R="\e[0m" #reset
  U="\e[4m" #underline
  Y="\e[33m" #yellow
  RED="\e[37m" #light red

  # Format Date and Time
  DATE=$(date +%d/%m/%Y)
  TIME=$(date +%k:%M:%S)
  DAY=$(date +%A)

  # User Logged In
  USER=$(whoami)

  # System Uptime
  UPTIME_S=$(uptime -s)
  UPTIME_P=$(uptime -p)

  # Free Memory
  FREE1=$(free -m | head -n 1)
  FREE2=$(free -m | head -n 2 | tail -n 1)
  SWAP=$(free -m | tail -n 1)

  # CPU Idle
  CPU=$(vmstat 1 2 | sed -n '/[0-9]/p' | sed -n '2p' | awk '{print $15}')

  # Free Disk Space
  SPACE=$(df -h / | awk '{ a = $2 } { b = $4 } END { print b , "/" ,a , "(Free/total disk)"}')

  # Define banner to print
  banner() {
    echo ----------------------------------------------------------------------
    echo -e "$RED" DAY: "$R"$DAY "$RED"'\t'DATE: "$R"$DATE "$RED"'\t'TIME: "$R"$TIME"$R"
    echo ----------------------------------------------------------------------
  }

  # Define nice-print stats
  print_stats() {
    echo -e "WELCOME:\t|\t$Y$USER$R"
    echo -e "UPTIME: \t|\t$Y$UPTIME_S \t $UPTIME_P$R"
    echo -e "MEMORY: \t|\t$Y$FREE1$R"
    echo -e "        \t|\t$Y$FREE2 MB$R"
    echo -e "        \t|\t$Y$SWAP MB$R"
    echo -e "CPU IDLE:\t|\t$Y$CPU %$R"
    echo -e "DISK SPACE:\t|\t$Y$SPACE$R"
  }

  # Execute Defined Functions
  clear && banner           # Print Banner
  print_stats               # Print Stats
}

del_sku_bins_loc() {
  sku=$1

  if [[ $sku = "" ]]
  then
    echo -e "\nPlease run del_sku_bins_loc with ${RED}SKU number${NC}\n"
    exit 1
  else
    echo
    exec_commands_over_ssh "del_sku_bins_loc_log $sku"
    echo
  fi
}

del_sku_bins_loc_log() {
  sku=$1

  RED='\033[1;31m'     # Red Color
  NC='\033[0m'         # No Color

  echo -e "Looking for SKU ${RED}$sku${NC} bins locations\n"

  grep -w -e "^.*PUT.*bins.*\"$sku\"" adminServer.* archive/adminServer.* > ~/file1

  if [[ -s ~/file1 ]]
  then
    echo -e "-------------------------------------------------------------------------------------------------"
    echo -e "Date\t\t\t| Bin\t\t| X\t| Y\t| Z \t| QTY\t| RES QTY\t| ORG QTY"
    echo -e "-------------------------------------------------------------------------------------------------"

    while IFS=" " read -r p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15
    do
      date="$p4 $p5"
      bin=$(echo $p9 | awk -F"/" '{printf $3}')
      bin=${bin::-1}
      loc=$(echo $p14 | awk -F"[:,]" '{printf $2 "\t| " $4 "\t| " $6 "\t| " $13 "\t| " $15 "\t\t| " $17}')
      echo -e "$date\t| $bin\t| $loc"
    done < ~/file1

    echo
  else
    echo -e "SKU ${RED}$sku${NC} wasn't found in adminServer.log\n"
  fi

  rm ~/file1
}

parse_machine_id() {
  machine_id=$machineid
  
  # parse robot name for script use, remove prefix i.e BB2,OP1,CT02,LT02,LT03
  case $machine_id in
    BB2*  | OP1*  ) machine_id=${machine_id:3} ;; # BB2DUNxxx of OP1ALTxxx: Remove first 3 chars
    CT02* | LT02* ) machine_id=${machine_id:0:2}${machine_id: -3} ;; # CT02_DUN_0xxx or LT02_ALT_0xxx: Take first 2 and last 3 chars = ctxxx of ltxxx
    LT03* )         machine_id=mg${machine_id: -3} ;; # LT03_53_0xxx: Take last 3 chars and add mg at the begining = mgxxx
    CT03* )         machine_id=mc${machine_id: -3} ;; # CT03_20_0xxx: Take last 3 chars and add mg at the begining = mcxxx
  esac

  # Make it lowercase
  machine_id=${machine_id,,}
}

etc_hosts() {
  sql_query="
    select * from (
    select distinct on (ip) ip,machineid
    from machines
    where to_timestamp(lastentry/1000) > now() - interval '30 day'
    order by ip,lastlogin desc
    ) as tb order by machineid asc"

  robotsList=$(exec_commands_over_ssh run_sql "$sql_query" CCSV | tr '\r' '|')
  $verbose && echo -e "\n$robotsList\n"

  echo -e "Getting the list of all known robots and parse it for ${RED}/etc/hosts${NC} use\n"

  echo -e "-----------------------------------------"
  echo -e "ip\t\tmachineid\tssh alias"
  echo -e "-----------------------------------------"

  #while IFS=, read -r ip machineid
  while IFS="," read -d "|" ip machineid p1
  do
    #$verbose && echo "ip=$ip,mid=$machineid"
    parse_machine_id
    echo -e -n "$ip\t${machineid}\t"
    echo -e -n "${machine_id}"
  done <<< "$robotsList"

  echo

  commands="exit"
}

show_pareto() {
  interval=$(Validate_time_interval HOURS $1)
  machine_id=$2
  CSV=$3

  if [[ -z "$interval" ]]
  then
    exit 1
  else
    machine_id=$(echo "$machine_id" | tr '[:lower:]' '[:upper:]')

    interval="'$interval hour'"
    echo -e "Using $interval interval\n"

    PARETO_FILE=$HOME/pareto_query.sql

    if [[ -f "$PARETO_FILE" ]]
    then
      PARETO_QUERY="$(<$PARETO_FILE)"
      PARETO_QUERY="$(echo "${PARETO_QUERY/\$\{interval\}/"$interval"}")"
      PARETO_QUERY="$(echo "${PARETO_QUERY/\$\{machine_id\}/"%$machine_id"}")"

      $verbose && echo -e "\n$PARETO_QUERY\n"

      echo
      exec_commands_over_ssh run_sql "$PARETO_QUERY" $CSV
      echo
    else
      echo -e "Pareto query file $PARETO_FILE was not found\n"
      exit 1
    fi
  fi
}

show_stop_err() {
  CSV=$1

  sql_query_1="
    SELECT started_on
    FROM warehouse_activity_log
    ORDER BY started_on DESC
    LIMIT 1 OFFSET 1"

  sql_query_2="
    SELECT status,
           to_char( to_timestamp((((((( created_on / 1000 ) - 4 * 60 * 60))))) :: DOUBLE PRECISION ), 'yyyy/MM/dd HH24:MI:SS' :: TEXT ) AS "WH_time",
           created_on,
           machine_id,name,description,sticker_code as sticker,bin_code,machine_status,frame_code,warehouse_state as WH_Stat
    FROM error_notifications
    WHERE created_on < ($TIMESTAMP + 10000)
      AND created_on > ($TIMESTAMP - 10000)
    ORDER BY created_on DESC"

  TIMESTAMP=$(exec_commands_over_ssh run_sql "$sql_query_1" CCSV)
  $verbose && echo TIMESTAMP = $TIMESTAMP

  echo
  exec_commands_over_ssh run_sql "$sql_query_2" $CSV
  echo
}

show_sbc_status() {
  Detailed=$1
  [[ $Detailed = "D" ]] && Detailed=true || Detailed=false

  SITE_PAR1=$(echo $opt | sed 's/\_[^_]*$//')
  SITE_PAR2=$(echo $SITE_PAR1 | cut -d_ -f2)
  [ $SITE_PAR1 = $SITE_PAR2 ] && subSite="" || subSite=$SITE_PAR2

  $verbose && echo Detailed=$Detailed
  $verbose && echo SITE_PAR1=$SITE_PAR1
  $verbose && echo SITE_PAR2=$SITE_PAR2
  $verbose && echo subSite=$subSite

  echo
  exec_commands_over_ssh "show_sbc_status_srv $Detailed $subSite"
  echo
}

show_sbc_status_srv() {
  Detailed=$1
  subSite=$2

  RED='\033[1;31m'     # Red Color
  BOLD='\033[1;37m'    # White bold Color
  NC='\033[0m'         # No Color

  pub_file="$HOME/.ssh/id_rsa_robots"
  ssh_flags="-o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"

  workingSBCs=$(grep -i sbc_$subSite /etc/hosts)

  if [[ $workingSBCs = "" ]]
  then
    echo -e "No SBC configuration found in /etc/hosts file\n"
  else
    echo -e "Showing SBCs slots data"

    while IFS=' '  read -r ip SBC sbc
    do
      echo -e "\n${RED}$SBC${NC} ip $ip slots"
    
      ping -w 1 $ip > /dev/null 2>&1
      ping_ret_val=$?
    
      if [[ $ping_ret_val -ne 0 ]]
      then
        echo -ne "SBC is unreachable\n" 1>&2
      else
        if $Detailed
        then
          # Detailed report
          echo -ne "Slot #\t\tCharging\t% Charged\tVoltage\t\t\tFlux\n"
          ssh -t -n $ssh_flags advantech@$ip "tail -12 /caja/logs/sbc_monitor_advantech.log"
        else
          sepLine="+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+"
          # Table report
          echo -e $sepLine
          echo -ne "| "
          for i in $( seq 1 12 ); do printf "${BOLD}  %02d${NC}\t| " "$i" ; done
          echo
          echo -e $sepLine
          echo -ne "| "
          ssh -t -n $ssh_flags advantech@$ip "tail -12 /caja/logs/sbc_monitor_advantech.log | awk -F, '{ if( \$6 == \"100.0\" ) {printf \"\033[1;32m\" \$6 \"\033[0m\" \"\t| \"} else if ( \$6 == \"0.0\" ) {printf \"   \t| \"} else {printf (\"%.5s\t| \",\$6)} }'"
          echo
          echo -e $sepLine
          echo
        fi
      fi
    done <<< "$workingSBCs"
  fi
}

### SWAGGER START ###
swgr_get_resources_list() {
  url="${swgr_url}openapi.json"
  $verbose && echo "url: $url"

  res="$(curl -s -X GET "${curl_headers[@]}" "$url")"
  $verbose && echo "res: $res"

  swgrResources="$(echo $res | jq .paths | grep '"/.*"' | tr -d '{}": ')"
  $verbose && echo -e "swgrResources:\n$swgrResources"
}

swgr_validate_resource() {
  path=$1
  res=$(echo "$swgrResources" | grep "$path")

  [ -n "$res" ] && echo true || echo false
}

swgr_common_variables() {
  SWGR_SUCCESS="200 202"
  write_out=";response_code:%{response_code},errormsg:%{errormsg},exitcode:%{exitcode}\n"
  curl_headers=(-H "Content-Type: application/json" -H "Accept: application/json;charset=utf-8")
  $verbose && echo curl_headers="${curl_headers[@]}"
}

swgr_common_variables_dependencies() {
  curl_headers+=(-H "Authorization: Bearer $access_token_id")
  $verbose && echo curl_headers="${curl_headers[@]}"
}

swgr_set_print_vars() {
  UI_user="SWGR"
  UI_password="SWGR"

  $verbose && [ ! -z "$server_ip"   ] && echo "server_ip:   $server_ip"
  $verbose && [ ! -z "$UI_user"     ] && echo "UI_user:     $UI_user"
  $verbose && [ ! -z "$UI_password" ] && echo "UI_password: $UI_password"
  $verbose && [ ! -z "$Machine_ID"  ] && echo "Machine_ID:  $Machine_ID"
  $verbose && [ ! -z "$Order_ID"    ] && echo "Order_ID:    $Order_ID"
}

swgr_build_url() {
  [ -z $selectedCfgURL ] && swgr_url="http://$server_ip:8082/admin/" || swgr_url="https://$selectedCfgURL/admin/"
  $verbose && echo swgr_url=$swgr_url
}

swgr_check_be_version() {
  swgr_be_ver=$(curl -s -X POST "${curl_headers[@]}" -d "{ \"userName\": \"$UI_user\", \"userPassword\": \"$UI_password\" }" "http://$server_ip/users/login" | grep "404" > /dev/null && echo 5.10 || echo 5.7)
  $verbose && echo swgr_be_ver=$swgr_be_ver

  case $swgr_be_ver in
    5.10) admin="admin/" ; $verbose && echo "swgr_be_ver: $swgr_be_ver+" ;;
    5.7)  admin=""       ; $verbose && echo "swgr_be_ver: $swgr_be_ver-";;
  esac
  $verbose && echo "admin: $admin"
  swgr_build_url
}

swgr_staticSettings() {
  resourcePath="configuration/staticSettings"
  url="${swgr_url}configuration/staticSettings"
  $verbose && echo "url: $url"

  res="$(curl -s -X GET "${curl_headers[@]}" "$url")"
  $verbose && echo "res: $res"

  staticSettings="$(echo $res | jq | tr -d '"{},')"
  $verbose && echo -e "staticSettings:\n$staticSettings"

  siteVersion="$(echo "$staticSettings" | grep version | awk -F": " {'print $2'})"
  $verbose && echo -e "siteVersion: $siteVersion"
}

swgr_get_token() {
  resourcePath="users/login"
  url="${swgr_url}users/login?returnToken=true"
  $verbose && echo "url: $url"
  #access_token_id=$(curl -s -X POST "${curl_headers[@]}" -d "{ \"userName\": \"$UI_user\", \"userPassword\": \"$UI_password\" }" "${swgr_url}users/login" | awk -F\" '{ print $6 }')

  res="$(curl -s -X POST "${curl_headers[@]}" -d "{
    \"userName\": \"$UI_user\",
    \"userPassword\": \"$UI_password\"
  }" "$url")"
  $verbose && echo "res: $(echo $res | jq | tr -d '"{},')"

  access_token_id="$(echo "$res" | jq | tr -d '"{},' | grep token | awk -F": " {'print $2'})"
  $verbose && echo "Authorization Token: $access_token_id"  
}

#swgr_parse_response() {
#  print_response=$1
#
#  # Part1 example: {"httpCode":404,"errorCode":"MACHINE_NOT_FOUND","externalErrorMessage"}
#  # Part2 example: response_code:404,errormsg:,exitcode:0
#  response_part1=$(echo -e $res | awk -F";" {'print $1'} | tr -d '"{}')
#  response_part2=$(echo -e $res | awk -F";" {'print $2'})
#
#  if [[ ! -z "$response_part1" ]]; then
#    # parse httpCode, errorCode and externalErrorMessage
#    httpCode=$(echo -e $response_part1 | awk -F"," {'print $1'} | cut -f2 -d":")
#    errorCode=$(echo -e $response_part1 | awk -F"," {'print $2'} | cut -f2 -d":")
#    externalErrorMessage=$(echo -e $response_part1 | awk -F"," {'print $3'} | cut -f2 -d":")
#
#    $print_response && echo -e "httpCode: ${RED}$httpCode${NC} \nerrorCode: $errorCode\nexternalErrorMessage: $externalErrorMessage"
#  fi
#
#  # parse response code, errormsg and exitcode
#  response_code=$(echo -e $response_part2 | awk -F"," {'print $1'} | cut -f2 -d":")
#  errormsg=$(echo -e $response_part2 | awk -F"," {'print $2'} | cut -f2 -d":")
#  exitcode=$(echo -e $response_part2 | awk -F"," {'print $3'} | cut -f2 -d":")
#
#  $print_response && echo -e "Response Code: ${RED}$response_code${NC} \nError Message: $errormsg\nExit Code: $exitcode"
#}

swgr_parse_part_response() {
  response_part=$1
  extract_var=$2
  part_type=$3

  case $part_type in
    json) extract_val="$(echo $response_part | jq | tr -d '"{},' | grep $extract_var | awk -F":" {'print $2'})" ;;
    csv)  extract_val="$(echo $response_part | sed 's/\,/\n/g'   | grep $extract_var | awk -F":" {'print $2'})" ;;
  esac

  $verbose && $print_response && echo -e "${extract_var}: ${RED}${extract_val}${NC}" > /dev/tty
  $verbose && echo "${extract_val}"
}

swgr_parse_response() {
  print_response=$1

  # Part1 example: {"httpCode":404,"errorCode":"MACHINE_NOT_FOUND","externalErrorMessage"}
  # Part2 example: response_code:404,errormsg:,exitcode:0
  response_part1="${res%;*}"    # will drop part of string from last occur of SubStr ';' to the end
  response_part2="${res##*;}"   # will drop begin of string upto last occur of SubStr ';'
  $verbose && echo -e "response_part1:\n$response_part1"
  $verbose && echo -e "response_part2:\n$response_part2"

  declare -A arrayResVars
  arrayResVars[part1]="httpCode errorCode externalErrorMessage userId access_token_id code message"
  arrayResVars[part2]="response_code errormsg exitcode"

  # create variable for each item in response_part1 
  if [ -n "$response_part1" ]
  then
    for var in ${arrayResVars[part1]}
    do
      #arrayResVars[part1,1]=$(printf -v "${var}" $(swgr_parse_part_response "$response_part1" $var json) args)
      #printf -v "${var}" "$(swgr_parse_part_response "$response_part1" $var json)" args
      swgr_parse_part_response "$response_part1" "$var" "json"
      $verbose && echo "extract_val=${extract_val}" > /dev/tty
      printf -v "${var}" "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" ${extract_val}
      $verbose && echo "var=${var}"
    done
  fi

  # create variable for each item in response_part2
  if [ -n "$response_part2" ]
  then
    for var in ${arrayResVars[part2]}
    do
      #arrayResVars[part2,1]=$(printf -v "${var}" $(swgr_parse_part_response "$response_part2" $var csv) args)
      #printf -v "${var}" $(swgr_parse_part_response "$response_part2" $var csv) args
      swgr_parse_part_response "$response_part2" "$var" "csv"
      $verbose && echo extract_val=${extract_val} > /dev/tty
      printf -v "${var}" "%s" ${extract_val}
    done
  fi
}

swgr_dependencies() {
  check_pkg_installed jq

  swgr_common_variables
  swgr_set_print_vars
  #swgr_check_be_version
  swgr_build_url
  swgr_get_resources_list
  swgr_get_token
  swgr_common_variables_dependencies
  swgr_staticSettings
}

swgr_welcome() {
  siteName="$(echo $opt | tr '_' ' ')"
  echo -e ">>> WELCOME: Swagger of ${RED}$siteName${NC} with IP ${RED}$server_ip${NC} <<<\n"
}

swgr_clearAllMachinesErrs() {
  swgr_welcome
  echo -e "Are you sure you would like to Clear All Backend Machines Errors? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Clearing All Backend Machines Errors.${NC}\n"
    swgr_clearAllMachinesErrs_srv
  else
    echo -e "\nAborted."
  fi
}

swgr_clearAllMachinesErrs_srv() {
  swgr_dependencies

  resourcePath="machines/clearBackendErrorsOnAllMachines"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  url="${swgr_url}machines/clearBackendErrorsOnAllMachines"
  $verbose && echo "url: $url"

  curl_headers=(-H "Content-Type: application/json" -H "Accept: application/json;charset=utf-8" -H "Authorization: Bearer $access_token_id")
  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Machines errors were cleared ${RED}successfully${NC}.\n" || echo -e "Machines errors clearance has ${RED}failed${NC}.\n"
}

swgr_clearRedis() {
  swgr_welcome
  swgr_dependencies

  resourcePath="redis"
  url="${swgr_url}redis"
  $verbose && echo "url: $url"

  # Show redis data
  curl -s -X GET "${curl_headers[@]}" "$url" | sed 's/\\r\\n/,/g' | tr -s ',' '\n' | sed 's/:/ : /g' | sed 's/#/\n#/g'

  echo ""

  echo -e "\n${RED}Make sure the WH is in STOP or SHUTDOWN state ${NC}\n"
  echo -e "Are you sure you would like to Clear Redis data? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Clearing Redis Data.${NC}\n"
    res=$(curl -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")
    $verbose && echo $res

    swgr_parse_response true

    [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Redis data was cleared ${RED}successfully${NC}.\n" || echo -e "Redis data clearance ${RED}failed${NC}.\n"
  else
    echo -e "\nAborted."
  fi
}

swgr_removedFromWarehouse() {
  Machine_ID=$1
  [ -z $Machine_ID ] && echo "Please entar a valid Machine ID" && exit 1

  swgr_welcome
  echo -e "Are you sure you would like to remove machine ${RED}$Machine_ID${NC} from the warehouse? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Removing $Machine_ID machine from the warehouse.${NC}\n"
    swgr_removedFromWarehouse_srv
  else
    echo -e "\nAborted."
  fi
}

swgr_removedFromWarehouse_srv() {
  swgr_dependencies

  resourcePath="machines/machineId/removedFromWarehouse"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  url="${swgr_url}machines/$Machine_ID/removedFromWarehouse"
  $verbose && echo "url: $url"

  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Machine $Machine_ID was removed ${RED}successfully${NC}.\n" || echo -e "Machine $Machine_ID removal has ${RED}failed${NC}.\n"
}

swgr_updateLastEntry() {
  Machine_ID=$1
  [ -z $Machine_ID ] && echo "Please entar a valid Machine ID" && exit 1

  swgr_welcome
  echo -e "Are you sure you would like to update last entry of machine ${RED}$Machine_ID${NC}? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Updating last entry of $Machine_ID machine.${NC}\n"
    swgr_updateLastEntry_srv
  else
    echo -e "\nAborted."
  fi
}

swgr_updateLastEntry_srv() {
  swgr_dependencies

  resourcePath="machines/updatelastentry"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  url="${swgr_url}machines/updatelastentry?machineid=$Machine_ID"
  $verbose && echo "url: $url"

  res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Machine $Machine_ID last entry updated ${RED}successfully${NC}.\n" || echo -e "Machine $Machine_ID last entry update ${RED}failed${NC}.\n"
}

swgr_ordersPackagedOne() {
  Order_ID=$1

  resourcePath="orders/packaged"
  url="${swgr_url}orders/packaged"
  $verbose && echo "url: $url"

  echo "Packaging order: $Order_ID"

  res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "{
    \"externalId\": \"$Order_ID\",
    \"putWallSlot\": \"\"
  }" "$url")

  $verbose && echo res=$res

  [[ $Order_ID != "-b" ]] && swgr_parse_response true || swgr_parse_response false
  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Order ${RED}$Order_ID${NC} was packaged ${RED}successfully${NC}.\n" || echo -e "Order ${RED}$Order_ID${NC} packaged has ${RED}failed${NC}.\n"
}

swgr_ordersPackagedBulk() {
  ordersPackagedFile="ordersPackaged.csv"

  check_pkg_installed xsel
  # In some environments xsel is working only when setting DISPLAY environment parameter 
  export DISPLAY=:0
  # Copy from clipboard to Products.csv file
  xsel -ob > $ordersPackagedFile

  if [ -s $ordersPackagedFile ]
  then
    # The file is not-empty.
    echo -e "\nOrders to package:" && cat $ordersPackagedFile && echo -e "\n"
    echo "" >> $ordersPackagedFile

    echo -e "Are you sure you would like to mark as packaged all orders listed above ? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}Packaging orders${NC}\n"

      while read -r Order_ID
      do
        Order_ID=$(echo $Order_ID | tr -d '\r')
        ((i++))
        swgr_ordersPackagedOne $Order_ID
      done < $ordersPackagedFile
    else
      echo -e "\nAborted."
    fi
  else
    # The file is empty.
    echo "The clipboard is empty, please copy packaged order list from Excel column into the clipboard (CTRL+C)"
  fi
}

swgr_ordersPackaged() {
  Order_ID=$1
  [ -z $Order_ID ] && echo "Please entar a valid Order ID or -b for bulk" && exit 1

  swgr_welcome
  swgr_dependencies

  if [[ $Order_ID == "-b" ]]
  then
    swgr_ordersPackagedBulk
  else
    echo -e "Are you sure you would like to packaged order ${RED}$Order_ID${NC}? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}Packaging order $Order_ID${NC}\n"
      swgr_ordersPackagedOne $Order_ID
    else
      echo -e "\nAborted."
    fi
  fi
}

swgr_ordersBoostOne() {
  Order_ID=$1
  boostUnboost=$2

  resourcePath="orders/orderId/boost"
  url="${swgr_url}orders/$Order_ID/boost?boost=$boostUnboost"
  $verbose && echo "url: $url"

  echo "${boostUnboostD^}ing order: $Order_ID"
  res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  [[ $Order_ID != "-b" ]] && swgr_parse_response true || swgr_parse_response false
  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Order ${RED}$Order_ID${NC} was ${boostUnboostD}ed ${RED}successfully${NC}.\n" || echo -e "Order ${RED}$Order_ID${NC} ${boostUnboostD} has ${RED}failed${NC}.\n"
}

swgr_ordersBoostBulk() {
  ordersBoostFile="ordersBoost.csv"

  check_pkg_installed xsel
  # In some environments xsel is working only when setting DISPLAY environment parameter 
  export DISPLAY=:0
  # Copy from clipboard to Products.csv file
  xsel -ob > $ordersBoostFile
 
  if [ -s $ordersBoostFile ]
  then
    # The file is not-empty.

    echo -e "\nOrders to $boostUnboostD:" && cat $ordersBoostFile && echo -e "\n"
    echo "" >> $ordersBoostFile

    echo -e "Are you sure you would like to $boostUnboostD all orders listed above ? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}${boostUnboostD^}ing orders${NC}\n"

      while read -r Order_ID
      do
        Order_ID=$(echo $Order_ID | tr -d '\r')
        ((i++))
        swgr_ordersBoostOne $Order_ID $boostUnboost
      done < $ordersBoostFile
    else
      echo -e "\nAborted."
    fi
  else
    # The file is empty.
    echo "The clipboard is empty, please copy request for $boostUnboostD orders list into the clipboard (CTRL+C)"
  fi
}

swgr_ordersBoost() {
  Order_ID=$1
  boostUnboostD=$2

  [ -z $Order_ID ] && echo "Please entar a valid Order ID or -b <boost|unboost> for bulk boost or unboost" && exit 1

  swgr_welcome
  swgr_dependencies

  [ -z $boostUnboostD ] && echo -e "No state entared <boost|unboost>, using ${RED}boost${NC} as default state" && boostUnboostD=boost

  case $boostUnboostD in
    boost   ) boostUnboost=true  ;;
    unboost ) boostUnboost=false ;;
    *       ) echo "Please entar a valid state <boost|unboost>" && exit 1 ;;
  esac

  if [[ $Order_ID == "-b" ]]
  then
    swgr_ordersBoostBulk
  else
    echo -e "Are you sure you would like to ${RED}$boostUnboostD${NC} order ${RED}$Order_ID${NC}? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}${boostUnboostD^}ing order $Order_ID${NC}\n"
      swgr_ordersBoostOne $Order_ID $boostUnboost
    else
      echo -e "\nAborted."
    fi
  fi
}

swgr_usersDeleteOne() {
  User_ID=$1

  resourcePath="users/userId"
  url="${swgr_url}users/$User_ID"
  $verbose && echo "url: $url"

  echo "Deleting user: $User_ID"
  res=$(curl -s -w "$write_out" -X DELETE "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  [[ $User_ID != "-b" ]] && swgr_parse_response true || swgr_parse_response false
  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "User ${RED}$User_ID${NC} was deleted ${RED}successfully${NC}.\n" || echo -e "User ${RED}$User_ID${NC} deletion has ${RED}failed${NC}.\n"
}

swgr_usersDeleteBulk() {
  usersDeleteFile="usersDelete.csv"

  check_pkg_installed xsel
  # In some environments xsel is working only when setting DISPLAY environment parameter 
  export DISPLAY=:0
  # Copy from clipboard to Products.csv file
  xsel -ob > $usersDeleteFile
 
  if [ -s $usersDeleteFile ]
  then
    # The file is not-empty.

    echo -e "\nUsers to delete:" && cat $usersDeleteFile && echo -e "\n"
    echo "" >> $usersDeleteFile

    echo -e "Are you sure you would like to delete all users listed above ? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}Deleteing users${NC}\n"

      loop="-n"
      while read -r User_Name
      do
        User_Name=$(echo $User_Name | tr -d '\r')
        swgr_getUserOne $User_Name
        ((i++))
        swgr_usersDeleteOne $User_ID
      done < $usersDeleteFile
    else
      echo -e "\nAborted."
    fi
  else
    # The file is empty.
    echo "The clipboard is empty, please copy request for delete users list from Excel column into the clipboard (CTRL+C)"
  fi
}

swgr_deleteUser() {
  swgr_getUserOne $User_Name

  if [ -n "$User_ID" ]
  then
    echo -e "Are you sure you would like to delete user ${RED}$User_Name${NC} ? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}Deleting user $User_ID${NC}\n"
      swgr_usersDeleteOne $User_ID
    else
      echo -e "\nAborted."
    fi
  fi
}

swgr_usersDelete() {
  User_Name=$1
  [ -z $User_Name ] && echo "Please entar a valid User Name or -b for bulk" && exit 1

  swgr_welcome
  swgr_dependencies

  case $User_Name in
    -b) swgr_usersDeleteBulk ;;
    #-i) get_user_db          ;;
     *) swgr_deleteUser      ;;
  esac
}

get_user_info_db() {
  sql_query="
    select id
    from users
    where user_name = '$User_Name'"

  echo
  exec_commands_over_ssh run_sql "$sql_query" CCSV
  echo
}

get_developer_users_db() {
  sql_query="
    SELECT u.id, u.user_name , array_agg(ur."role") as Roles
    FROM users u 
    LEFT OUTER JOIN user_roles ur ON (u.id = ur.user_id)
    where u.id in (
      select u.id 
      from user_roles ur, users u 
      where u.id = ur.user_id 
        and ur.role='DEVELOPER'
    )
    GROUP BY u.id
    ORDER BY u.id"

  echo
  exec_commands_over_ssh run_sql "$sql_query"
  echo
}

swgr_getUserOne() {
  User_Name=$1

  #User_ID="$(echo $(get_user_info_db) | tr -d '\r')"
  User_ID=$(get_user_info_db)
  User_ID=$(echo $User_ID | tr -d '\r')

  echo -e "User_Name ${RED}$User_Name${NC}"
  echo -e "User_ID ${RED}$User_ID${NC}"

  if [ -n "$User_ID" ]
  then
    resourcePath="users/userId"
    url="${swgr_url}users/$User_ID"
    $verbose && echo "url: $url"

    res="$(curl -s -w "$write_out" -X GET "${curl_headers[@]}" "$url")"
    $verbose && echo "res=$res"
    echo "${res}" | jq 2> /dev/null

    [[ "$User_Name" != "-b" ]] && swgr_parse_response true || swgr_parse_response false
    [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "User ${RED}$User_Name${NC} info read ${RED}successfully${NC}.\n" || echo -e "User ${RED}$User_Name${NC} info read ${RED}failed${NC}.\n"
  else
    echo -e "User ${RED}$User_Name${NC} does not exist.\n" 
  fi
}

swgr_removeUsresRoleOne() {
  User_ID=$1
 
  resourcePath="users/userId"
  url="${swgr_url}users/$User_ID"
  $verbose && echo "url: $url"

  isDev=$(echo "$res" | grep "DEVELOPER")
  if [ -n "$isDev" ]
  then
    #res="$(echo "$res" | sed "s/\"DEVELOPER\"//g" | sed "s/\",]/\"]/g" | jq | grep -iv confirmed | grep -v "\"id\"" | grep -v userName | grep -v creationTime | grep -v modificationTime | sed "s/\"/\\\\\"/g")"
    res="$(echo "$res" | sed "s/\"DEVELOPER\"//g" | sed "s/\",]/\"]/g" | jq 2> /dev/null | grep -iv confirmed | grep -v "\"id\"" | grep -v userName | grep -v creationTime | grep -v modificationTime)"
    res=$(echo $res | tr -d ' ')

    echo "Removing DEVELOPER role from user: $User_Name ID: $User_ID"

    res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "$res" "$url")

    $verbose && echo "res=$res"

    [[ "$User_Name" != "-b" ]] && swgr_parse_response true || swgr_parse_response false
    [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "User ${RED}$User_Name${NC} role DEVELOPER was deleted ${RED}successfully${NC}.\n" || echo -e "User ${RED}$User_Name${NC} role DEVELOPER deletion has ${RED}failed${NC}.\n"
  else
    echo "No DEVELOPER role found"
  fi
}

swgr_removeUsersRoleBulk() {
  usersDeleteFile="usersDelete.csv"

  check_pkg_installed xsel

  # In some environments xsel is working only when setting DISPLAY environment parameter 
  export DISPLAY=:0
  # Copy from clipboard to Products.csv file
  xsel -ob > $usersDeleteFile
 
  if [ -s $usersDeleteFile ]
  then
    # The file is not-empty.

    echo -e "\nUsers to remove DEVELOPER role from:" && cat $usersDeleteFile && echo -e "\n"
    echo "" >> $usersDeleteFile

    echo -e "Are you sure you would like to remove DEVELOPER role from all users listed above ? (Yy)."
    read -p "[ ]"$'\b\b' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      echo -e "\n${BLINK}Removing DEVELOPER role from users${NC}\n"

      loop="-n"
      while read -r User_Name
      do
        User_Name=$(echo $User_Name | tr -d '\r')
        swgr_getUserOne $User_Name
        ((i++))
        swgr_removeUsresRoleOne $User_ID
      done < $usersDeleteFile
    else
      echo -e "\nAborted."
    fi
  else
    # The file is empty.
    echo "The clipboard is empty, please copy request for delete users list from Excel column into the clipboard (CTRL+C)"
  fi
}

swgr_removeUserRole() {
  swgr_getUserOne $User_Name

  echo -e "Are you sure you would like to remove DEVELOPER role from user ${RED}$User_Name${NC} ? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Removing DEVELOPER role from user $User_Name${NC}\n"
    swgr_removeUsresRoleOne $User_ID
  else
    echo -e "\nAborted."
  fi
}

swgr_removeUsresRole() {
  User_Name=$1
  [ -z $User_Name ] && echo "Please entar a valid User Name or -b for bulk or -i for users list with DEVELOPER role" && exit 1

  swgr_welcome
  swgr_dependencies

  case $User_Name in
    -b) swgr_removeUsersRoleBulk ;;
    -i) get_developer_users_db   ;;
     *) swgr_removeUserRole      ;;
  esac
}

swgr_skusBulkUnsuspend() {
  swgr_welcome
  swgr_dependencies

  # Show suspended SKUs
  pageNum=0
  j=-1
  declare -A skuArr

  resourcePath="skus/suspend/search"

  while [[ $pageNum -ge 0 ]]
  do
    url="${swgr_url}skus/suspend/search?pageNum=${pageNum}&pageSize=100"
    $verbose && echo "url: $url"
    #res=$(<$HOME/sku_data_$pageNum.json)
    res=$(curl -s -X GET "${curl_headers[@]}" "$url")
    $verbose && echo $res

    i=-1
    skuCode=""

    while [ $i -lt 100 ] && [[ $skuCode != "null" ]]
    do
      ((i++))

      [ $i = 0 ] && echo "Found the following suspended SKUs:"
      [ $i = 0 ] && ($verbose && echo "#, pageNum, idxInPage, skuCode, batchId" || echo "skuCode")

      skuCode=$(jq -r ".data[$i].skuCode" <<< "$res")
      batchId=$(jq -r ".data[$i].batchId" <<< "$res")
    
      if [[ $skuCode != "null" ]]
      then
        ((j++))

        skuArr[$j,0]=$skuCode
        skuArr[$j,1]=$batchId
        $verbose && echo "$j, $pageNum, $i, ${skuArr[$j,0]}, ${skuArr[$j,1]}" || echo "${skuArr[$j,0]}"
      fi
    done

    if [[ $skuCode != "null" ]]
    then
      ((pageNum++))
    else
      pageNum=-1
    fi
  done

  if [[ $j -eq -1 ]]
  then
    echo "No suspended SKU found. Aborting"
    exit 1
  fi

  echo -e "Are you sure you would like to Unsuspend ${RED}$((j+1))${NC} suspended SKUs? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Unsuspending all suspended SKUs.${NC}\n"

    resourcePath="skus/suspend"
    url="${swgr_url}skus/suspend"
    $verbose && echo "url: $url"
    $verbose && silent="" || silent="-s"

    for (( i=0; i<=$j; i++ ))
    do 
  # Example: {skuCode: "1123669-CTSD-10", batchId: "510312905", entryDate: 0, expirationDate: 0, suspend: false}
      skuCode=${skuArr[$i,0]}
      batchId=${skuArr[$i,1]}

      res=$(curl $silent -w "$write_out" -X PUT "${curl_headers[@]}" -d "{
        \"skuCode\": \"$skuCode\",
        \"batchId\": \"$batchId\",
        \"entryDate\": 0,
        \"expirationDate\": 0,
        \"suspend\": false
      }" "$url")

      $verbose && echo res=$res

      swgr_parse_response false

      echo -e "$i, $skuCode, $batchId, Response code: ${RED}$response_code${NC}"
    done
  else
    echo -e "\nAborted."
  fi
}

swgr_bulkCancelRecallBin() {
  swgr_welcome
  swgr_dependencies

  # Show suspended SKUs
  declare -A recallBinsArr

  resourcePath="bins/lockedForRecall"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  url="${swgr_url}bins/lockedForRecall"
  $verbose && echo "url: $url"

  res=$(curl -s -X GET "${curl_headers[@]}" "$url")

  $verbose && echo $res

  i=-1
  binCode=""

  while [[ $binCode != "null" ]]
  do
    ((i++))

    [ $i = 0 ] && echo -e "Found the following locked for recall Bins:\n#, binCode"

    binCode=$(jq -r ".bins[$i].binCode" <<< "$res")
    
    if [[ $binCode != "null" ]]
    then
      recallBinsArr[$i]=$binCode
      echo "$i, ${recallBinsArr[$i]}"
    fi
  done

  if [[ $i -eq -1 ]]
  then
    echo "No recall BIN requests found. Aborting"
    exit 1
  fi

  echo -e "Are you sure you would cancel ${RED}$i${NC} recall BINs requests? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Cancelling all recall BINs requests.${NC}\n"

    resourcePath="recall/binCode/cancel"

    for (( j=0; j<$i; j++ ))
    do 
      binCode=${recallBinsArr[$j]}
      url="${swgr_url}recall/$binCode/cancel"
      $verbose && echo "url: $url"
      $verbose && silent="" || silent="-s"

      res=$(curl $silent -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")

      $verbose && echo res=$res

      swgr_parse_response false

      echo -e "$j, $binCode, Response code: ${RED}$response_code${NC}"
    done
  else
    echo -e "\nAborted."
  fi
}

swgr_bulkUnpauseOrders() {
  swgr_welcome
  swgr_dependencies

  # Show suspended SKUs
  pageNum=0
  j=-1
  declare -A ordersArr

  resourcePath="orders/search"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  while [[ $pageNum -ge 0 ]]
  do
    #res=$(<$HOME/sku_data_$pageNum.json)
    url="${swgr_url}orders/search?pageNum=${pageNum}&pageSize=100"
    $verbose && echo "url: $url"
    res=$(curl -s -X GET "${curl_headers[@]}" "$url")

    $verbose && echo -e "\nPage: $pageNum\n$res"

    i=0
    orderExternalId=""

    while (( $i < 100 )) && [[ $orderExternalId != "null" ]]
    do
      orderExternalId=$(jq -r ".orderModels[$i].externalId" <<< "$res")
      orderStatus=$(jq -r ".orderModels[$i].status" <<< "$res")

      $verbose && echo "$i, orderExternalId: $orderExternalId, orderStatus: $orderStatus"

      if [[ $orderExternalId != "null" ]] && [[ "$orderStatus" == "PAUSED" ]]
      then
        ((j++))

        ordersArr[$j,0]=$orderExternalId
        ordersArr[$j,1]=$orderStatus
        $verbose && echo "$j, $pageNum, $i, orderExternalId=${ordersArr[$j,0]} , orderStatus=${ordersArr[$j,1]}"
      fi

      ((i++))
    done

    if [[ $orderExternalId != "null" ]]
    then
      ((pageNum++))
    else
      pageNum=-1
    fi
  done

  if [[ $j -eq -1 ]]
  then
    echo "No paused order found. Aborting"
    exit 1
  fi

  echo -e "Are you sure you would like to Unpause ${RED}$((j+1))${NC} orders? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Unpausing all orders.${NC}\n"

    resourcePath="orders/orderId/pause"

    for (( i=0; i<=$j; i++ ))
    do 
      orderExternalId=${ordersArr[$i,0]}
      orderStatus=${ordersArr[$i,1]}

      url="${swgr_url}orders/$orderExternalId/pause?pause=false"
      $verbose && echo "url: $url"
      res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")

      swgr_parse_response false

      echo -e "$i, $orderExternalId, $orderStatus, Response code: ${RED}$response_code${NC}"
    done

    echo -e "All paused orders were unpaused.\n"
  else
    echo -e "\nAborted."
  fi
}

swgr_bulkOrdersRelevancy_usage() {
  echo -e "Usage: $0 $inpSite $inpCmd <originalRelevancyTime (DD:MM:YYYY_HH:MM)> <targetRelevancyTime (DD:MM:YYYY_HH:MM)>" >&2
  exit 1
}

validate_and_set_relevancy(){
  relevancy=$1

  #if [[ $relevancy =~ ^[0-9]{2}-[.]{3}-[0-9]{4}_[0-9]{2}:[0-9]{2}$ ]]
  if [[ $relevancy =~ ^[0-9]{1,2}-.{2,3}-[0-9]{4}_[0-9]{2}:[0-9]{2}$ ]]
  then
    echo -e "Date $relevancy is in a valid date format DD-MM-YYYY_HH:MM"
  else 
    echo -e "Date $relevancy is ${RED}NOT${NC} in a valid date format DD-MM-YYYY_HH:MM"
    swgr_bulkOrdersRelevancy_usage
  fi

  IFS=_ read -r relevancyDate relevancyTime <<< $relevancy
  IFS=- read -r DD MM YYYY <<< $relevancyDate

  $verbose && echo "date -d: $YYYY $DD $MM"

  #relevancyDate=$(tr '-' '/' <<<"$relevancyDate")
  # In case MM is digits only and not Jan-Dec
  if [[ $MM =~ [0-9]{2} ]]
  then
    date_format="$MM/$DD/$YYYY"
  else
    date_format="$MM $DD $YYYY"
  fi

  $verbose && echo date_format=$date_format

  date -d "$date_format" > /dev/null 2>&1
  is_valid=$?

  $verbose && echo is_valid=$is_valid

  # Validate relevancyDate
  if [ $is_valid -eq 0 ]
  then
    echo -e "Date $relevancyDate is a valid date"
  else
    echo -e "Date $relevancyDate is ${RED}NOT${NC} a valid date"
    swgr_bulkOrdersRelevancy_usage
  fi

  relevancyDate=$(date "+%d-%m-%Y" -d "$date_format")
  $verbose && echo relevancyDate=$relevancyDate

  # Validate relevancyTime
  if $(date "+%H:%M" -d "$relevancyTime" > /dev/null 2>&1)
  then
    echo -e "Time $relevancyTime is a valid time"
  else
    echo -e "Time $relevancyTime is ${RED}NOT${NC} a valid time"
    swgr_bulkOrdersRelevancy_usage
  fi

  $verbose && echo relevancyTime=$relevancyTime

  IFS=: read -r HH MIN <<< $relevancyTime

  IFS=- read -r DD MM YYYY <<< $relevancyDate
  relevancyEpoch=$(date "+%s" -d "$MM/$DD/$YYYY $relevancyTime")
  $verbose && echo relevancyEpoch=$relevancyEpoch
}

validate_old_new_relevancy() {
  if ! [[ "$oldRelevancyDate" == "$newRelevancyDate" ]]
  then
    echo -e "Old relevancy date is ${RED}DIFFER${NC} than new relevancy date, ignoring."
    swgr_bulkOrdersRelevancy_usage
  fi
}

swgr_bulkOrdersRelevancy() {
  oldRelevancy=$1
  newRelevancy=$2

  if [ $# -ne 2 ] 
  then 
    swgr_bulkOrdersRelevancy_usage
    exit 1
  fi

  swgr_welcome

  validate_and_set_relevancy $oldRelevancy
  oldRelevancyDate=$relevancyDate
  oldRelevancyTime=$relevancyTime
  oldHH=$HH
  oldMM=$MIN
  oldRelevancyEpoch=$relevancyEpoch

  validate_and_set_relevancy $newRelevancy
  newRelevancyDate=$relevancyDate
  newRelevancyTime=$relevancyTime
  newHH=$HH
  newMM=$MIN
  newRelevancyEpoch=$relevancyEpoch

  validate_old_new_relevancy $oldRelevancyDate $newRelevancyDate

  nowEpoch=$(date +%s)
  #IFS=_ read -r oldRelevancyDate oldRelevancyTime <<< $oldRelevancy
  #IFS=_ read -r newRelevancyDate newRelevancyTime <<< $newRelevancy

  swgr_dependencies

  $verbose && echo "oldRelevancy:       $oldRelevancy"
  $verbose && echo "oldRelevancyDate:   $oldRelevancyDate"
  $verbose && echo "oldRelevancyTime:   $oldRelevancyTime"
  $verbose && echo "oldHH:              $oldHH"
  $verbose && echo "oldMM:              $oldMM"
  $verbose && echo "oldRelevancyEpoch:  $oldRelevancyEpoch"
  $verbose && echo "newRelevancy:       $newRelevancy"
  $verbose && echo "newRelevancyDate:   $newRelevancyDate"
  $verbose && echo "newRelevancyTime:   $newRelevancyTime"
  $verbose && echo "newHH:              $newHH"
  $verbose && echo "newMM:              $newMM"
  $verbose && echo "newRelevancyEpoch:  $newRelevancyEpoch"
  $verbose && echo "nowEpoch:           $nowEpoch"

  echo ""

  resourcePath="orders/relevancy"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  url="${swgr_url}orders/relevancy?originalRelevancyTime=${oldRelevancyDate}%20${oldHH}%3A${oldMM}&targetRelevancyTime=${newRelevancyDate}%20${newHH}%3A${newMM}"
  $verbose && echo "url: $url"

  $verbose && echo -e "Command: curl -X PUT --header \"Content-Type: application/json\" --header \"Accept: application/json;charset=utf-8\" --header \"Authorization: Bearer $access_token_id\" -d \"{}\" \"$url\"\n"

  echo -e "Are you sure you would like to Change relevancy time from $oldRelevancyDate $oldRelevancyTime to $newRelevancyDate $newRelevancyTime? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Moving relevancy time.${NC}\n"

    res=$(curl -s -w "$write_out" -X PUT "${curl_headers[@]}" -d "{}" "$url")

    $verbose && echo res=$res

    swgr_parse_response true

    echo -e "Relevancy time changed.\n"
  else
    echo -e "\nAborted."
  fi
}

swgr_buildMap() {
  mapUrl=$1
  mapFileExt=$2

  resourcePath="${mapUrl}"
  url="${swgr_url}${mapUrl}"
  $verbose && echo "url: $url"
  curl -s -X GET "${curl_headers[@]}" "$url" | jq > $envMapFolder/${envName}_$mapFileExt.json
}

swgr_buildMapFolder() {
  envName=$1
  [ -z $envName ] && echo "Please entar a valid environment name" && exit 1
  # Fix folder name, replace spaces with underscores

  $verbose && echo -e "envName: ${RED}$envName${NC}"

  swgr_welcome
  swgr_dependencies

  envMapFolder=./maps/${envName}
  [ -d "$envMapFolder" ] && envMapFolderExist=true || envMapFolderExist=false
  ! $envMapFolderExist && (mkdir -p -- $envMapFolder ; echo -e "Folder ${RED}$envMapFolder${NC} was created")

  swgr_buildMap warehouse/area/CT02_08 cart
  swgr_buildMap warehouse/area/LT02_32 lift
  swgr_buildMap queues queues
  swgr_buildMap frames frames
  swgr_buildMap frames/types frameTypes

  $envMapFolderExist && echo -e "\nFolder ${RED}$envMapFolder${NC} files were updated"
  echo -e "\nFolder contents:"
  #ls -Ss --block-size=1 --format=single-column $envMapFolder/*
  ls -ld $envMapFolder/* | awk '{printf "%12s %3s %2s %5s %s \n", $5,$6,$7,$8,$9}'
}

swgr_resendNotification() {
  swgr_welcome
  notificationObjs=("BINS" "ORDERS" "FRAMES" "SYNC_CONSOLIDATION_REQUESTS" "RECALL_REQUESTS" "CYCLE_COUNT_REQUESTS" "CONSOLIDATION_REQUESTS" "PUTWALL_SLOTS" "MOV_ADV" "PCK_ADV" "TSC_ADV" "STC_ADV" "ISP_ADV")

  PS3=$'\n'"Please select a notification object to resend: "

  echo -e "Available notification objects:\n"
  select notificationObj in "${notificationObjs[@]}"
  do
    [[ " ${notificationObjs[@]} " =~ " ${notificationObj} " ]] && break || echo -e "Invalid option ${RED}$REPLY${NC}"
  done

  echo -e "\nAre you sure you would like to resend notifications for ${RED}$notificationObj${NC} object? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Resending notifications for $notificationObj ${NC}\n"
    swgr_resendNotification_srv
  else
    echo -e "\nAborted."
  fi
}

swgr_resendNotification_srv() {
  swgr_dependencies

  resourcePath="notification"
  $(swgr_validate_resource $resourcePath) || { echo "Resource $resourcePath is not supported in v${siteVersion}. Aborting." ; exit 1 ; }

  url="${swgr_url}notification?notificationObj=$notificationObj"
  $verbose && echo "url: $url"

  curl_headers=(-H "Content-Type: application/json" -H "Accept: application/json;charset=utf-8" -H "Authorization: Bearer $access_token_id")
  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Machines errors were cleared ${RED}successfully${NC}.\n" || echo -e "Machines errors clearance has ${RED}failed${NC}.\n"
}

### SWAGGER END ###

scan_network() {
  first_ip_in_range=$1
  $verbose && first_ip_in_range=$first_ip_in_range

  if [[ $first_ip_in_range == "" ]]
  then
    echo "The scan_network command should be run with input of First IP In Range"
    echo
    echo "Example of knowen sites IP range"
    echo "     10.42.0.0     Deckers MW"
    echo "     10.30.0.0     Deckers MV"
    echo "     10.43.0.0     Keshet KH"
    echo "     10.45.0.0     Keshet PT"
    echo "     10.47.1.0     Gaia"
    echo "     10.44.1.0     DSV Lahr"
    echo "     10.45.1.0     VM Bochum"
    echo "     10.45.1.0     Clini Stoke"
    echo "     172.16.30.0   Sela Urbanica"
    echo "     172.16.20.0   Sela Nike"
    echo
  else
    if [[ $first_ip_in_range =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
    then
      exec_commands_over_ssh "scan_network_srv $first_ip_in_range"
    else
      echo -e "The provided IP is not valid\n"
    fi
  fi
}

scan_network_srv() {
  first_ip_in_range=$1

  echo -e "Searching for live devices in network, nmap of $first_ip_in_range/24\n"
  nmap -v -sn $first_ip_in_range/24 | grep -v 'host down' | grep 'Nmap scan report for' | sed 's/Nmap scan report for //g'
}

keshet_web_upd() {
  CSV=$1

  sql_query_1="
    select s.upcs, s.qty,s.name, to_char(to_timestamp(updated /1000),'DD/MM/YYYY HH24:MI:SS') AS updated1
    from skus s 
    where status ='SYNCED'
      and qty = 0
      and family_id > 0
      and s.updated >= round(DATE_PART('epoch',now() - '1 day'::INTERVAL)*1000) 
    order by updated  desc"

  sql_query_2="
    with STEP as (
      select distinct s.upcs, s.qty,
	           to_char(to_timestamp(b.entry_date /1000),'DD/MM/YYYY HH24:MI:SS') AS fixed_entry,
	           s."name", s.updated
      from binsskus b
      right join skus s on b.skuid = s.id
      where b.entry_date  >= round(DATE_PART('epoch',now()- ' 20 hour'::INTERVAL)*1000))

      select distinct upcs as upcs, qty ,name, min(fixed_entry)
      from STEP
      group by qty, upcs, name, updated
      order by qty desc"

  echo
  exec_commands_over_ssh run_sql "$sql_query_1" $CSV
  echo
  exec_commands_over_ssh run_sql "$sql_query_2" $CSV
  echo
}

vacant_storage() {
  CSV=$1

  sql_query="
    SELECT t1.frametype as frame_type, t1.total, t2.vacant
    FROM (select frametype, count(*) as total
      from frames f
      where locationarea in (select id from zones z where type = 'STORAGE')
        and isavailable and issuspended = false and accessible
      group by frametype) as t1,
     (select frametype, count(*) as vacant
      from frames f
      where locationarea in (select id from zones z where type = 'STORAGE')
        and isvacant and isavailable and issuspended = false and accessible
      group by frametype) as t2
    WHERE t1.frametype = t2.frametype"

  echo
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

pgsql_db() {
  SITE=$(echo $opt | sed 's/\_[^_]*$//')
  $verbose && echo SITE_PAR1=$SITE

  exec_commands_over_ssh "./db_login_$SITE.sh"
}

check_picking_req() {
  query_by=$1
  param=$2

  sql_query_PICK_TICKET="
    select ao.externalid,ao.pickticket,s.upcs,s."name" ,sku_qty as ORDERD_QTY,picked_qty,s.qty as QTY_IN_WH ,apr.status, bin_code,apr.zone_id,
    to_char(to_timestamp(last_status_chaned /1000),'DD/MM/YYYY HH24:MI:SS') as last_status_chaned,
        to_char(to_timestamp(apr.startprocesstime /1000),'DD/MM/YYYY HH24:MI:SS') as startprocesstime,
            to_char(to_timestamp(apr.updated /1000),'DD/MM/YYYY HH24:MI:SS') as updated    
                from all_picking_requests apr
                    join skus s on apr.sku_id = s.id
                        join all_orders ao on ao.externalid = apr.order_id
                            where ao.pickticket = '${param}'"

  sql_query_EXTERNAL_ID="
    select ao.externalid,ao.pickticket,s.upcs,s."name",sku_qty as ORDERD_QTY,picked_qty,s.qty as QTY_IN_WH,apr.status,bin_code,apr.zone_id,
        to_char(to_timestamp(last_status_chaned /1000),'DD/MM/YYYY HH24:MI:SS') as last_status_chaned,
            to_char(to_timestamp(apr.startprocesstime /1000),'DD/MM/YYYY HH24:MI:SS') as startprocesstime,
                to_char(to_timestamp(apr.updated /1000),'DD/MM/YYYY HH24:MI:SS') as updated    
                    from all_picking_requests apr
                        join skus s on apr.sku_id = s.id
                            join all_orders ao on ao.externalid = apr.order_id
                                where ao.externalid  = '${param}'"

  sql_query_TIME="
    select ao.pickticket,s.upcs,s."name",sku_qty as ORDERD_QTY,picked_qty,s.qty as QTY_IN_WH,apr.status,bin_code,apr.zone_id,
    to_char(to_timestamp(last_status_chaned /1000),'DD/MM/YYYY HH24:MI:SS') as last_status_chaned,
        to_char(to_timestamp(apr.startprocesstime /1000),'DD/MM/YYYY HH24:MI:SS') as startprocesstime,
            to_char(to_timestamp(apr.updated /1000),'DD/MM/YYYY HH24:MI:SS') as updated,apr.completing_user
                from all_picking_requests apr
                    join skus s on apr.sku_id = s.id
                        join all_orders ao on ao.externalid = apr.order_id
                            where apr.updated >= round(DATE_PART('epoch',now()- '200 hours'::INTERVAL)*1000)
                                order by 12,1 desc"

  sql_query_SKUS="
    select ao.pickticket,s.upcs,s."name" ,sku_qty as ORDERD_QTY,picked_qty,s.qty as QTY_IN_WH ,apr.status, bin_code,apr.zone_id,
    to_char(to_timestamp(last_status_chaned /1000),'DD/MM/YYYY HH24:MI:SS') as last_status_chaned,
        to_char(to_timestamp(apr.startprocesstime /1000),'DD/MM/YYYY HH24:MI:SS') as startprocesstime,
            to_char(to_timestamp(apr.updated /1000),'DD/MM/YYYY HH24:MI:SS') as updated,apr.completing_user
                from all_picking_requests apr
                    join skus s on apr.sku_id = s.id
                        join all_orders ao on ao.externalid = apr.order_id
                            where s.upcs = '{${param}}'
                                order by 12,1 desc"
  
  case $query_by in
    PICK_TICKET)   sql_query=$sql_query_PICK_TICKET ;;
    EXTERNAL_ID)   sql_query=$sql_query_EXTERNAL_ID ;;
    TIME)          sql_query=$sql_query_TIME ;;
    SKUS)          sql_query=$sql_query_SKUS ;;
    *)             echo -e "\nPlease enter a valid option: PICK_TICKET | EXTERNAL_ID | TIME | SKUS" ; exit 1 ;;
  esac

  echo -e "\nCheck picking requests by ${RED}$query_by${NC} $param\n"
  exec_commands_over_ssh run_sql "$sql_query" $CSV
  echo
}

search_logs() {
  item=$1
  archive=$2
  days=$3

  $verbose && echo item = $item
  $verbose && echo archive = $archive

  if [[ $item == "" ]]
  then
    echo "Please enter a valid ORDER# | BIN# | SKU#"
    exit 1
  else
    echo
    exec_commands_over_ssh "search_logs_srv $item $archive $days"
    echo
  fi
}

search_logs_srv() {
  item=$1
  archive=$2
  days=$3

  RED='\033[1;31m'     # Red Color
  NC='\033[0m'         # No Color

  grep -E "order.*$item|bin.*$item|sku.*$item" $(ls -1rt /usr/local/backend/log/*log)

  if [[ $? -ne 0 ]]
  then
    echo "Item not found in logs."
  fi

  if [[ $archive == "A" ]]
  then
    re='^[0-9]+([.][0-9]+)?$'

    # Search days is not digits
    if ! [[ $days =~ $re ]]
    then
      echo -e "\nInvalid days parameter. Using default ${RED}1${NC} day"
      days=1
    fi

    echo -e "\nSearching in archive folder ${RED}$days${NC} days back\n"
    numFiles=$(find /usr/local/backend/log/archive/*gz -mtime -$days | wc -l)
    echo -e "Searching in ${RED}$numFiles${NC} files"

    zgrep -E "order.*$item|bin.*$item|sku.*$item" $(find /usr/local/backend/log/archive/*gz -mtime -$days)

    if [[ $? -ne 0 ]]
    then
      echo "Item not found in archive logs."
    fi
  fi
}

exec_commands_over_ssh_robot() {
  in_run_func=$1
  machine_id=$2
  in_param1=$3
  in_param2=$4
  in_param3=$5
  in_param4=$6

  $verbose && echo in_run_func = $in_run_func
  $verbose && echo machine_id = $machine_id
  $verbose && echo in_param1 = $in_param1
  $verbose && echo in_param2 = $in_param2
  $verbose && echo in_param3 = $in_param3
  $verbose && echo in_param4 = $in_param4

  commands=" $(typeset -f $in_run_func) ; $in_run_func \"$in_param1\" \"$in_param2\" \"$in_param3\" \"$in_param4\""

  $verbose && echo "sshpass -p a ssh -t $machine_id \"$commands\""
  sshpass -p a ssh -t $machine_id "$commands"
}

search_robot_logs() {
  machine_id=$1
  search_str=$2
  archive=$3
  days=$4

  $verbose && echo machine_id = $machine_id
  $verbose && echo search_str = $search_str
  $verbose && echo archive = $archive
  $verbose && echo days = $days
  
  [[ -z "$machine_id" ]] && { echo "Please enter a machine_id" ; exit 1 ; }
  [[ -z "$search_str" ]] && { echo "Please enter a string to search" ; exit 1 ; }
  [[ -n "$archive" ]] && [[ "$archive" != "A" ]] && { echo "Please enter 'A' to search also in archive logs" ; exit 1 ; }

  echo
  exec_commands_over_ssh_robot search_robot_logs_robot $machine_id $search_str $archive $days
  echo
}

search_robot_logs_robot() {
  search_str=$1
  archive=$2
  days=$3

  RED='\033[1;31m'     # Red Color
  NC='\033[0m'         # No Color

  grep -E "$search_str" $(ls -1rt /caja/logs/*log)

  if [[ $? -ne 0 ]]
  then
    echo "String $search_str was not found in logs."
  fi

  if [[ $archive == "A" ]]
  then
    re='^[0-9]+([.][0-9]+)?$'

    # Search days is not digits
    if ! [[ $days =~ $re ]]
    then
      echo -e "\nInvalid days parameter. Using default ${RED}1${NC} day"
      days=1
    fi

    echo -e "\nSearching in archive folder ${RED}$days${NC} days back\n"
    numFiles=$(find /caja/archive-logs/*gz -mtime -$days | wc -l)
    echo -e "Searching in ${RED}$numFiles${NC} files"

    zgrep -a -E "$search_str" $(find /caja/archive-logs/*gz -mtime -$days)

    if [[ $? -ne 0 ]]
    then
      echo "String $search_str was not found in archive logs."
    fi
  fi
}

validate_service_name() {
  services="admin machines wms routes safety viadat"
  $verbose && [ ! -z "$Service"    ] && echo "Service: $Service"
  [ -z $Service ] || ! [[ $(grep -w $Service <<< $services) ]] && echo "Please entar a valid Service name ($services)" && exit 1
}

api_clean_mem() {
  Service=$1
  validate_service_name

  echo -e "\nGarbage Collection of ${RED}$(echo $opt | tr '_' ' ')${NC} with IP ${RED}$server_ip${NC}"
  echo -e "\n${RED}Make sure the WH is in STOP or SHUTDOWN state ${NC}\n"
  echo -e "Are you sure you would like to run Garbage Collection of $Service service? (Yy)."
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo -e "\n${BLINK}Running Garbage Collection of $Service service.${NC}\n"
    api_clean_mem_srv
  else
    echo -e "\nAborted."
  fi
}

api_clean_mem_srv() {
  swgr_common_variables

  url="https://$selectedCfgURL/$Service/monitoring/tasks/gc"
  $verbose && echo "url: $url"

  res=$(curl -s -w "$write_out" -X POST "${curl_headers[@]}" -d "{}" "$url")
  $verbose && echo res=$res

  swgr_parse_response true

  [[ "$SWGR_SUCCESS" =~ $response_code ]] && echo -e "Service $Service memory was cleaned ${RED}successfully${NC}.\n" || echo -e "Service $Service memory cleane has ${RED}failed${NC}.\n"
}

cancel_all_printer_jobs_srv() {
  status() {
    echo -e "\nAll printers status"
    $docker $cups lpstat -a

    echo -e "\nAll printers"
    printers=$($docker $cups lpstat -a | awk '{print $1}')
    echo $printers

    echo -e "\nAll not-completed jobs"
    $docker $cups lpstat -W not-completed
  }

  cancel() {
    echo -e "\nDelete printer jobs"
    $docker $cups cancel -a $cancel_printer
  }

  cancel_option=$1
  cancel_printer=$2
  docker="/usr/bin/docker exec --tty"

  cups=$(docker ps | grep cups | awk '{print $NF}')
  echo -e "CUPS docker name: $cups"

  $cancel_option
}

cancel_all_printer_jobs_usage() {
  echo "Usage: cancel_all_printer_jobs status"
  echo "       cancel_all_printer_jobs cancel <printer>"
  exit 1
}

cancel_all_printer_jobs() {
  cancel_option=$1
  cancel_printer=$2
  $verbose && echo cancel_option=$cancel_option
  $verbose && echo cancel_printer=$cancel_printer

  case $cancel_option in
    cancel | status) exec_commands_over_ssh "cancel_all_printer_jobs_srv $cancel_option $cancel_printer" ;;
    *) cancel_all_printer_jobs_usage ;;
  esac
}

find_instruction_index() {
  echo -e "\nSearching for ${RED}findInstructionIndex${NC} occurences in jobServer_trace.log\n"

  exec_commands_over_ssh "find_instruction_index_srv"
}

find_instruction_index_srv() {
  RED='\033[1;31m'     # Red Color
  NC='\033[0m'         # No Color

  cd /usr/local/backend/log
  findInstructionIndex=$(grep -B 3 findInstructionIndex jobServer_trace.log | grep startTime | tail -1 | grep -oP '(?<=\().*?(?=\))')

  if [ -z $findInstructionIndex ]
  then
    echo -e "No occurence of ${RED}findInstructionIndex${NC} found"
  else
    echo -e "findInstructionIndex: ${RED}$findInstructionIndex${NC}"
    echo -e "Now use the Connect with ${RED}robots_by_mission_route_start_time${NC} command to get the robots list"
  fi

  echo
}

robots_by_mission_route_start_time() {
  route_start_time=$1

  [ -z $route_start_time ] && echo "Please run command with route_start_time timestamp" && exit 1

  sql_query="SELECT machine_id,status,completed_on
             FROM missions m
             WHERE m.route_start_time=${route_start_time}"

  echo -e "\nRobots with mission by route_start_time\n"
  exec_commands_over_ssh run_sql "$sql_query"
  echo
}

usage() {
  shName=$(basename $0)

  echo -e "Usage:"
  echo -e "  $shName                                    - interactive mode"
  echo -e "  $shName -h                                 - display this help and exit"
  echo -e "  $shName -l                                 - display menu list and exit"
  echo -e "  $shName -f <String to filter results by>   - filter results" 
  echo -e "  $shName -g                                 - graphic mode"
  echo -e "  $shName -v                                 - display verbose messages"
  echo -e "  $shName <Site # | Name>                    - connect to site # or name from $shName -l"
  echo -e "  $shName <Site # | Name> <Command>          - execute command on site # or name from $shName -l"
  echo -e "  Commands:"
  echo -e "    Works directly on robots:"
  echo -e "      ${RED}search_robot_logs${NC} <STRING> <A> <Days>     - find string in logs. Use A switch to search also in archive X Days in the past"
  echo -e "    Works on WEB, JOB, SERVICE and TEST servers:"
  echo -e "      ${RED}server_stats${NC}                              - show server statistics"
  echo -e "      ${RED}htop${NC} <Java>                               - show server htop, Java switch will show only the Java processes"
  echo -e "    Works on WEB, JOB and TEST servers:"
  echo -e "      ${RED}restart_services${NC}                          - restarting the server services"
  echo -e "      ${RED}services_status${NC}                           - show BE services status"
  echo -e "      ${RED}find_instruction_index${NC}                    - Find findInstructionIndex string in jobServer_trace.log and returns its timestamp"
  echo -e "    Works on WEB servers:"
  echo -e "      ${RED}noisy_robots${NC}                              - show robots with ERROR in machinesServer.log"
  echo -e "      ${RED}del_sku_bins_loc${NC}  <SKU#>                  - find deleted bins by known SKU #"
  echo -e "      ${RED}search_logs${NC} <ORDER# | BIN# | SKU#> <A> <Days> - find info in logs for known ORDER# | BIN# | SKU#. Use A switch to search also in archive X Days in the past"
  echo -e "      ${RED}swgr_clearRedis${NC}                           - Run swagger clear Redis command"
  echo -e "      ${RED}swgr_clearAllMachinesErrs${NC}                 - Run swagger clearBackendErrorsOnAllMachiness command"
  echo -e "      ${RED}swgr_removedFromWarehouse${NC} <Machine ID>    - Run swagger removedFromWarehouse command"
  echo -e "      ${RED}swgr_updateLastEntry${NC} <Machine ID>         - Run swagger updateLastEntry command"
  echo -e "      ${RED}swgr_ordersPackaged${NC} <Order ID | -b>       - Run swagger ordersPackaged command use Order ID or -b flag for bulk packaged"
  echo -e "      ${RED}swgr_ordersBoost${NC} <Order ID | -b>          - Run swagger ordersBoost command use Order ID or -b flag for bulk boost"
  echo -e "      ${RED}swgr_usersDelete${NC} <Username | -b>          - Run swagger usersDelete command use Username or -b flag for bulk users delete"
  echo -e "      ${RED}swgr_removeUsresRole${NC} <Username | -b>      - Run swagger removeUsresRole command use Username or -b flag for bulk users role delete"
  echo -e "      ${RED}swgr_skusBulkUnsuspend${NC}                    - Run swagger skusSuspend command in bulk and do unsuspend"
  echo -e "      ${RED}swgr_bulkCancelRecallBin${NC}                  - Run swagger recallBinCodeCancel command in bulk and do cancel for all bins recall requests"
  echo -e "      ${RED}swgr_bulkUnpauseOrders${NC}                    - Run swagger ordersPause command in bulk and do unpause for all paused orders"
  echo -e "      ${RED}swgr_bulkOrdersRelevancy${NC} <originalRelevancyTime> <targetRelevancyTime> - Run swagger ordersRelevancy command. Input dates format are DD-MM-YYYY_HH:MM"
  echo -e "      ${RED}swgr_buildMapFolder${NC} <Environment name>    - Run swagger resouces to build the map folder used by robots"
  echo -e "      ${RED}swgr_resendNotification${NC}                   - Run swagger notification resouce to resend objects notifications"
  echo -e "      ${RED}api_clean_mem${NC} <service>                   - Run Garbage Collection on API services (admin, machines, wms, routes, safety, viadat)"
  echo -e "      ${RED}cancel_all_printer_jobs${NC} <printer>         - Cancel all printer jobs exist in CUPS"
  echo -e "    Works on SERVICE servers:"
  echo -e "      ${RED}pgsql_db${NC}                                  - connect to database console"
  echo -e "      ${RED}bad_locations${NC} <CSV>                       - show bins in bad locations"
  echo -e "      ${RED}machines_status${NC}                           - show machine status in machines DB table"
  echo -e "      ${RED}all_machines${NC} <CSV>                        - show all known machines in machines DB table"
  echo -e "      ${RED}errors_stopped_wh${NC} <Interval (min)> <CSV>  - show all errors that stopped wh / stop and resume wh / robot needs new route"
  echo -e "      ${RED}errors_by_network${NC} <Interval (min)> <CSV>  - show errors caused by network issues"
  echo -e "      ${RED}skus_zero_qty${NC}                             - find skus with zero quantity by products (copy products list from excel column)"
  echo -e "      ${RED}check_order${NC} <Order#>                      - check if order is locked for recall"
  echo -e "      ${RED}off_machines_errs${NC}                         - show offline machines with errors"
  echo -e "      ${RED}off_machines_missions${NC}                     - show offline machines with missions"
  echo -e "      ${RED}unseen_stickers${NC} <machine ID | all> <CSV>  - show robot unseen stickers"
  echo -e "      ${RED}stickers_list${NC} <Sticker Code | Alias>      - show stickers list or find a single sticker"
  echo -e "      ${RED}etc_hosts${NC}                                 - show list of all known robots and parse it for /etc/hosts use"
  echo -e "      ${RED}show_pareto${NC} <Interval (hours)> <machine ID | all>  <CSV> - show errors pareto"
  echo -e "      ${RED}show_stop_err${NC} <CSV>                       - show the error that stopped the WH"
  echo -e "      ${RED}show_sbc_status${NC} <D>                       - show SBC status, D flag for detailed report"
  echo -e "      ${RED}keshet_web_upd${NC}                            - Generate daily report for Keshet KH"
  echo -e "      ${RED}vacant_storage${NC} <CSV>                      - Show vacant storage by bin type"
  echo -e "      ${RED}scan_network${NC} <First IP In Range i.e 10.30.0.0> - scan robots network"
  echo -e "      ${RED}check_picking_req${NC} <PICK_TICKET | EXTERNAL_ID | TIME | SKUS> <Value> - Check picking requests by selected option"
  echo -e "      ${RED}robots_by_mission_route_start_time${NC} <route start time timestamp> - Get robots list with mission by specific route start time"
  echo
}
 
sites_list() {
  for i in "${!options[@]}"
  do
    num=$((i+1))
    printf "%d) %s\n" $num "${options[$i]}"
  done
}

unsupported() {
  echo -e "\nUnsupported command ${RED}$inpCmd${NC}\n"
  usage
  exit 1
}

invalid_option() {
  echo -e "Invalid option $inpSite \n"
  exit 1
}

validate_pem_premissions() {
  pem_file=${connect_str%%[[:space:]]*}

  if ! [ -f "$pem_file" ]
  then
    echo -e "The PEM file $pem_file does ${RED}NOT${NC} exists, aborting."
    exit 1
  fi

  stat_pem=$(stat -L -c "%a" $pem_file)
  run_user=$(whoami)
  pem_owner=$(ls -ld $pem_file | awk '{print $3}')

  $verbose && echo pem_file = $pem_file
  $verbose && echo stat_pem = $stat_pem
  $verbose && echo run_user = $run_user
  $verbose && echo pem_owner = $pem_owner

  if [[ $stat_pem -ne 400 ]]
  then
    echo -n "pem file $pem_file permissions is too open, should be read only. "
    chmod 400 $pem_file && echo "Fixed permissions to 400 successfully" || echo "Unable to fix pem permissions please do it manually"
  fi

  if [[ $run_user != $pem_owner ]]
  then
    echo -n "pem file $pem_file owner $pem_owner is not the running user $run_user"
    chown $run_user:$run_user $pem_file && echo "Fixed pem file $pem_file owner to $run_user successfully" || echo "Unable to fix pem owner to $run_user please do it manually"
  fi
}

test_robot_connectivity() {
  machine_ip=$(getent hosts $machine_id | cut -f1 -d" ")
  [[ -z $machine_ip ]] && { echo "Machine $machine_id does not exist in /etc/hosts file" ; exit 1 ; }

  ping -w 1 $machine_id > /dev/null 2>&1
  [[ $? -ne 0 ]] && { echo "Robot $machine_id is unreachable (ping $machine_ip)" ; exit 1 ; }

  sshpass -p a ssh -q $machine_id exit
  [[ $? -ne 0 ]] && { echo "Robot $machine_id is unreachable (ssh $machine_ip)" ; exit 1 ; }
}

case_opt() {
  opt=$1

  case $opt in
    Robot_LOCAL)
        connect_str=""
        machine_id=$inpCmdPar1
        test_robot_connectivity ;;
    $selectedCfgOpt)
        connect_str="$pem_folder/$selectedCfgPem $selectedCfgUser@$selectedCfgIP"
        $verbose && echo connect_str=$connect_str
        validate_pem_premissions ;;
    Remmina)
        export DISPLAY=:0 ; remmina > /dev/null 2>&1 ; exit 0 ;;
    Quit)
        exit 0 ;;
    *)  invalid_option ;;
  esac
}

readOptVarsFromCfg(){
  selectedOptCfgLine=$(grep $opt $configFile | tr -d '\r')
  $verbose && echo selectedOptCfgLine=$selectedOptCfgLine

  while IFS=, read -r selectedOpt selectedPem selectedUser selectedIP selectedURL
  do
    selectedCfgOpt=$selectedOpt
    selectedCfgPem=$selectedPem
    selectedCfgUser=$selectedUser
    selectedCfgIP=$selectedIP
    selectedCfgURL=$selectedURL
  done <<< "$selectedOptCfgLine"

  $verbose && echo selectedCfgOpt=$selectedCfgOpt
  $verbose && echo selectedCfgPem=$selectedCfgPem
  $verbose && echo selectedCfgUser=$selectedCfgUser
  $verbose && echo selectedCfgIP=$selectedCfgIP
  $verbose && echo selectedCfgURL=$selectedCfgURL
}

Validate_inpCmd() {
  $verbose && echo -e "inpCmd = $inpCmd"

  # Validate inpCmd as a valid command
  printf '%s\n' "${arrayAllowExeCmd[@]}" | grep -w "${inpCmd}" > /dev/null 2>&1
  res=$?

  if [[ $res -gt 0 ]]
  then
    unsupported
  fi

  # Search inpCmd in arrayAllowExeCmd
  printf '%s\n' "${arrayAllowExeCmd[$serverType]}" | grep -w "${inpCmd}" > /dev/null 2>&1
  res=$?

  if [[ $res -eq 0 ]]
  then
    echo -e "Command ${RED}${inpCmd}${NC} is allowed to run on $serverType server type"
  else
    echo -e "\nCommand ${RED}${inpCmd}${NC} is not allowed to run on $serverType server type. Ignoring command.\n"
    exit 1
  fi
}

Validate_inpSite() {
  re='^[0-9]+$'

  # Search inpSite is not digits
  if ! [[ $inpSite =~ $re ]]
  then
    # Search inpSite in options
    printf '%s\n' "${options[@]}" | grep -w "${inpSite}" > /dev/null 2>&1
    res=$?

    if [[ $res -ne 0 ]]
    then
      invalid_option
    fi
  fi
}

check_pkg_installed() {
  pkg=$1

  apt-cache show $pkg > /dev/null 2>&1
  res=$?

  if [[ $res -eq 0 ]]
  then
    echo -e "Package ${RED}$pkg${NC} installed."
  else
    echo -e "Package ${RED}$pkg${NC} not installed."
    echo -e "Please install the package using: sudo apt-get install -y $pkg\n"
    exit 1
  fi
}

validate_running_user() {
  if [ "$EUID" -eq 0 ]
  then
    echo -e "Running ${RED}Connect${NC} as root / sudoer is not allowed"
    exit 1
  fi
}

build_options_list() {
  # Build options menu from input config file first column
  if [[ "$filter" = "ALL" ]]
  then
    numOpts=$(cat $configFile | wc -l)
    $graphics && catOpt="--number"
    options=($(cat $catOpt $configFile | cut -d, -f1))
    # Add remmina remote desktop option
    $graphics && options+=($((numOpts+1)) Remmina) || options+=(Remmina)
  else
    numOpts=$(grep -i $filter $configFile | wc -l)
    $graphics && options=($(awk '{printf "%d\t%s\n", NR, $0}' <<< $(grep -i $filter $configFile | cut -d, -f1))) || options=($(grep -i $filter $configFile | cut -d, -f1))
  fi

  # Add quit option
  $graphics && options+=($((numOpts+2)) Quit)    || options+=(Quit)
}

### MAIN ###

# Colors for display - will not be reconized on servers
YLW='\033[1;33m'     # Yellow Color
RED='\033[1;31m'     # Red Color
BLINK='\033[33;5;7m' # Blink
NC='\033[0m'         # No Color

validate_running_user

declare -A arrayAllowExeCmd
arrayAllowExeCmd[WEB]="restart_services,noisy_robots,services_status,htop,server_stats,del_sku_bins_loc,swgr_clearAllMachinesErrs,swgr_clearRedis,swgr_removedFromWarehouse,swgr_updateLastEntry,swgr_ordersPackaged,swgr_ordersBoost,swgr_skusBulkUnsuspend,swgr_bulkCancelRecallBin,swgr_bulkUnpauseOrders,swgr_bulkOrdersRelevancy,swgr_buildMapFolder,swgr_resendNotification,search_logs,api_clean_mem,cancel_all_printer_jobs"
arrayAllowExeCmd[JOB]="restart_services,services_status,htop,server_stats,find_instruction_index"
arrayAllowExeCmd[ROUTE]="restart_services,services_status,htop,server_stats"
arrayAllowExeCmd[SERVICE]="bad_locations,machines_status,htop,errors_stopped_wh,errors_by_network,server_stats,skus_zero_qty,check_order,off_machines_errs,off_machines_missions,pgsql_db,unseen_stickers,all_machines,etc_hosts,show_pareto,show_stop_err,show_sbc_status,stickers_list,scan_network,keshet_web_upd,vacant_storage,check_picking_req,robots_by_mission_route_start_time,swgr_removeUsresRole,swgr_usersDelete"
arrayAllowExeCmd[TEST]="restart_services,services_status,htop,server_stats"
arrayAllowExeCmd[LOCAL]="search_robot_logs"

# configFile should contailns the sites list
configFile=$HOME/Connect.cfg

# Handle script swiches
verbose=false
graphics=false

OPTSTRING=":hlf:vg"
#FILTERARGS="^SERVICE|^WEB|^JOB|^ROUTE"
#FILTERARGSDISP="SERVICE | WEB | JOB | ROUTE"
filter="ALL"

while getopts ${OPTSTRING} option; do
  case $option in
    h) usage; exit ;;
    l) build_options_list ; sites_list ; exit ;;
    #f) [[ $OPTARG =~ $FILTERARGS ]] && filter=$OPTARG || { echo "$OPTARG not in filter list $FILTERARGSDISP" ; exit ; };;
    f) filter=$OPTARG ;;
    v) verbose=true ;;
    g) graphics=true ;;
    :) echo "Option -${OPTARG} requires an argument." ; exit 1 ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit 1 ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

connect_opts=()
$verbose && connect_opts+=( -v )
$graphics && connect_opts+=( -g )
# $long && connect_opts+=( -l )
# ls "${ls_opts[@]}" "$@"

inpSite=$1
inpCmd=$2
inpCmdPar1=$3
inpCmdPar2=$4
inpCmdPar3=$5
inpCmdPar4=$6

re='^[0-9]+$'

build_options_list

$verbose && echo inpSite    = $inpSite
$verbose && echo inpCmd     = $inpCmd
$verbose && echo inpCmdPar1 = $inpCmdPar1
$verbose && echo inpCmdPar2 = $inpCmdPar2
$verbose && echo inpCmdPar3 = $inpCmdPar3
$verbose && echo inpCmdPar4 = $inpCmdPar4

case $inpSite in
      "") $graphics || clear ;;
       *) Validate_inpSite   ;;
esac

connect_str=""
opt=""
pem_folder="$HOME/.ssh/pem_files"
ssh_flags="-q -o ConnectTimeout=5 -o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
typeset_funcs="run_sql parse_yaml working_machines_db check_be_version services_status_srv stickers_by_alias_db never_seen_stickers_db"

# clear
echo -e "\nSelect Caja site to SSH to...\n"
echo -e "${YLW}Make sure that VPN to the site is turned on${NC}\n"

# Display the list of options
COLUMNS=1

# If no input site #, show interactive list
if [[ -z $inpSite ]]
then
  if [ "$graphics" = true ]
  then
    # --print-column="2" is used to return the value from the second column and not the first
    # ((sleep .4 ; wmctrl -r Service -R Service -e 0,50,20,-1,-1) wmctrl -R Service) | 
    title="Service - Connect Tool"
    opt=$(zenity --width=300 --height=900 --title="$title" --text="Select Caja site to SSH to..." --list --column="#" --column="Option" "${options[@]}" --print-column="2" && (sleep .4 ; wmctrl -r $title -R $title -e 0,50,20,-1,-1))
  else
    PS3=$'\n'"Select the option: "
  
    select opt in "${options[@]}"
    do
      break
    done
  fi

  $verbose && echo "opt = $opt"
  readOptVarsFromCfg
  case_opt $opt
else
  if [[ $inpSite =~ $re ]]
  then
    $verbose && echo ${options[$inpSite-1]}
    opt=${options[$inpSite-1]}
  else
    opt=$inpSite
  fi
  $verbose && echo "opt: $opt"

  readOptVarsFromCfg
  case_opt $opt
  server_ip=${connect_str#*@}
  $verbose && echo "opt: $opt"
  $verbose && echo "Server IP: $server_ip"

  if [[ -z $inpCmd ]]
  then
    commands=""
  else
    if [[ $inpSite =~ $re ]]
    then
      serverType=${options[$inpSite-1]##*_}
    else
      serverType=${inpSite##*_}
    fi
    $verbose && echo -e "serverType = $serverType"

    Validate_inpCmd

    [[ ${inpCmd:0:5} == "swgr_" ]] && check_pkg_installed curl

    echo -e "Connecting to ${RED}$(echo $opt | tr '_' ' ')${NC} with IP: ${RED}$selectedCfgIP${NC} \n"

    # Execute the selected command
    $inpCmd $inpCmdPar1 $inpCmdPar2 $inpCmdPar3 $inpCmdPar4

    commands="exit"
  fi
fi

if [[ $connect_str ]] && [[ ${inpCmd:0:5} != "swgr_" ]] && [[ $commands != "exit" ]]
then
  #gpg -d -q .sshpassword.gpg > fifo
  $verbose && echo -e "connect_str = $connect_str \n"

  connect_IP=$(echo "$connect_str" | awk -F "@" '{print $2}')

  check_pkg_installed sshpass

  if [[ "$connect_IP" == "10.10.10.10" ]]
  then
    echo -e "Removing IP 10.10.10.10 from known_hosts" 
    sshpass -f fifo sudo ssh-keygen -f "/root/.ssh/known_hosts" -R "10.10.10.10"
  fi

  echo -e "Connecting to ${RED}$(echo $opt | tr '_' ' ')${NC} with IP: ${RED}$connect_IP${NC} \n"

  i=1
  sp="/-\|"

  $verbose && echo ops = "${connect_opts[@]}" "$@"
  $verbose && echo verbose = $verbose

  $verbose && echo -e "SSH command: ssh -t $ssh_flags -i $connect_str \"\$commands\" \n"

  while ! (sshpass -f fifo ssh -t $ssh_flags -i $connect_str "$commands")
  do
    echo -ne "SSH failed, retrying...    "
    echo -ne "\b\b\b[${sp:i++%${#sp}:1}]\r"
    sleep 2s
  done
fi

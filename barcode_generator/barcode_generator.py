# Prerequsite: packages python-barcode and pillow should be installed
# pip install python-barcode
# pip install pillow
# pip install colorama --break-system-packages
# pip install fpdf --break-system-packages

import sys
import os
from colorama import Fore, Back, Style

# import Code128 from barcode module
#from barcode import Code128
import barcode

# import ImageWriter to generate an image file
from barcode.writer import ImageWriter

from fpdf import FPDF

script = sys.argv[0]

if len(sys.argv) <= 2:
    print("Please enter a valid code file name and code Type\nUsage:",script,"<Codes file name> <Code type>")
    sys.exit(1)
else:
    inFile = sys.argv[1]
    codeType = sys.argv[2].lower()

    if os.path.isfile(inFile):
        print ("File",Fore.RED + inFile + Fore.RESET,"exist")
    else:
        print ("File",Fore.RED + inFile + Fore.RESET,"does not exist")
        sys.exit(1)

try:
    CODE_TYPE = barcode.get_barcode_class(codeType)
    print ("Code",Fore.RED + codeType + Fore.RESET,"exist in barcode library")
except:
    print ("Code",Fore.RED + codeType + Fore.RESET,"does not exist in barcode library")
    print ("Supported codes can be found in the list below:\n",barcode.PROVIDED_BARCODES)
    sys.exit(1)

# Using readlines()
file1 = open(inFile, 'r')
Lines = file1.readlines()

if len(Lines) == 0:
    print ("File",Fore.RED + inFile + Fore.RESET,"is empty")
    sys.exit(1)

folder_path = ("codes_" + codeType)
os.makedirs(folder_path, exist_ok=True) 

count = 0
imagelist = []

# Strips the newline character
for line in Lines:
    code = "%s" % line.strip()

    if not code:
        print ("Read empty line, skipping")
    else:
      count += 1

      file_name = code + "_" + codeType
      print(file_name)

      # Now, let's create an object of EAN13 class and
      # pass the number with the ImageWriter() as the
      # writer
      my_code = CODE_TYPE(code , writer=ImageWriter())

      # Our barcode is ready. Let's save it.
      file_path = os.path.join(folder_path, file_name)
      my_code.save(file_path)
      
      imagelist .append(file_name)

print("Total barcode lines:",count)

file1.close()

# Join png files to a single pdf file

pdf = FPDF()
# imagelist is the list with all image filenames
#imagelist =  ["upcCode0001_gs1_128.png","upcCode0002_gs1_128.png"]
start_x = 20
start_y = 15
x = start_x
y = start_y
w = 80
h = 30

cnt = 0

pdf.add_page()

for image_name in imagelist:
    image_path = os.path.join(folder_path, image_name + ".png")
    pdf.image(image_path,x,y,w,h)

    if (cnt % 2) == 0:
        x = x + w + 10
    else:
        x = start_x
        y = y + h + 10

    cnt += 1

    if (cnt % 14) == 0:
        pdf.add_page()
        x = start_x
        y = start_y

pdf.output(codeType + ".pdf", "F")

sys.exit(0)

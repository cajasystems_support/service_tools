#!/bin/bash

usage() {
  echo -e "\nUsage: $0 <Site name>" >&2
  echo -e "  Site name  - Site name like Deckers | Keshet | VM | Gaia | DSV | Clini"
  echo -e "  -h flag    - show this usage help"
  echo -e "  -v flag    - display verbose messages"
  exit 1
}

sync_files_with_machine() {
  echo -e "\nSyncing files with ${RED}$MACHINE_NAME${NC} machine"

  if $(test_connectivity)
  then
    # Go over all files in files_to_deploy folder and sync one by one
    FILES_TO_SYNC="$(find files_to_deploy -type f)"
    for SOURCE_PATH in $FILES_TO_SYNC ; do
        SOURCE_FILE=${SOURCE_PATH##*/}

        $verbose && echo "SOURCE_PATH=$SOURCE_PATH"
        $verbose && echo "SOURCE_FILE=$SOURCE_FILE"

        REMOTE_DIR="$(grep $SOURCE_FILE $configFile | awk -F, '{print $2}')"
        #[ -n "$REMOTE_DIR" ] && REMOTE_DIR=${REMOTE_DIR::-1}

        $verbose && echo "REMOTE_DIR=$REMOTE_DIR"

        [ -z "$REMOTE_DIR" ] && REMOTE_DIR_D="~" || REMOTE_DIR_D=$REMOTE_DIR
        echo -en "=> Syncing ${RED}$SOURCE_FILE${NC} file with remote ${RED}$REMOTE_DIR_D${NC} folder "

        if [ $verbose = true ]
        then
          echo rsync -e \"ssh -i $PEM_PATH\" -avzpP $SOURCE_PATH $REMOTE_USER@$REMOTE_IP:$REMOTE_HOME/$REMOTE_DIR
        else
          rsync -e "ssh -i $PEM_PATH" -azq $SOURCE_PATH $REMOTE_USER@$REMOTE_IP:$REMOTE_HOME/$REMOTE_DIR &&
                echo -e " => rsync ${GRN}succeed${NC}" 1>&2 || echo -e " => rsync ${RED}failed${NC}" 1>&2
        fi
    done
  fi
}

test_connectivity() {
  if [ -f "$PEM_PATH" ]
  then
    ssh -n -i $PEM_PATH $REMOTE_USER@$REMOTE_IP -o ConnectTimeout=2 -q exit
    [ $? = 0 ] && echo true || { echo -e "${RED}Connection timeout${NC} with server" >&2 ; echo false ; }
  else
    echo -e "PEM file $PEM_PATH ${RED}not found.${NC}" >&2
    echo false
  fi
}

exec_sync() {
  # Go over all selected machines and perform the sync
  while IFS=, read -r MACHINE_NAME PEM_FILE REMOTE_USER REMOTE_IP
  do
    REMOTE_IP=${REMOTE_IP::-1}
    REMOTE_HOME=/home/$REMOTE_USER
    $verbose && echo -e "read: $MACHINE_NAME $PEM_FILE $REMOTE_USER $REMOTE_IP"

    PEM_PATH=$PEM_FOLDER/$PEM_FILE
    sync_files_with_machine
  done <<< $selectedList
}

sync_machines() {
  $verbose && echo -e "selectedList:\n$selectedList"

  echo -e "\nSelected Machines:"
  list="$(echo "$selectedList" | awk -F, '{print "\t" $1}')"
  echo "$list"

  FILES_TO_SYNC="$(ls -1A $SOURCE_DIR)"

  echo -e "\nFiles to Sync:"
  for file in $FILES_TO_SYNC ; do
    echo -e "\t${file##*/}"
  done

  echo -en "\nAre you sure you would like to continue with the sync? (Yy). "
  read -p "[ ]"$'\b\b' -n 1 -r
  echo

  [[ $REPLY =~ ^[Yy]$ ]] && exec_sync || echo -e "\nAborting."
}

sync_all_machines() {
  echo -e "Syncing ${RED}ALL${NC} machines"

  selectedList="$(cat $machinesList)"
  sync_machines
}

sync_selected_machines() {
  echo -e "Syncing ${RED}SELECTED${NC} machine"

  selectedList="$(cat $machinesList | grep -i $SITE)"
  sync_machines
}

### MAIN ###

# Handle script swiches
verbose=false

while getopts ":vh" option; do
  case $option in
    h) usage        ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

SITE=$1
[ -z "$SITE" ] && SITE="ALL"

RED="\e[31m"     # Red
GRN="\e[32m"     # Green
NC='\033[0m'     # No Color

PEM_FOLDER=$HOME/.ssh/pem_files
SOURCE_DIR=$HOME/deploy_service/files_to_deploy

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
mkdir -p files_to_deploy

ssh_flags="-q -o ConnectTimeout=5 -o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
configFile=$SCRIPT_DIR/deploy_service.cfg
machinesList=$SCRIPT_DIR/service_machines.cfg

config="$(cat "$configFile" | awk -F, '{print $1 " " $2}')"
machines="$(cat "$machinesList" | awk -F, '{print $1 " " $2 " " $3 " " $4}')"

$verbose && echo -e "config:\n$config\n"
$verbose && echo -e "machines:\n$machines\n"

[ $(echo $(find files_to_deploy -type f -name "*.sh" | grep -q .;echo $?)) -eq 0 ] && chmod +x $SOURCE_DIR/*sh

[ $SITE = ALL ] && sync_all_machines || sync_selected_machines

exit 0

#!/bin/bash

usage() {
  echo "Usage: $0 <user> <password> <first scan ip>" >&2
}

# Handle script swiches
verbose=false

while getopts ":vh" option; do
  case $option in
    h) usage; exit ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

if [ "$#" -ne 3 ]
then
  usage
  exit 1
fi

user=$1
pass=$2
ip=$3

START="${ip##*.}"
END=255

first_ip=$(awk -F"." '{print $1"."$2"."$3}'<<<$ip)

$verbose && echo user $user
$verbose && echo pass $pass
$verbose && echo ip $ip
$verbose && echo START $START
$verbose && echo END $END
$verbose && echo first_ip of scan is $first_ip

for (( i=$START; i<=$END; i++ ))
do
    test_ip=$first_ip.$i
    echo -en "IP: $test_ip "

    ping -w 1 $test_ip > /dev/null 2>&1
    ping_ret_val=$?

    if [[ $ping_ret_val -ne 0 ]]
    then
      echo "unreachable"
      sleep 0.2
    else
      sshpass -p $pass ssh -o NumberOfPasswordPrompts=1 $user@$test_ip
    fi
done

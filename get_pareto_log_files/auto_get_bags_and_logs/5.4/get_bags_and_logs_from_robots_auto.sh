#!/bin/bash

find_logs() {
    ts=$1
    day=$(date -d @$ts +%F -u)

    cd /caja/archive-logs

    # serach for timestamp in tar.gz files, ignore errors like "unexpected end of file"
    result=$(zgrep -l $ts $day* 2> /dev/null)

    # if no result, search 1 day forward
    if [[ -z "$result" ]]
    then
      day=$(date +%F -d "$day+1day")
      result=$(zgrep -l $ts $day* 2> /dev/null)
    fi

    echo $result
}

# include parse_yaml function
source /home/ubuntu/auto_get_bags_and_logs/parse_yaml.sh

robot=$1
timestamp=$2
logOnly=$3
useSCP=$4

global_yml="/home/ubuntu/.config/global.yml"
eval $(parse_yaml $global_yml)

echo "WH_name = $WH_name"

# Convert timestamp to human readable to be use with output filename
timestampH=$(date -d @$timestamp +%d-%m-%Y_%H_%M_%S)

time=$(date -d @$timestamp -u "+%F @ %T GMT+0")
printf "\n>>> your timestamp = $time\n\n"

dest_folder="/mnt/Service/log"
pub_file="~/.ssh/id_rsa_robots"
ssh_flags="-o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"

# Connect to robot and find a log that contain the timestamp
result=$(ssh -n $ssh_flags $robot "$(typeset -f) ; find_logs $timestamp")
resultb=${result:0:20}bags.tar.gz

sleep 1

if [[ -z "$result" ]]
then
  echo "No matching log file found on the $robot with $timestamp"
  exit 1
else
  echo "Found log $result"
  output_log=${WH_name}_${robot}_${timestampH}_${timestamp}_${result}
  output_bag=${WH_name}_${robot}_${timestampH}_${timestamp}_${resultb}

  echo "output log name: " $output_log
  if [ "$useSCP" = false ]
  then
    rsync -azh -e "ssh $ssh_flags " $robot:/caja/archive-logs/$result  $dest_folder/logs-from-robots/$output_log
  else
    scp -C $ssh_flags $robot:/caja/archive-logs/$result  $dest_folder/logs-from-robots/$output_log
  fi

  if [ "$logOnly" = false ]
  then
    echo "output bag name: " $output_bag

    if [ "$useSCP" = false ]
    then
      rsync -azh -e "ssh $ssh_flags " $robot:/caja/archive-bags/$resultb $dest_folder/bags-from-robots/$output_bag
    else
      scp -C $ssh_flags $robot:/caja/archive-bags/$resultb $dest_folder/bags-from-robots/$output_bag
    fi

    # Delete bags file with size 45 Bytes
    if [ $? -eq 0 ]
    then
      bagSize=$(wc -c < $dest_folder/bags-from-robots/$output_bag)
      if [ $bagSize -eq 45 ]
      then
        echo "File $dest_folder/bags-from-robots/$output_bag size is $bagSize Bytes, therefore will be deleted"
        rm $dest_folder/bags-from-robots/$output_bag
      fi
    fi
  else
    echo "LogOnly flag is set to True : Requested for log only, no BAG file is required"
  fi

  exit 0
fi

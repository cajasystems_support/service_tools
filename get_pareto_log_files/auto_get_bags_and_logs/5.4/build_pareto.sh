#!/bin/bash

# include parse_yaml function
source /home/ubuntu/auto_get_bags_and_logs/parse_yaml.sh

db_yml="/home/ubuntu/.config/database.yml"
eval $(parse_yaml $db_yml)

# echo "pgsql_host = $pgsql_host"
# echo "pgsql_name = $pgsql_name"
# echo "pgsql_user = $pgsql_user"
# echo "pgsql_pass = $pgsql_pass"

export PGPASSWORD=$pgsql_pass

# IFS=$'\n'

interval="'$1 hour'"

sql_1="
with all_errors as (
	with errors as (
		select
		    *,
		    to_char( to_timestamp( (nextActiveTs/1000) - 4 * 60 * 60 ), 'yyyy/MM/dd HH24:MI:SS' :: TEXT ) AS nextActivation,
		    to_char( to_timestamp( (nextStopTs/1000) - 4 * 60 * 60 ), 'yyyy/MM/dd HH24:MI:SS' :: TEXT ) AS nextStop,
		    to_char( to_timestamp( (formerStopTs/1000) - 4 * 60 * 60 ), 'yyyy/MM/dd HH24:MI:SS' :: TEXT ) AS formerStop,
		    to_char( to_timestamp( (formerStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) AS formerStopDay,
		    to_char( to_timestamp( (nextStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) AS nextStopDay,
		    case	when src.error_action <> 'STOP_WAREHOUSE' then 0
		    		else ( nextActiveTs - nextStopTs ) / 1000
		    		end  as stopDuration_S,
		    case	when src.error_action <> 'STOP_WAREHOUSE' then 0
		    		else ROUND(( nextActiveTs - nextStopTs ) / 1000::numeric / 60::numeric,2)
		    		end  as stopDuration_min,
		    case	when src.error_action <> 'STOP_WAREHOUSE' then 0
		    		else ROUND(( nextActiveTs - nextStopTs ) / 1000::numeric / 60::numeric / 60::numeric,2)
		    		end  as stopDuration_hr,
		    case    when src.error_action in ('STOP_WAREHOUSE') and src.warehouse_state not in ('SHUTDOWN','RESUME_AND_ACTIVATE','RESUME_AND_SHUTDOWN') and to_char( to_timestamp( (formerStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) = to_char( to_timestamp( (nextStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) then ts - formerStopTs/1000 
		    		else null
		    		end as dt_s,
		    case	when src.error_action in ('STOP_WAREHOUSE') and src.warehouse_state not in ('SHUTDOWN','RESUME_AND_ACTIVATE','RESUME_AND_SHUTDOWN') and to_char( to_timestamp( (formerStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) = to_char( to_timestamp( (nextStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) then ROUND((ts - formerStopTs/1000)::numeric/60,2) 
		    		else null
		    		end as dt_m,
			case	when src.error_action in ('STOP_WAREHOUSE') and src.warehouse_state not in ('SHUTDOWN','RESUME_AND_ACTIVATE','RESUME_AND_SHUTDOWN') and to_char( to_timestamp( (formerStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) = to_char( to_timestamp( (nextStopTs/1000) - 4 * 60 * 60 ), 'dd' :: TEXT ) then ROUND((ts - formerStopTs/1000)::numeric/60/60,2)
		    		else null
		    		end as dt_hr
		FROM
		(
		    select
		    	to_char( to_timestamp(((((((  created_on / 1000 )- 4 * 60 * 60))))) :: DOUBLE PRECISION ), 'yyyy/MM/dd' :: TEXT ) AS "Date",
		        to_char( to_timestamp((((((( error_notifications.created_on / 1000 ) - 4 * 60 * 60))))) :: DOUBLE PRECISION ), 'yyyy/MM/dd HH24:MI:SS' :: TEXT ) AS "WH_time",
		        to_char( to_timestamp((((((( error_notifications.created_on / 1000 ) ))))) :: DOUBLE PRECISION ), 'yyyy/MM/dd HH24:MI:SS' :: TEXT ) AS gmt_time,
		        created_on / 1000 AS ts,
		        machine_id,
		        case when
    					name like any (array['%t communicate with Side Camera','Robot slipped while in location work','Robot slipped and rotated while in location work','PickingAccident','FrontRobotInsidePicking',
    					'Invalid mission - loop route on completed mission','Robot slipped and rotated while delivering','Robot slipped while delivering','Last mission seen/unseen stickers list','Success on%']) 
    					then 'NOTIFICATION'
    					when
    					name in ('Bin mishandled (bin sensor validation failed)','No side sticker in slide to target', 'Robot abandoned delivery', 'Robot abandoned retrieve',
    					'Attempting to retrieve bin, but location is empty','Attempting to deliver bin, but location is occupied')
    					then 'LOCATIONAL'
    					when
    					name like any (array['%t manage to stop at target','Bad bin approach. Can not slide to target', 'Cannot complete stop at in time', 'Elevator go to min position failed',
    					'Elevator + Theta move failed','illegal stop at point - after HOLD','Invalid mission - first ttl is way in the past',
    					'KA response exceeded timeout','Lidar imminent collision detection','Robot already passed stop at point','Robot pose is uncertain','Invalid mission - empty mission',
    					'Robot is canceling mission, cannot take refresh','%t communicate with Shelf Camera','%t communicate with Ground Camera','%t communicate with Front Camera','%t communication with IMU','%t communicate with Lidar','%t communicate with Odometry','%t communicate with BMS','blocked queue entry, waiting',
    					'Gripper rotation failed'])
    					then 'EXTENDABLE'
    					when
    					name in ('chargingNeeded', 'goToMaintenanceOrderCompletingMissions','Bad bin approach. Go to maintenance','Front camera did not detect sticker',
    					'Go to maintenance after BB location work fail', 'Too many location disqualification. Go to maintenance', 'goToMaintenanceOrderImmediately','Go to maintenance after BB delivery fail','Go to maintenance after BB retrieve fail',
    					'Machine needs charging','User invoked go to maintenance after missions')
    					then 'MAINTENANCE'
    					when
    					name like any (array['connectionLost','Connection was lost with machine'])
    					then 'RESTORABLE'
    					when
    					name like any (array['Robot went far from path','Robot went far from path, recoverable','Lidar detection, remove obstacle','Position jump linear','Barcode jump - check if sticker is correct in map',
    					'Bin fell while driving','Position jump linear in queue','machineFailedKeepAliveTooManyTimes','Keep alive failed too many times'])
    					then 'RECOVERABLE'
    					when
    					name like any (array['Drive Roboteq not responding to commands','Deliver failed - place bin INSIDE LOCATION and reboot machine','Lifter homing failed','Lifter received an illegal command'
    					,'No barcode for too long','Position jump turn','Retrieve failed - place bin ON ROBOT and reboot machine','Bin mishandled - place bin ON ROBOT and reboot machine',
    					'Robot failed searching floor sticker','Robot is too close to shelves','Bin mishandled - place bin INSIDE LOCATION and reboot machine',
    					'%t complete turn on place','unknown stop reason','Not all robot sensors are up','Robot failed to accept REFRESH','EKF freeze - pure odom is moving but odom not','General rebootable error',
    					'Lidar collision timed-out','Retrieve failed - place bin ON ROBOT and activate machine','Deliver failed - place bin INSIDE LOCATION and activate machine','Critical sensor timed-out',
    					'Bin mishandled - place bin ON ROBOT and activate machine'])
    					then 'REBOOTABLE'
    					when
    					name like any (array['Bms reports battery level is under threshold','Emergency Button Pressed','Picking station was not detected','machineWokeUpOffRouteInQueueError',
    					'machineTooCloseToAnotherMachine','tooManyErrors','machineDidNotWakeUpForTooLong','machineDeviatedFromRouteTooManyTimes',
    					'machineGetsUnsolvableRoutesProblems','wakeupPathRequestError','failedResume','%t recover in queue','IllegalLoadedBinStatusError','Elevator move z failed',
    					'Machine did not wake up for too long','Machine is too close to another machine','Woke up in illegal position','Caused last resume attempt to fail',
    					'Machine deviated from route too many times'])
    					then 'UNRECOVERABLE'
				end as error_type,
				"name",
		        description,
		        error_action,
		        metadatajson,
		        warehouse_state,
		        mission_type,
		        mission_version,
		        frame_code,
		        sticker_code,
		        machine_orientation,
		        machine_velocity,
		        mission_id,
		        bin_code,
		        machine_x,
		        machine_y,
		        mission_instruction_index,
		        task_type,
		        task_id,
		        machine_status,
		        ( SELECT started_on FROM warehouse_activity_log WHERE warehouse_activity_log."state" = 'ACTIVE' AND warehouse_activity_log.started_on > error_notifications.created_on ORDER BY started_on ASC LIMIT 1 ) AS nextActiveTs,
		        ( SELECT started_on FROM warehouse_activity_log WHERE warehouse_activity_log."state" = 'STOPPED' AND warehouse_activity_log.started_on > error_notifications.created_on ORDER BY started_on ASC LIMIT 1 ) AS nextStopTs,
		        ( SELECT started_on FROM warehouse_activity_log WHERE warehouse_activity_log."state" = 'STOPPED' AND warehouse_activity_log.started_on < error_notifications.created_on ORDER BY started_on DESC LIMIT 1 ) AS formerStopTs
		    FROM
		        error_notifications
		    where
			    status = 'EXISTS' 
			    and created_on >= extract ('epoch' from ('now'::timestamp - ${interval}::interval)) * 1000
		    ORDER BY
		        created_on DESC
		) AS src
	)

	select
		"gmt_time",
		"WH_time",
		"ts",
		"machine_id",
		"error_type",
		"name",
		"description",
		"stopduration_s",
		"error_action",
		"warehouse_state",
		"sticker_code" as "sticker",
		"frame_code" as "frame",
		"machine_orientation",
		"bin_code"
	from errors
	where warehouse_state not in ('STOPPED','SHUTDOWN')
	and error_action='STOP_WAREHOUSE'
), top_errors as (
	select
		*,
		case when error_type='UNRECOVERABLE' then count*10
			when error_type='REBOOTABLE' then count*7
			when error_type='RESTORABLE' then count*5
			when error_type='RECOVERABLE' then count*2
			when error_type not in ('UNRECOVERABLE','REBOOTABLE','RESTORABLE','RECOVERABLE') then 0
		end as "weight"
	from (
		select
			now() as time,
			error_type,
			name,
			count(*) as count
		from all_errors
		group by error_type, name
		order by count desc
	) as errors_agregate
	order by weight desc
)

select *
from all_errors
where name in (select name from top_errors)
order by ts asc
"

Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c "${sql_1}"`

if [[ -n "$Database"  ]]
then
  echo "$Database"
fi

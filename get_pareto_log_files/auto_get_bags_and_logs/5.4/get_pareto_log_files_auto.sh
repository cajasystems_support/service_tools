#!/bin/bash

RED='\033[1;31m'
NC='\033[0m' # No Color

echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

if [[ $# -eq 0 ]] ; then
  echo -e "\ninsufficient arguments: <CSV filename> <SCP>"
  echo -e "Usage: '$0 <CSV filename>' <SCP>\n"
  exit 1
fi

FILE=$1
echo "CSV File name is: $FILE"

SCP=$2
if [[ $SCP == "SCP" ]]
then
  useSCP=true
  echo "Will use SCP for file copying"
else
  useSCP=false
  echo "Will use RSYNC for file copying"
fi

line_count=0
processed=0
failed_counter=0
succeed_counter=0

logOnlyErrArr=(
  "Lifter homing failed"
  "Critical sensor timed-out"
  "Not all robot sensors are up"
  "EKF freeze - pure odom is moving but odom not"
  "Elevator homing failed"
  "Elevator move z failed"
  "Not all robot sensors are up"
  "Machine did not wake up for too long"
)

ignoreErrArr=(
  "can't recover in queue"
  "Connection was lost with machine"
  "Emergency Button Pressed"
  "Machine is too close to another machine"
  "Woke up in illegal position"
)

# Serach for tabs in input file
awk '{exit !/\t/}' $FILE

if [[ $? = 0 ]]
then
  echo -e "Found tab delimited file \n"
  IFS=$'\t'
else
  echo -e "Found comma delimited file \n"
  IFS=","
fi

# Read input file line by line delimited by IFS=
while read gmt_time WH_time timestamp machine_id error_type name f7 f8 f9 f10
do
  ((line_count++))

  echo -e "\n####################################"
  echo -e "CSV line: $line_count"
  echo -e "gmt_time = $gmt_time"
  echo -e "WH_time = $WH_time"
  echo -e "timestamp = $timestamp"
  echo -e "machine_id = $machine_id"
  echo -e "error_type = $error_type"
  echo -e "name = $name"

  # Check timestamp validaty and that the machine_id is not empty
  if [[ $(date -d @$timestamp 2> /dev/null) ]] && [[ -n "$machine_id" ]]
  then

    # Serch error in ignoreErrArr
    printf '%s\n' "${ignoreErrArr[@]}" | grep "${name}" > /dev/null 2>&1
    res=$?

    if [[ $res -eq 0 ]]
    then
      ignoreLine=true
	  echo "Error is not relevant for investigation, ignoring error"
    else
      ignoreLine=false
	  echo "Error is relevant for investigation, continue grabbing files"

      # parse robot name for script use, remove prefix i.e BB2 or OP1
      machine_id=${machine_id:3}
      # Make it lowercase
      machine_id=${machine_id,,}

      ping -w 1 $machine_id > /dev/null 2>&1
      ping_ret_val=$?

      if [[ $ping_ret_val -ne 0 ]]
      then
        echo "Robot $machine_id is unreachable"
        ((failed_counter++))
      else

        # Check if script was execute from cron
        if [[ -t 1 ]]
        then
          # Not from cron since FD 1 (stdout) is open
          echo -e "CSV line: $line_count, getting log files for: ${RED}$machine_id${NC} with ${RED}$timestamp${NC} timestamp"
        else
          echo -e "CSV line: $line_count, getting log files for: $machine_id with $timestamp timestamp"
        fi

        printf '%s\n' "${logOnlyErrArr[@]}" | grep "${name}" > /dev/null 2>&1
        res=$?

        if [[ $res -eq 0 ]]
        then
          logOnly=true
        else
          logOnly=false
        fi

        /bin/bash /home/ubuntu/auto_get_bags_and_logs/get_bags_and_logs_from_robots_auto.sh $machine_id $timestamp $logOnly $useSCP
        ret_val=$?

        if [[ $ret_val -ne 0 ]]
        then
          echo "Error code: " $ret_val
        fi

        if [[ $ret_val -ne 0 ]]
        then
          ((failed_counter++))
        else
          ((succeed_counter++))
        fi
      fi
      ((processed++))
    fi
  else
    echo "Skipped."
  fi
done < "$FILE"

echo -e "####################################\n"
echo -e "Summary:"
echo -e "Total CSV lines: $line_count"
echo -e "Processed lines: $processed"
echo -e "Succeed lines:   $succeed_counter"
echo -e "Failed lines:    $failed_counter"
echo -e "Skipped lines:   " $(( $line_count - $processed ))

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0

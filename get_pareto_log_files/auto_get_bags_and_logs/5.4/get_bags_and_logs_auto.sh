#!/bin/bash

echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

interval=$1
ts=$(date +%s)

home="/home/ubuntu/auto_get_bags_and_logs"
output_csv="$home/archive/auto_pareto_$ts.csv"

if [[ -z $interval ]]
then
  echo "No input interval using 1 "
  interval=1
fi

echo "Using $interval interval"

source $home/build_pareto.sh $interval > $output_csv

# Check file is not empty
if [[ -s $output_csv ]]
then
  source $home/get_pareto_log_files_auto.sh $output_csv
else
  echo "Pareto CSV file is empty, nothing to do."
fi

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0

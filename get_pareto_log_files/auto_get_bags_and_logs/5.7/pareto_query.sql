select to_timestamp(created_on/1000) AT TIME ZONE 'UTC' as gmt,
       created_on/1000 as ts, machine_id, name, description, metadatajson, sticker_code,
       error_group, error_action,warehouse_state ,frame_code,
       (next_activation - created_on)/1000 as error_duration  ,battery_level
from all_error_notifications
where error_action in ('SET_LOST_IN_WAREHOUSE','SET_FAULTY','STOP_AND_RESUME_WAREHOUSE','STOP_WAREHOUSE') 
  and status = 'EXISTS'
  and not sticker_code is null
  and not warehouse_state in ('STOPPED','SHUTDOWN')
  and created_on >=  extract ('epoch' from ('now'::timestamp with time zone - ${interval}::interval))*1000
  and created_on <=  extract(epoch FROM now())*1000
order by created_on asc

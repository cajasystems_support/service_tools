#!/bin/bash

# This function is perfoemed on the Robot side
find_logs() {
  ts=$1
  day=$(date -d @$ts +%F -u)

  cd /caja/archive-logs

  # serach for timestamp in tar.gz files, ignore errors like "unexpected end of file"
  result=$(zgrep -l $ts $day* 2> /dev/null)

  # if no result, search 1 day forward
  if [[ -z "$result" ]]
  then
    day=$(date +%F -d "$day+1day")
    result=$(zgrep -l $ts $day* 2> /dev/null)
  fi

  echo $result
}

# This function is perfoemed on the Robot side
copy_to_local_robot() {
  localFile=$1
  targetFile=$2
  localUser=$3
  localIP=$4

  localPC="$localUser@$localIP"

  #ssh-add -L

  # Copy log file to a 3rd machine on the network
  res=$(rsync -azh -e "ssh -o StrictHostKeyChecking=no" $localFile $localPC:$targetFile)

  echo $res
}

search_bag() {
  if [ -f $1 ]
  then
    bagSize=$(wc -c < $1)
    [ $bagSize -eq 45 ] && echo 2 || echo 0
  else
    echo 1
  fi
}

# This function is perfoemed on the service machine
copy_to_local() {
  pkill ssh-agent
  eval $(ssh-agent) # Start ssh-agent
  ssh-add $pub_file # add ssh key

  echo "Copy file $localLogFile to $targetLogFile"
  # Connecting to the Robot and executing copy the LOG file to a 3rd machine
  res=$(ssh -n -A $robot "$(typeset -f) ; copy_to_local_robot $localLogFile $targetLogFile $localUser $localIP")
  echo "res=$res"

  if [ -z "$res" ]
  then
    echo "copy_to_local_robot $localLogFile $targetLogFile succeed"
  else
    echo "copy_to_local_robot $localLogFile $targetLogFile failed"
  fi

  # Connecting to the Robot and executing copy the BAG file to a 3rd machine
  if [ "$logOnly" = false ]
  then
    echo -e "Source bag: $localBagFile"

    res=$(ssh -n -A $robot "$(typeset -f) ; search_bag $localBagFile")

    case $res in
      0) echo "BAG file $localBagFile exist"
         res=$(ssh -n -t -A $robot "$(typeset -f) ; copy_to_local_robot $localBagFile $targetBagFile")
         if [ -z "$res" ]
         then
           echo "copy_to_local_robot $localBagFile $targetBagFile succeed"
         else
           echo "copy_to_local_robot $localBagFile $targetBagFile failed"
         fi
         ;;
      1) echo "BAG file $localBagFile does not exist." ;;
      2) echo "BAG file $localBagFile size is too small, ignoring." ;;
    esac
  fi
}

copy_to_amazon() {
  $verbose && echo "Files will be copy to Amazon server"
  # Copy log file
  echo "output log name: " $output_log

  if [ "$scp" = false ]
  then
    echo -e "Running: rsync -azh -e \"ssh $ssh_flags \" $robot:$localLogFile $targetLogFile"
    rsync -azh -e "ssh $ssh_flags " $robot:$localLogFile $targetLogFile
  else
    echo -e "Running: scp $ssh_flags $robot:$localLogFile $targetLogFile"
    scp $ssh_flags $robot:$localLogFile $targetLogFile
  fi

  # Copy bag file
  if [ "$logOnly" = false ]
  then
    echo "output bag name: " $output_bag

    if [ "$scp" = false ]
    then
      echo -e "Running: rsync -azh -e \"ssh $ssh_flags \" $robot:$localBagFile $targetBagFile"
      rsync -azh -e "ssh $ssh_flags " $robot:$localBagFile $targetBagFile
    else
      echo -e "Running: scp $ssh_flags $robot:$localBagFile $targetBagFile"
      scp $ssh_flags $robot:$localBagFile $targetBagFile
    fi

    # Delete bags file with size 45 Bytes
    if [ $? -eq 0 ]
    then
      bagSize=$(wc -c < $targetBagFile)
      if [ $bagSize -eq 45 ]
      then
        echo "File $targetBagFile size is $bagSize Bytes, therefore will be deleted"
        rm $targetBagFile
      fi
    fi
  else
    echo "LogOnly flag is set to True : Requested for log only, no BAG file is required"
  fi
}

handle_result() {
  result=$1
  resultb=${result:0:20}bags.tar.gz

  sleep 1

  output_log=${WH_name}_${robot}_${timestampH}_${timestamp}_${result}
  output_bag=${WH_name}_${robot}_${timestampH}_${timestamp}_${resultb}

  $local && mntFolder=/mnt/storage || mntFolder=/mnt/Service

  localLogFile=/caja/archive-logs/$result
  localBagFile=/caja/archive-bags/$resultb
  targetLogFile=$mntFolder/archive-logs/$output_log
  targetBagFile=$mntFolder/archive-bags/$output_bag

  $local && copy_to_local || copy_to_amazon
}

### MAIN ###
RED='\033[1;31m'
NC='\033[0m' # No Color

# include parse_yaml function
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $SCRIPT_DIR/parse_yaml.sh

robot=$1
timestamp=$2
logOnly=$3
scp=$4
local=$5
verbose=$6
$local && localUser=$7
$local && localIP=$8

$verbose && echo -e "inside $0, verbose=$verbose, scp=$scp, local=$local"
$verbose && echo -e "robot=$robot"
$verbose && echo -e "timestamp=$timestamp"
$verbose && echo -e "logOnly=$logOnly"
$verbose && echo -e "scp=$scp"
$verbose && echo -e "local=$local"
$verbose && ($local && echo -e "localUser=$localUser")
$verbose && ($local && echo -e "localIP=$localIP")

# I am using /home/ubuntu and not $HOME so every running user will be able to access this file
global_yml="/home/ubuntu/.config/global.yml"
eval $(parse_yaml $global_yml)

echo "WH_name = $WH_name"

# Convert timestamp to human readable to be use with output filename
timestampH=$(date -d @$timestamp +%d-%m-%Y_%H_%M_%S)

time=$(date -d @$timestamp -u "+%F @ %T GMT+0")
printf "\n>>> your timestamp = $time\n\n"

dest_folder="/mnt/Service/log"
pub_file="/home/ubuntu/.ssh/id_rsa_robots"
ssh_flags="-o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $pub_file"
#ssh_flags="-n -o StrictHostKeyChecking=no -i $pub_file"

# Connect to robot and find a log that contain the timestamp
echo "Searching for log file that contains $timestamp timestamp..."
res=$(ssh -n $ssh_flags $robot "$(typeset -f) ; find_logs $timestamp")

if [[ -z "$res" ]]
then
  echo "No matching log file found on the $robot with $timestamp"
  exit 1
else
  echo "Found result(s): $res"

  # Handle each result separately
  for result in $res
  do
    # Check if script was execute from cron and echo accordingly
    [ -t 1 ] && echo -e "Handling result file ${RED}$result${NC}" || echo -e "Handling result file $result"
    handle_result $result
  done
fi

exit 0
#!/bin/bash

# include parse_yaml function
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $SCRIPT_DIR/parse_yaml.sh

# I am using /home/ubuntu and not $HOME so every running user will be able to access this file
db_yml="/home/ubuntu/.config/database.yml"
eval $(parse_yaml $db_yml)

# echo "pgsql_host = $pgsql_host"
# echo "pgsql_name = $pgsql_name"
# echo "pgsql_user = $pgsql_user"
# echo "pgsql_pass = $pgsql_pass"

export PGPASSWORD=$pgsql_pass

interval="'$1 hour'"
PARETO_FILE=$SCRIPT_DIR/pareto_query.sql

if [[ -f "$PARETO_FILE" ]]
then
  PARETO_QUERY="$(<$PARETO_FILE)"
  PARETO_QUERY="$(echo "${PARETO_QUERY/\$\{interval\}/"$interval"}")"
else
  echo -e "Pareto query file $PARETO_FILE was not found\n"
  exit 1
fi

sql_1=$PARETO_QUERY

Database=`psql -h $pgsql_host -d $pgsql_name -U $pgsql_user -A -F , -t -c "${sql_1}"`

if [[ -n "$Database"  ]]
then
  echo "$Database"
fi

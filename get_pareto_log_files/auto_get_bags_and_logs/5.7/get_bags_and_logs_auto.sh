#!/bin/bash

bags_and_logs_usage() {
  echo -e "\nUsage: $0 -hlsv <Hours>" >&2
  echo -e "  $0 -h               - display this help and exit"
  echo -e "  $0 -l <User> <IP>   - transfer files to local machine"
  echo -e "  $0 -s               - use SCP instead of RSYNC"
  echo -e "  $0 -v               - display verbose messages"
  exit 1
}

validate_interval() {
  # Validate digits only
  #re='^[0-9]+$'

  # Validate real number (x.x)
  re='^[0-9]+([.][0-9]+)?$'

  if ! [[ $interval =~ $re ]]
  then
    echo "Invalid interval '$interval', not a number" >&2
    bags_and_logs_usage
  fi

  #if (( $interval <= 0 ))
  if (( $(echo "$interval <= 0" | bc -l) ))
  then
    echo "Invalid interval '$interval', should be greater than 0" >&2
    bags_and_logs_usage
  fi
}

validate_user() {
  if [ -z "$localUser" ]
  then
    echo -e "Invalid local user, should not be empty"
    bags_and_logs_usage
  fi
}

validate_ip() {
  if [ -z "$localIP" ]
  then
    echo -e "Invalid IP address, should not be empty"
    bags_and_logs_usage
  fi

  if ! [[ $localIP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]
  then
    echo "Invalid IP Address $localIP"
    bags_and_logs_usage
  fi

  ping -w 1 $localIP > /dev/null 2>&1
  ping_ret_val=$?

  if [[ $ping_ret_val -ne 0 ]]
  then
    echo -e "Local machine with IP ${RED}$localIP${NC} is unreachable" 1>&2
    bags_and_logs_usage
  else
    echo -e "Local machine with IP ${RED}$localIP${NC} is reachable" 1>&2
  fi
}

### MAIN ###

# Handle script swiches
verbose=false
local=false
scp=false

while getopts ":hlsv" option; do
  case $option in
    h) bags_and_logs_usage; exit ;;
    l) local=true ;;
    s) scp=true ;;
    v) verbose=true ;;
    ?) echo "error: option -$OPTARG is not implemented"; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

connect_opts=()
$local && connect_opts+=( -l )
$scp && connect_opts+=( -s )
$verbose && connect_opts+=( -v )
$verbose && echo ops = "${connect_opts[@]}" "$@"

$verbose && echo "local=$local scp=$scp" 

interval=$1
validate_interval

$local && localUser=$2 && validate_user
$local && localIP=$3 && validate_ip
$verbose && ($local && echo "localUser=$localUser localIP=$localIP")

echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

ts=$(date +%s)

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
output_csv=$SCRIPT_DIR/archive/auto_pareto_$ts.csv

echo "Using $interval interval"

source $SCRIPT_DIR/build_pareto.sh $interval > $output_csv

# Check file is not empty
if [[ -s $output_csv ]]
then
  $verbose && echo -e "source $SCRIPT_DIR/get_pareto_log_files_auto.sh $output_csv $scp $local $verbose $localUser $localIP"
  source $SCRIPT_DIR/get_pareto_log_files_auto.sh $output_csv $scp $local $verbose $localUser $localIP
else
  echo "Pareto CSV file is empty, nothing to do."
fi

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0

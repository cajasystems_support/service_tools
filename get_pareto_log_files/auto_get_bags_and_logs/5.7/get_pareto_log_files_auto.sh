#!/bin/bash

test_connectivity() {
  machine_ip=$(getent hosts $machine_id | cut -f1 -d" ")

  ping -w 1 $machine_id > /dev/null 2>&1
  res=$?
  [ $res -ne 0 ] && { echo "Robot $machine_id is unreachable (ping $machine_ip)" ; return 1 ; }

  ssh -n $ssh_flags -q $machine_id exit
  res=$?
  [ $res -ne 0 ] && echo "Robot $machine_id is unreachable (ssh $machine_ip)"
}

parse_machine_id() {
  # parse robot name for script use, remove prefix i.e BB2 or OP1
  [[ $machine_id =~ ^BB2*|^OP1* ]] && machine_id=${machine_id:3}
  [[ $machine_id =~ ^CT*|^LT* ]]   && machine_id=${machine_id:0:2}${machine_id: -3}

  # Make it lowercase
  machine_id=${machine_id,,}
}

usage() {
  echo -e "\ninsufficient arguments: <CSV filename> <SCP> <LOCAL>"
  echo -e "Usage: '$0 <CSV filename>' <SCP (true|false)> <LOCAL (true|false)>\n"
  exit 1
}

### MAIN ###

[ $# -eq 0 ] && usage

echo -e "\nConnected. -- `date +'%d-%b-%Y %H:%M:%S'` --\n"

FILE=$1
scp=$2
local=$3
verbose=$4
$local && localUser=$5
$local && localIP=$6

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Check if the script was executed from the shell or from another script
parent=$(basename $(readlink -nf $0))
source=${BASH_SOURCE[0]##*/}
$verbose && echo source: $source parent: $parent
[ $parent = $source ] && from_shell=true || from_shell=false
$verbose && echo from_shell = $from_shell

$from_shell && { RED='\033[1;31m' ; NC='\033[0m' ; }

$verbose && echo -e "inside $0, verbose=$verbose, scp=$scp, local=$local"
$scp && echo -e "Will use ${RED}SCP${NC} for file copying" || echo -e "Will use ${RED}RSYNC${NC} for file copying"
$local && echo -e "Will copy files to ${RED}LOCAL${NC} machine $localUser@$localIP " || echo -e "Will copy files to ${RED}CLOUD${NC} machine"
echo -e "CSV File name is: $FILE"

line_count=0
processed=0
failed_counter=0
succeed_counter=0

LOG_ONLY_FILE=$SCRIPT_DIR/logOnlyErrArr.cfg
BAGS_LOGS_FILE=$SCRIPT_DIR/bagsAndLogsErrArr.cfg
IGNORE_FILE=$SCRIPT_DIR/ignoreErrArr.cfg

$verbose && ( [ -f $LOG_ONLY_FILE ]  && echo "LOG_ONLY_FILE $LOG_ONLY_FILE found"   || echo "LOG_ONLY_FILE $LOG_ONLY_FILE not found" )
$verbose && ( [ -f $BAGS_LOGS_FILE ] && echo "BAGS_LOGS_FILE $BAGS_LOGS_FILE found" || echo "BAGS_LOGS_FILE $BAGS_LOGS_FILE not found" )
$verbose && ( [ -f $IGNORE_FILE ]    && echo "IGNORE_FILE $IGNORE_FILE found"       || echo "IGNORE_FILE $IGNORE_FILE not found" )

logOnlyErrArrCfg="$(<$LOG_ONLY_FILE)"
bagsAndLogsErrArrCfg="$(<$BAGS_LOGS_FILE)"
ignoreErrArrCfg="$(<$IGNORE_FILE)"

$verbose && echo -e "\nlogOnlyErrArrCfg\n$logOnlyErrArrCfg\n"
$verbose && echo -e "bagsAndLogsErrArrCfg\n$bagsAndLogsErrArrCfg\n"
$verbose && echo -e "ignoreErrArrCfg\n$ignoreErrArrCfg\n"

# Serach for tabs in input file
awk '{exit !/\t/}' $FILE
[ $? = 0 ] && echo -e "Found tab delimited file, will convert to comma delimited \n" || echo -e "Found comma delimited file \n"

echo "Working on the following cases"
echo -e "Time \t\t\tRobot \t\tTimestamp"
awk -F, '{printf $1 "\t\t" $3 "\t" $2  "\n"}' <<< $(sed 's/\t/,/g' $FILE)
echo

# Convert input file to comma delimited and read line by line
while IFS=, read gmt_time timestamp machine_id name f5 f6 f7 f8
do
  ((line_count++))

  echo -e "\n####################################"
  echo -e "CSV line:\t $line_count"
  echo -e "gmt_time:\t $gmt_time"
  echo -e "timestamp:\t $timestamp"
  echo -e "machine_id:\t $machine_id"
  echo -e "name:\t\t $name"
  echo

  # Check timestamp validaty and that the machine_id is not empty
  if [[ $(date -d @$timestamp 2> /dev/null) ]] && [ -n "$machine_id" ]
  then
    # Serch error in ignoreErrArr
    printf '%s\n' "${ignoreErrArrCfg[@]}" | grep "${name}" > /dev/null 2>&1
    # In case the from_shell is true, continue grabbing files
    # altough it exists in ignoreErrArrCfg file
    if [[ $? -eq 0 && $from_shell = false ]]
    then
      echo "Error is not relevant for investigation, ignoring error"
    else
      echo "Error is relevant for investigation, continue grabbing files"

      parse_machine_id
      test_connectivity

      if [[ $res -ne 0 ]]
      then
        ((failed_counter++))
      else
        # Check if script was execute from cron
        if [[ -t 1 ]]
        then
          # Not from cron since FD 1 (stdout) is open
          echo -e "CSV line: $line_count, getting log files for: ${RED}$machine_id${NC} with ${RED}$timestamp${NC} timestamp"
        else
          echo -e "CSV line: $line_count, getting log files for: $machine_id with $timestamp timestamp"
        fi

        # Search if error exist in log array
        printf '%s\n' "${logOnlyErrArrCfg[@]}" | grep "${name}" > /dev/null 2>&1
        [[ $? -eq 0 ]] && logOnly=true || logOnly=false

        /bin/bash $SCRIPT_DIR/get_bags_and_logs_from_robots_auto.sh $machine_id $timestamp $logOnly $scp $local $verbose $localUser $localIP
        ret_val=$?

        [ $ret_val -ne 0 ] && echo -e "Error code: ${RED}$ret_val${NC}"
        [ $ret_val -ne 0 ] && ((failed_counter++)) || ((succeed_counter++))
      fi
      ((processed++))
    fi
  else
    [[ "$gmt_time" = "gmt" ]] && echo "Skipping header line." || echo "Invalid data in line, skipping line."
  fi
done <<< $(sed 's/\t/,/g' $FILE)

echo -e "\n####################################\n"
echo -e "Summary:"
echo -e "Total CSV lines: $line_count"
echo -e "Processed lines: $processed"
echo -e "Succeed lines:   $succeed_counter"
echo -e "Failed lines:    $failed_counter"
echo -e "Skipped lines:  " $(( $line_count - $processed ))

echo -e "\nDONE! -- `date +'%d-%b-%Y %H:%M:%S'` --\n"
exit 0
